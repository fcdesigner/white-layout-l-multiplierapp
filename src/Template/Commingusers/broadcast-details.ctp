<style type="text/css">
  img.listing_image {    height: 35px;    width: 36px; }
  .top-th{text-align: center !important}
  .youtube_official{
      color: #ff0000 !important;
  }
  .cust td{text-align: center !important}

  .btn-success{
	margin: 10px 0;
  }
  
</style>

<div class="row">
  <div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light bordered">                             
      <div class="portlet-title">
        <div class="caption font-dark bd_tab_boxs ">
          <i class="icon-settings font-dark"></i>
          <span class="caption-subject bold uppercase"><?php echo  __(broadcasts_details); ?></span>
          <!-- <a href="<?php echo HTTP_ROOT.'app/updateStats?stream_id='.$liveStreamsData->id; ?>" class=""><img src="<?php echo HTTP_ROOT ;?>img/refresh.png" width="30"></a> -->
          <p></p>
		  <div class="table-scrollable">
          <table class="table table-striped table-bordered order-column bd_info_tb">
            <tr>
              <th><?php echo  __(Application); ?></th>
              <td><?php echo $liveStreamsData['stream_app']; ?></td>
              <th> Stream name</th>
              <td><?php echo $liveStreamsData['stream_name']; ?></td>
            </tr>
            <tr>
              <th><?php echo  __(Account); ?></th>
              <td><?php echo $groupsData['group_name']; ?></td>
              <th> <?php echo __(Formname);?></th>
              <td class="form_title" style="font-size: 12px !important;">
			  <?php //echo strip_tags($FormListingData['form_title']); ?>
			  <?php echo strip_tags($FormListingData['form_name']); ?>
			  </td>
            </tr>
          </table>
  		</div>
          <div style="font-size:15px"> <?= __('other_detail_note',[HTTP_ROOT.'commingusers/view-broadcasts', HTTP_ROOT.'app/updateStats?stream_id='.$liveStreamsData->id]); ?> </div> 
        </div>
        <div class="tools"></div>               
        <div class="row">
          <div class="table-scrollable col-md-6">
            <table class="table table-bordered">
              <?php 
                $total = count($liveBroadData);
                //$followers = sumByKey($liveBroadData,'followers');
                 $followers =  @$this->Custom->getBroadcastReached('','',$stream_id);
                $error = 0;

                /*$starting = countValuesByKey($liveBroadData,'wowza_status', 'Starting');
                $active = countValuesByKey($liveBroadData,'wowza_status', 'Active');
                $error = countValuesByKey($liveBroadData,'wowza_status', 'Error');
                */
              ?>
              <tr>
                <?php echo __(norealtime); ?>
              </tr>
              <tr>
                <th><?php echo __(Social_Accounts); ?> <span data-toggle="tooltip" title="<?php echo  __(sum_broadcast_detail); ?>"><i class="fa fa-question-circle"></i></span></th>
                <td class=""><?php echo $total; ?></td>
                <th><?php echo __(Success); ?> <span data-toggle="tooltip" title="<?php echo __(sum_broadcast_followers); ?>"><i class="fa fa-question-circle"></i></span></th>
                <td class=""><?php echo number_format($followers,0,0, '.'); ?></td>
                <th><?php echo __(Error); ?> <span data-toggle="tooltip" title="<?php echo __(content_broadcasted); ?>"><i class="fa fa-question-circle"></i></span></th>
                <td class=""><?php echo $error; ?></td>
              </tr>
            </table>
          </div> 
        </div> 
      </div> 

      <div class="portlet-body portlet-body">
        <table class="table table-striped table-bordered table-hover table-checkable order-column cust" id="broad_cast_users_listing">
          <thead>
            <tr>
              <th class="top-th alignCenter" style="padding: 10px 18px;">#</th>
              <th class='alignCenter'><?php echo __(date);?></th>
              <?php if (isset($authUser['role']) && in_array( $authUser['role'], ['1', '2', '3']) ) { ?>
              <th class='alignCenter'><?php echo __(start);?></th>
              <th class='alignCenter'><?php echo __(end);?></th>
              <th class='alignCenter'><?php echo __(broadcasted);?></th>
              <?php  }  ?>
              <th class='alignCenter'><?php echo __(TD);?></th>
              <th class='alignCenter'><?php echo __(type);?></th>
              <th class='alignCenter'><?php echo __(image);?></th>
              <th class='alignCenter'><?php echo __(type);?></th>
              <th class='alignCenter'><?php echo __(Privacy);?></th>
              <th class='alignCenter'><?php echo __(name); ?></th>
              <th class='alignCenter'>ID</th>
              <th>
                <img  src="<?php echo HTTP_ROOT ;?>img/total.png" width="25" data-text="TOTAL">
                <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('total_msg'); ?>"><i class="fa fa-question-circle"></i></span>
              </th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/comment.png" width="30" data-text="COMMENT"></th>
              <!-- <th>
                <img  src="<?php echo HTTP_ROOT ;?>img/view.png" width="38" data-text="VIEW">
                <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('view_msg'); ?>"><i class="fa fa-question-circle"></i></span>
              </th> -->
              <th><img src="<?php echo HTTP_ROOT ;?>img/like.png" width="35" data-text="LIKE"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/love.png" width="30" data-text="LOVE"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/haha.png" width="30" data-text="HAHA"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/wow.png" width="30" data-text="WOW"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/sad.png" width="30" data-text="SAD"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/angry.png" width="30" data-text="ANGRY"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/share.png" width="30" data-text="SHARE"></th>
              <th class='alignCenter'><?php echo __(Status);?></th>
              <th class='alignCenter'><?php echo __(reach); ?></th>
              <th class='alignCenter'><?php echo __(Success); ?></th>
              <th class='alignCenter'><?php echo __(Error); ?></th>
              <th class='alignCenter'><?php echo __(total); ?> %</th>
            </tr>
          </thead>                   
          <tbody>
            <?php if(!empty($liveBroadData)){
              $total = 0;
              foreach($liveBroadData as $live){
                $total += $live['followers'];
              }

              foreach($liveBroadData as $k => $liveBroad){
                $stream_flag = $liveBroad['flag'];
                $flag_option = $liveBroad['flag_option'];
              ?>
              <tr class="odd gradeX">
                <td><?php echo $k+1; ?></td>
                <td>
                  <?php echo appDate($liveStreamsData['stream_started']); ?>
                </td>
                <?php if(isset($authUser['role']) && in_array($authUser['role'], ['1', '2', '3'])){ ?>
                  <td>
                    <?php echo appTime($startDate); ?>
                  </td>
                  <td>
                    <?php echo appTime($endDate); ?>
                  </td>
                  <td>
                    <?php echo tDiffecence($startDate,$endDate); ?>
                  </td>
                <?php } ?>
                <td>
                  <a href="<?php echo HTTP_ROOT ;?>commingusers/broadcast-title/<?php echo $liveBroad['id']; ?>/<?php echo $liveStreamsData->id; ?>" data-target="#myModal" data-toggle="ajaxModal"><?php echo __(Read); ?></a>
                </td>
                <?php if($liveBroad['flag'] != "youtube") { ?>
                  <td class="facebook_official"><i class="fa fa-facebook-official fbIcon" aria-hidden="true" data-text="FB"></i></td>
                <?php } else { ?>
                  <td class="youtube_official"><i class="fab fa-youtube fbIcon" aria-hidden="true" data-text="Youtube"></i></td>
                <?php } ?>
                <td>
                  <?php $b64image = base64_encode(file_get_contents($liveBroad['image_path'])); ?>
                  <img class="listing_image" src="data:image/png;base64,<?php echo $b64image; ?>">
                </td>
                <td>
                    <?php echo ($liveBroad['flag'] == "youtube") ? "YouTube" : $liveBroad['flag']; ?>
                </td>
                <td>
                    <?php
                     echo get_privacy_type($flag_option,$stream_flag); ?>
                </td>
                <td>
                    <?php echo $liveBroad['fb_name']; ?>
                </td>
                <td>
                  <?php if($liveBroad['flag'] != "youtube") { ?>
                    <a target="_blank" href="https://www.facebook.com/<?php echo $liveBroad['user_identifier_id']; ?>">
                        <?php echo $liveBroad['user_identifier_id']; ?>
                    </a>
                  <?php } else { ?>
                    <a target="_blank" href="<?php echo "https://www.youtube.com/channel/".$liveBroad['channelId']; ?>"><?php echo $liveBroad['channelId']; ?></a>
                  <?php } ?>
                </td>
                <?php $dataTotal =  @$this->Custom->getBroadCastData('',$liveBroad['id'],'',$liveStreamsData['id'],''); ?>
                <!-- <td><?=  $dataTotal->angry+$dataTotal->comment+$dataTotal->view+$dataTotal->vlike+$dataTotal->love+$dataTotal->haha+$dataTotal->wow+$dataTotal->sad+$dataTotal->dislike; ?></td> -->
                <td><?=  $dataTotal->angry+$dataTotal->comment+$dataTotal->vlike+$dataTotal->love+$dataTotal->haha+$dataTotal->wow+$dataTotal->sad+$dataTotal->dislike+$dataTotal->view+$dataTotal->share; ?></td>
                <td><?=  ($dataTotal->comment) ? $dataTotal->comment:'0'; ?></td>
                <!-- <td><?=  ($dataTotal->view) ? $dataTotal->view:'0'; ?></td> -->
                <td><?=  ($dataTotal->vlike) ? $dataTotal->vlike:'0'; ?></td>
                <td><?=  ($dataTotal->love) ? $dataTotal->love:'0'; ?></td>
                <td><?=  ($dataTotal->haha) ? $dataTotal->haha:'0'; ?></td>
                <td><?=  ($dataTotal->wow) ? $dataTotal->wow:'0'; ?></td>
                <td><?=  ($dataTotal->sad) ? $dataTotal->sad:'0'; ?></td>
                <td><?=  ($dataTotal->angry) ? $dataTotal->angry:'0'+$dataTotal->dislike; ?></td>
                <td><?=  ($dataTotal->share) ? $dataTotal->share:'0'; ?></td>
                <?php $video_status = @$this->Custom->getVideoStatus($liveBroad['id'],$liveStreamsData['id']); ?>
                <td>
                  <?php 
                    if($video_status->status != "UNPUBLISHED"){ 
                    $status = "Success";
                    ?>
                    <button data-toggle="tooltip" title="" data-original-title="" class="btn btn-success btn-xs" style="background: #26cc00;">Live</button>
                  <?php }else{ 
                     $status = "Error";
                    ?>
                    <button data-toggle="tooltip" title="<?php echo $video_status->message; ?>" data-original-title="<?php echo $video_status->message; ?>" class="btn btn-danger btn-xs">Error</button>
                  <?php } ?>
                </td>
                <td>
                  <?php echo $liveBroad['followers']; ?>
                </td>
                <td>
                  <?php echo ($status == "Success") ? $liveBroad['followers'] : 0; ?>
                </td>
                <td>
                 <?php echo ($status == "Error") ? $liveBroad['followers'] : 0; ?>
                </td>
                <td>
                  <?php 
                  if(($status == "Error")){
                     echo "0 %";
                  }else{
                    $per = ($liveBroad['followers']/$total)*100;
                    $per = round($per, 4);
                    echo number_format($per,4,',', '') . "%";
                  }
                  
                  ?>
                </td>
              </tr><?php } } ?>                       
            </tbody>
          </table>
        </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
              
  
     
<style>
.fullwid{

   width:45%;
   text-align: center;
}
.alignCenter{
    text-align:center;
}
.toggler{
    display: none;
}
@media screen and (max-width: 640px){
 div.dataTables_length select {
  width: 145px!important;
    float: right;
    margin-left: 9px;
} 
.dataTables_wrapper .dt-buttons a{padding: 6px 35px!important;}
}
@media screen and (max-width:480px){
.bd_info_tb {
    min-width: 600px;
}
.bd_tab_box ,.bd_tab_boxs{
    overflow: scroll;
    overflow-y: hidden;
    width: 100%;
}
.dataTables_paginate{
    overflow: scroll;
    overflow-y: hidden;  
}
.dataTables_paginate .pagination{
    min-width: 500px;
    text-align: left;

}
}
@media screen and (max-width: 360px)
{
.top-right-btn{
  padding: 5px 7px;
}
}

</style>