<style type="text/css">
  .portlet-title{border-bottom :none !important;}
</style>
<div class="row">
  <div class="col-md-12">
    <div class="portlet light bordered">                             
      <div class="portlet-title">
        <?php echo $this->element('include/time_filter'); ?>
        <div class="caption font-dark">
            <i class="icon-settings font-dark"></i>
            <span class="caption-subject bold uppercase"><?php echo  __(lives_made); ?></span>
        </div>
        <br>
        <p><?php echo __(view_note);?></p>
         <?php echo __(other_note_reac); ?>
        <div class="row">
          <div class="col-md-8 bd_tab_box">
            <table class="table table-bordered bd_info_tb">
              <tr>
                <th><?php echo  __(total_broadcast); ?> <span data-toggle="tooltip" title="<?php echo  __(sum_broadcast); ?>"><i class="fa fa-question-circle"></i></span></th>
                <td class=""><?php echo $liveBroadDataCount; ?></td>
                <th>Total Success <span data-toggle="tooltip" title="<?php echo __(sum_broadcast_followers); ?>"><i class="fa fa-question-circle"></i></span></th>
                <td class=""><?php echo number_format($followersSum,0,",","."); ?></td>
                <th><?php echo __(total_broadcasted); ?> <span data-toggle="tooltip" title="<?php echo __(total_broadcasted_tooltip); ?>"><i class="fa fa-question-circle"></i></span></th>
                <td class=""><?php echo $totalBroadCasted; ?> </td>
              </tr>
            </table>
          </div> 
        </div> 
        <div class="tools"></div>                                     
        <input type="hidden" id="startDate" value="<?php echo $startDate; ?>">
        <input type="hidden" id="endDate" value="<?php echo $endDate; ?>">
        <?php echo __(other_note,[HTTP_ROOT.'app/updateStats?form_id='.$form_id]); ?>
       
      </div>
      <div class="row">
          <div class="table-scrollable col-lg-12 col-md-12 col-sm-12" style="text-align:center;">
          <?php 
           $FakeUsers = @$this->Custom->getFakeUsers();

          $dataTotal =  @$this->Custom->getBroadCastData($form_id); 
          $reached =  @$this->Custom->getBroadcastReached($form_id);

          if($authUser['role'] == "1" || $authUser['role'] == "2"){
            $reached  +=  $FakeUsers['reached'];
            $dataTotal->vlike  +=  $dataTotal->vlike+ $FakeUsers['vlike'];
            $dataTotal->haha  +=  $FakeUsers['haha'];
            $dataTotal->love  +=  $FakeUsers['love'];
            $dataTotal->sad  +=  $FakeUsers['sad'];
            $dataTotal->angry  +=  $FakeUsers['angry'];
            $dataTotal->wow  +=  $FakeUsers['wow'];
          }
         
          ?>
            <table class="table custm-cls table-striped table-bordered order-column">
              <thead>
                <tr>
                    <th><img  src="<?php echo HTTP_ROOT ;?>img/total.png" width="25">
                        <span data-toggle="tooltip" title="" data-placement="bottom" data-original-title="<?= __('total_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                    </th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/comment.png" width="30"></th>
                    <!-- <th><img  src="<?php echo HTTP_ROOT ;?>img/view.png" width="38">
                        <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('view_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                    </th> -->
                    <th><img src="<?php echo HTTP_ROOT ;?>img/like.png" width="35"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/love.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/haha.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/wow.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/sad.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/angry.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/share.png" width="30"></th>
                    <th><?= __('reached')?></th>
                    <?php if($authUser['role'] != "1" && $authUser['role'] != "2"){ ?>
                      <th><?= __('Refresh'); ?></th>
                    <?php } ?>
                </tr>
              </thead>

              <tbody align="center">
                <tr>
                  <td><?php //echo 'angry'.$dataTotal->angry;echo 'comment'.$dataTotal->comment;echo 'view'.$dataTotal->view;echo 'vlike'.$dataTotal->vlike;echo 'love'.$dataTotal->love;echo 'haha'.$dataTotal->haha;echo 'wow'.$dataTotal->wow;echo 'sad'.$dataTotal->sad;echo 'dislike'.$dataTotal->dislike;?><?=  @$this->Custom->number_shorten($dataTotal->angry+$dataTotal->comment+$dataTotal->view+$dataTotal->vlike+$dataTotal->love+$dataTotal->haha+$dataTotal->wow+$dataTotal->sad+$dataTotal->dislike+$dataTotal->share, 2); ?></td>
                  <td><?=  ($dataTotal->comment) ? @$this->Custom->number_shorten($dataTotal->comment, 2):'0'; ?></td>
                  <!-- <td><?=  ($dataTotal->view) ? @$this->Custom->number_shorten($dataTotal->view):'0'; ?></td> -->
                  <td><?=  ($dataTotal->vlike) ? @$this->Custom->number_shorten($dataTotal->vlike, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->love) ? @$this->Custom->number_shorten($dataTotal->love, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->haha) ? @$this->Custom->number_shorten($dataTotal->haha, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->wow) ? @$this->Custom->number_shorten($dataTotal->wow, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->sad) ? @$this->Custom->number_shorten($dataTotal->sad, 2):'0'; ?></td>
                  <td><?=  @$this->Custom->number_shorten(($dataTotal->angry) ? $dataTotal->angry:'0'+$dataTotal->dislike, 2); ?></td>
                  <td><?=  ($dataTotal->share) ? @$this->Custom->number_shorten($dataTotal->share, 2):'0'; ?></td>
                  <td><?=  ($reached) ? @$this->Custom->number_shorten($reached, 2):'0'; ?></td>
                  <?php if($authUser['role'] != "1" && $authUser['role'] != "2"){ ?>
                    <td><a href="<?php echo HTTP_ROOT.'app/updateStats?form_id='.$form_id; ?>" class=""><img src="<?php echo HTTP_ROOT ;?>img/refresh.png" width="30"></a></td>
                  <?php } ?>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <br><br><br>
      <div class="portlet-body portlet-body">                                           
        <table class="table table-striped table-bordered table-hover table-checkable order-column text-center" id="broadcasts_listing">
          <thead>
            <tr>
              <th class="no-sort alignCenter" style="padding: 10px 18px;">#</th>
              <th class='alignCenter'><?php echo __(date);?></th>                  
              <th class='alignCenter'><?php echo __(start);?></th>
              <th class='alignCenter'><?php echo __(end);?></th>
              <th class='alignCenter'><?php echo __(broadcasted); ?></th>
              <th class='alignCenter'><?php echo __(TD); ?></th>
              <th>
                <img  src="<?php echo HTTP_ROOT ;?>img/total.png" width="25" data-text="TOTAL">
                <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('total_msg'); ?>"><i class="fa fa-question-circle"></i></span>
              </th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/comment.png" width="30" data-text="COMMENT"></th>
              <!-- <th>
                <img  src="<?php //echo HTTP_ROOT ;?>img/view.png" width="38" data-text="VIEW">
                <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('view_msg'); ?>"><i class="fa fa-question-circle"></i></span>
              </th> -->
              <th><img src="<?php echo HTTP_ROOT ;?>img/like.png" width="35" data-text="LIKE"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/love.png" width="30" data-text="LOVE"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/haha.png" width="30" data-text="HAHA"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/wow.png" width="30" data-text="WOW"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/sad.png" width="30" data-text="SAD"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/angry.png" width="30" data-text="ANGRY"></th>
              <th><img src="<?php echo HTTP_ROOT ;?>img/share.png" width="30" data-text="SHARE"></th>
              <th class='alignCenter'><?php echo __(reach); ?></th>
              <th class='alignCenter'><?php echo __(Error); ?></th>
              <th class='alignCenter'><?php echo __(Success); ?></th>
            <!--   <th class='alignCenter'><?php echo __(total); ?></th> -->
              <th class='alignCenter'><?php echo __(per_success); ?></th>
              <th class='alignCenter'><?php echo __(last_update); ?></th>
            </tr>
          </thead>
          <tbody>
                                                      
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<style>
.fullwid{
  width:45%;
  text-align: center;
}
.alignCenter{
	text-align:center;
}
.toggler{
  display: none;
}

.btn-success{
	margin: 10px 0;
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    vertical-align: inherit;
}

@media screen and (max-width: 640px){
  div.dataTables_length select {
    width: 145px!important;
    float: right;
    margin-left: 9px;
  } 
  .dataTables_wrapper .dt-buttons a{padding: 6px 35px!important;}
}
@media screen and (max-width:480px){
  .bd_info_tb {
    min-width: 600px;
  }
  .bd_tab_box {
    overflow: scroll;
    overflow-y: hidden;
  }
  .dataTables_paginate{
    overflow: scroll;
    overflow-y: hidden;  
  }
  .dataTables_paginate .pagination{
    min-width: 500px;
    text-align: left;
  }
}
@media screen and (max-width: 360px){
  .top-right-btn{
    padding: 5px 7px;
  }
}
.custm-cls th{text-align: center;}
</style>