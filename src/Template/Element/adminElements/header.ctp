<script type="text/javascript"> var base_url = '<?php echo HTTP_ROOT; ?>';</script>
<script type="text/javascript"> var page_title = '<?php echo SITE_TITLE." | ".SITE_TITLE2;?>';</script>
<script type="text/javascript"> var logo_pdf_style = '<img style="margin:0 auto;" src="'+ base_url +'img/smalllogo.png">';</script>


<?php 
if($this->request->params['action'] != 'index')
{
    $set_filename_csv = $this->request->params['controller']."-".$this->request->params['action'];
}
else
{
    $set_filename_csv = $this->request->params['controller'];
}
$session        = $this->request->session();

?>
<script type="text/javascript"> var set_filename_csv = '<?php echo $set_filename_csv; ?>'</script>

<div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?php echo HTTP_ROOT.'users/dashboard';?>">
					<img class=" sizeLogo logo-default" src="<?php echo HTTP_ROOT.'img/logo_big.jpg' ?>"/>
                        <!-- <img src="../assets/layouts/layout/img/logo_big.jpg" alt="logo" class="logo-default" /> --> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <span></span>
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
				
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    

                    <ul class="nav navbar-nav pull-right">
                    <?php if ( $authUser['role'] == '1' || $authUser['is_admin'] == '1' || $authUser['role'] == '2' || $authUser['role'] == '5'){  ?>
                        <li class="">
                             <form action="" method="post" id="managerform">
                                <?php if (!empty($managers)) {  
                                        echo $this->Form->input('managers',[
                                            'templates' => ['inputContainer' => '{{content}}'],
                                            'label' => false,
                                            'required' => false,
                                            'class'=>'boot_select',
                                            'type'=>select,
                                            'options'=> @$managers
                                            ]);
                                    } ?>
                            </form>
                        </li>
                        <?php } ?>
                        
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <?php 
                             $flag = HTTP_ROOT.'img/brasilflag.jpg';
                            if($session->read('language') == "en_US"){
                                $flag = HTTP_ROOT.'img/usaflag.jpg';
                            }else if($session->read('language') == "en_SP"){
                                $flag = HTTP_ROOT.'img/spanish.jpg';
                            }
                            ?>
                            <img alt=""  src="<?php echo $flag; ?>"/>
                                <span class="username username-hide-on-mobile"><?php //echo __(select_language); ?></span>
                               
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                     <a href="<?php echo HTTP_ROOT.'app/changeLanguage?language=2'; ?>" class="">
                                    <img src="<?php echo HTTP_ROOT."img/brasilflag.jpg"; ?>" alt="" height="35" > <?php echo __(portuguese); ?>
                                     </a>
                                </li>

                                <li>
                                    <a href="<?php echo HTTP_ROOT.'app/changeLanguage?language=1'; ?>" class="">
                                    <img src="<?php echo HTTP_ROOT.'img/usaflag.jpg'; ?>" alt="" height="35"> <?php echo __(english); ?>
                                     </a>
                                </li>
                                <li>
                                     <a href="<?php echo HTTP_ROOT.'app/changeLanguage?language=3'; ?>" class="">
                                    <img src="<?php echo HTTP_ROOT."img/spanish.jpg"; ?>" alt="" height="32" > <?php echo __(spanish); ?>
                                     </a>
                                </li>
                            </ul>
                        </li>

                        
                        <!-- BEGIN NOTIFICATION DROPDOWN -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar" style='display:none;'>
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <i class="icon-bell"></i>
                                <span class="badge badge-default"> 7 </span>
                            </a>
                            <ul class="dropdown-menu">
                                <li class="external">
                                    <h3>
                                        <span class="bold">12 pending</span> notifications</h3>
                                    <a href="page_user_profile_1.html">view all</a>
                                </li>
                                <li>
                                    <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">just now</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-success">
                                                        <i class="fa fa-plus"></i>
                                                    </span> New user registered. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">3 mins</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Server #12 overloaded. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">10 mins</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-warning">
                                                        <i class="fa fa-bell-o"></i>
                                                    </span> Server #2 not responding. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">14 hrs</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-info">
                                                        <i class="fa fa-bullhorn"></i>
                                                    </span> Application error. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">2 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Database overloaded 68%. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">3 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> A user IP blocked. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">4 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-warning">
                                                        <i class="fa fa-bell-o"></i>
                                                    </span> Storage Server #4 not responding dfdfdfd. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">5 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-info">
                                                        <i class="fa fa-bullhorn"></i>
                                                    </span> System Error. </span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:;">
                                                <span class="time">9 days</span>
                                                <span class="details">
                                                    <span class="label label-sm label-icon label-danger">
                                                        <i class="fa fa-bolt"></i>
                                                    </span> Storage server failed. </span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
							<img alt="" class="img-circle" src="<?php echo HTTP_ROOT.'img/uploads/'.(@$admins_info->admin_img != ''?@$admins_info->admin_img:'prof_photo.png'); ?>"/>
                                <span class="username username-hide-on-mobile"><?php  echo ucfirst($authUser['name']) ;?>   <?php if ($authUser['is_admin'] == 1) { echo "( ".$authRole." )"; } ?>  </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="<?php echo HTTP_ROOT."users/profile-password/".$authUser['id']; ?>">
                                        <i class="icon-key"></i> <?php echo __(change_password); ?> </a>
                                </li>
                                <li>
                                    <a href="<?php echo HTTP_ROOT."users/update-profile/".$authUser['id']; ?>">
                                        <i class="icon-user"></i> <?php echo __(edit_profile); ?> </a>
                                </li>

                                <!--  <li>
                                <li>
                                    <a href="<?php echo HTTP_ROOT."users/admin-edit"; ?>">
                                        <i class="icon-user"></i> My Profile </a>
                                </li>
                                    <a href="app_calendar.html">
                                        <i class="icon-calendar"></i> My Calendar </a>
                                </li>
                                <li>
                                    <a href="app_inbox.html">
                                        <i class="icon-envelope-open"></i> My Inbox
                                        <span class="badge badge-danger"> 3 </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="app_todo.html">
                                        <i class="icon-rocket"></i> My Tasks
                                        <span class="badge badge-success"> 7 </span>
                                    </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="page_user_lock_1.html">
                                        <i class="icon-lock"></i> Lock Screen </a>
                                </li> -->
                                <li>
                                    <a href="<?php echo HTTP_ROOT.'users/logout';?>">
                                        <i class="icon-lock"></i><?php echo __( log_out); ?> </a>
                                </li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                        <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                        <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                        <li class="dropdown dropdown-quick-sidebar-toggler" style='display:none;'>
                            <a href="javascript:;" class="dropdown-toggle">
                                <i class="icon-logout"></i>
                            </a>
                        </li>
                        <!-- END QUICK SIDEBAR TOGGLER -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END HEADER INNER -->

                

        </div>
<style>
/*.sizeLogo{
	height:45px;
    margin: 0 auto;
}*/

.sizeLogo{
    height:60px;
    width:70px;
    margin: -2px auto !important;
}
</style>