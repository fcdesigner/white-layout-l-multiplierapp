<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.6
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

<style>

	.x_title h4{
		font-weight: bold;
	}

</style>

<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <!-- <title><?php echo SITE_TITLE." | ".SITE_TITLE2;?></title> -->
        <title><?php echo __(site_title)." | ".__(site_sub_title);?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<!-- <link rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" /> -->
        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo HTTP_ROOT;?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo HTTP_ROOT;?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->

        <link href="<?php echo HTTP_ROOT;?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo HTTP_ROOT;?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo HTTP_ROOT;?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="<?php echo HTTP_ROOT;?>assets/global/plugins/emojionearea-master/dist/emojionearea.min.css" media="screen">
		<link href="<?php echo HTTP_ROOT;?>css/white_layout.css" rel="stylesheet" type="text/css" />
    
        <!-- END THEME LAYOUT STYLES -->
       <!--  <link rel="shortcut icon" href="favicon.ico" />  -->
        <link rel="icon" href="<?php echo HTTP_ROOT; ?>img/uploads/favicon.ico" sizes="32x32" />
		<?php // echo $this->Html->css(['Admin/custom.css','Admin/icheck/flat/green.css']); ?>
        <?php echo $this->element('include/header_css'); ?>
        <?php echo $this->element('include/header_js'); ?>
        <!-- Custom Css for sonoclick -->
        <link href="<?php echo HTTP_ROOT;?>css/sonoclick.css" rel="stylesheet" type="text/css" />
        <!-- End Custom Css -->
        <script type="text/javascript">
            var HTTP_ROOT = "<?php echo HTTP_ROOT;?>";
        </script>

        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
         <script type="text/javascript"> var pyzeAppKey="oKpwARKJSTGM0rPeBx8SMw"; !function(a,b){function k(a,b){function c(b){a[b]=function(){a._q.push([b].concat(Array.prototype.slice.call(arguments,0)))}}for(var d=0;d<b.length;d++)c(b[d])}var c=a.Pyze||{_q:[]},d=a.PyzeEvents||{_q:[]},e=a.PyzeCuratedEvents||{_q:[]},f=a.PyzeIdentity||{_q:[]},g=["initialize","getTimerReference","postWebAppVersion","postPageView"],h=["postCustomEvent","postCustomEventWithAttributes","postTimedEvent","postExplicitActivation"],i=["post"],j=["setUserIdentifer","resetUserIdentifier","postTraits"];k(c,g),k(d,h),k(e,i),k(f,j);var l=b.createElement("script"),m=b.getElementsByTagName("script")[0];l.type="text/javascript",l.async=!0,l.src="https://cdn.pyze.com/pyze.js",m.parentNode.insertBefore(l,m),a.Pyze=c,a.PyzeEvents=d,a.PyzeCuratedEvents=e,a.PyzeIdentity=f}(window,document),Pyze.initialize(pyzeAppKey);</script>
         <script> window.fbAsyncInit = function() { FB.init({ appId : '181949048879001', xfbml : true, version : 'v2.10' });
FB.AppEvents.logPageView();
};
(function(d, s, id){ var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://connect.facebook.net/en_US/sdk.js"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk')); </script>

        
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <!-- BEGIN HEADER -->
        <?php echo $this->element('adminElements/header'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <?php echo $this->element('adminElements/left_sidebar'); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <?php if( ($authUser['role'] == 3 && $errormessagehour12New) || ($authUser['role'] == 4 && $errormessagehour12New)){ ?>
                    <div class="global12hourdivgreen" data-autohide="false" style="">
                        <div class="toast-body" style="background-color:#42ef01;color:white;text-align: center;padding: 5px;">
                            <?php echo __(errormessagehour12_green); ?>
                            
                        </div>
                    </div> 

                    <div class="global12hourdiv" data-autohide="false" style="margin-top:5px;">
                        <div class="toast-body" style="background-color:red;color:white;text-align: center;padding: 5px;">
                            <?php echo __(errormessagehour12); ?>
                            
                        </div>
                    </div> 
                    <?php } ?>  
                    
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->
                    <?php echo $this->element('adminElements/admin_theme_panel'); ?>
                    <!-- END THEME PANEL -->

                    <!-- BEGIN Content INCLUDE -->

                     <?php echo $this->Flash->render(); ?>
                    <?php echo $this->fetch('content');?>
                    <!-- END Content INCLUDE -->

                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php echo $this->element('adminElements/footer'); ?>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="<?php echo HTTP_ROOT;?>assets/global/plugins/respond.min.js"></script>
<script src="<?php echo HTTP_ROOT;?>assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/apps/scripts/jquery.form.min.js"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <!-- <link href="https://raw.githubusercontent.com/t0m/select2-bootstrap-css/bootstrap3/select2-bootstrap.css" rel="stylesheet" type="text/css" /> -->
        <!-- <link href="<?php echo HTTP_ROOT;?>assets/global/plugins/select2/css/select2-bootstrap.css" rel="stylesheet" type="text/css" /> -->
        <script src="<?php echo HTTP_ROOT;?>assets/global/plugins/select2/js/select2.full.js" type="text/javascript"></script>
        
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo HTTP_ROOT;?>assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS 
        <script src="<?php echo HTTP_ROOT;?>assets/pages/scripts/table-datatables-buttons.js" type="text/javascript"></script>-->
        <!-- <script src="<?php echo HTTP_ROOT;?>assets/pages/scripts/table-datatables-managed.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo HTTP_ROOT;?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo HTTP_ROOT;?>assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
      
        <script src="<?php echo HTTP_ROOT;?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        
        <!-- Include Required Prerequisites -->
        
        <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
        
        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

        <?php echo $this->element('include/footer_js'); ?>
        <script src="<?php echo HTTP_ROOT;?>js/sonoclick.js"></script>

        <!--<link href="https://snatchbot.me/sdk/webchat.css" rel="stylesheet" type="text/css"><script src="https://snatchbot.me/sdk/webchat.min.js"></script><script> Init('?botID=22104&appID=UWHbOGdOy8LpXhyFH5zy', 400, 400, 'https://dvgpba5hywmpo.cloudfront.net/media/image/rywfP58bGrSVFGkGopRBWsOrB', 'bubble', '#53A19A', 90, 90, 62.99999999999999, '', '1', '#FFFFFF', 0);  </script>-->

        <a target="_blank" href="https://api.whatsapp.com/send?phone=5521990555702&text=MultiplierApp%20Suporte.%20Por%20favor%2C%20salve%20esse%20n%C3%BAmero.%20MultiplierApp%20Helpdesk.%20Please%2C%20save%20this%20number."><img src="<?php echo HTTP_ROOT ;?>img/1962073065-Help.png" id="bottom-help-img"></a>

         <script src="<?php echo HTTP_ROOT;?>js/wizard.js"></script>
         <script>
        jQuery( document ).ready(function ($) {
            $('.global12hourdiv').click(function () {
                
                $(this).hide();
            })

            $('.global12hourdivgreen').click(function () {
                $(this).hide();
            })
        });
        </script>

        <?php if ($editor) { ?>
        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.css">
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.min.js"></script>

        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/emojionearea-master/dist/emojionearea.js"></script>


        <script type="text/javascript">
            var ComponentsEditors = function () {
                var handleSummernote = function () {
                    $('.summernote').summernote({height: 300});
                }
                
                return {
                    //main function to initiate the module
                    init: function () {
                        handleSummernote();
                    }
                };
            }();
            jQuery(document).ready(function() {    
                $("#facebook-title").emojioneArea();
                $("#facebook-description").emojioneArea();
                $(".hint2emoji").emojioneArea();
               ComponentsEditors.init(); 
            });
        </script>

        <?php } ?>

        
        <?php /***********************/ ?>

        <?php if ($editor) { ?>
        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css">
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>
        <script type="text/javascript">
            $('.wysihtml5').wysihtml5({
                "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
                "emphasis": true, //Italics, bold, etc. Default true
                "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
                "html": true, //Button which allows you to edit the generated HTML. Default false
                "link": true, //Button to insert a link. Default true
                "image": false, //Button to insert an image. Default true,
                "color": false, //Button to change color of font  
                // "blockquote": true //Blockquote  
                // "size": <buttonsize> //default: none, other options are xs, sm, lg
            });
        </script>
           

        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/updated_stylesheet.css">
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/advanced.js"></script>
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.4.0pre.min.js"></script>
        <script type="text/javascript">
            var editor = new wysihtml5.Editor("wysihtml5", {
                toolbar:        "toolbar",
                stylesheets:    "css/stylesheet.css",
                parserRules:    wysihtml5ParserRules
              });

            var editor = new wysihtml5.Editor("wysihtml51", {
                toolbar:        "toolbar1",
                stylesheets:    "css/stylesheet.css",
                parserRules:    wysihtml5ParserRules
              });

        </script>
        <?php } ?>


        <?php /***********************/ ?>

        <!-- 
        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>/assets/wyswyg/css/froala_editor.css">
        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>/assets/wyswyg/css/froala_style.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>/assets/wyswyg/js/froala_editor.min.js" ></script>
        <script>
            $(function(){
              // $('.mytinymce').froalaEditor({
                // tabSpaces: 4
              // })
            });
        </script>
         -->




	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
    <style>
    body {
    	background: #364150 none repeat;
    }
    .scorllBox{
        max-height: 70px;
        overflow: auto;
        width: 300px;
        text-align: left;
	}
	
	/* Side Menu */

	.page-sidebar .page-sidebar-menu .selected{
		display: block !important;
	}

	.page-sidebar .page-sidebar-menu .selected > ul.sub-menu{
		display: block;
	}


	/* .open{
		display: block;
	} */

	/* .open > ul{
		display: block !important;
	} */

    </style>  
    <!--<style type="text/css" media="print">
        /*@page {
            margin-top: 10;  
        }*/

    </style>-->


    </body>

</html>