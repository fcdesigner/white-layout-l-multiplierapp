<style>

@media(max-width: 450px){
	.portlet.light>.portlet-title>.caption>.caption-subject {
		font-size: 13px;
    	vertical-align: bottom;
	}
}


</style>

<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">

                    <span class="caption-subject bold uppercase"><?php echo __(form_listing); ?> </span> &nbsp;&nbsp;
                    <?php if (isset($authUser['role']) && in_array( $authUser['role'], ['1', '2']) && isset($groupsdata->group_name) ) { ?>
                    	<span class="caption-subject bold uppercase" style = "display: block;"><?php echo __(Account); ?> : <?php echo $groupsdata->group_name; ?></span>
                    <?php } ?>
                </div>
            </div>
            
            <div class="portlet-body">
                <div class="portlet light bordered">
                    <div class="portlet-title">

                    <?php if (isset($authUser['role']) && in_array( $authUser['role'], ['3', '4']) ) { ?>
                        <div class="row">
                            <div class="caption font-dark viewUsertable">
                                <div class="table-scrollable">
                                  <table class="table table-striped table-bordered order-column"> 
                                    <tr> 
                                      <th>Url</th>
                                      <td><?php echo RTMP_DOMAIN.$applicationData['application_name']; ?> </td>  
                                      <th> Stream name</th> 
                                      <td><?php echo $formListing[0]['wowza_source_name']; ?> </td> 
                                    </tr>
                                    <tr>
                                      <th> <?php echo __(Formname); ?></th>
                                      <td><?php echo $formListing[0]['form_name']; ?> </td>  
                                      <th> <?php echo  __(Account); ?></th> 
                                      <td><?php echo $applicationData['group']['group_name']; ?> </td> 
                                    </tr>
                                  </table>
                                </div>                                        
                            </div>
                        </div>                        
                    <?php } ?>



                        <div class="caption font-dark">
                            <div class="btn-group">

                                <?php if(empty($group_id)){
                                    if (count($formListing) <= 0) { ?>
                                        <a href="<?php echo HTTP_ROOT; ?>formbuilder/add" class="nav-link ">
                                            <button id="sample_editable_1_new" class="btn sbold green"> <?php echo __(add_new); ?>
                                                <i class="fa fa-plus"></i>
                                            </button>
                                        </a>
                                    <?php }
                                } ?>

                                <span class="form-icons" style="display:none;">
                                    <a href="" class="nav-link tooltips" id="viewFormLink" data-container="body" data-placement="top" data-original-title="<?php echo __(Open_Form); ?>" target="_blank">
                                        <button id="sample_editable_1_new" class="btn sbold btn-warning"> <i class="fa fa-eye" aria-hidden="true"></i></button>
                                    </a>

                                    <a href="" class="nav-link tooltips" id="editLink" data-container="body" data-placement="top" data-original-title="<?php echo __(edit); ?>" style="display:none;">
                                        <button id="sample_editable_1_new" class="btn sbold btn-info"> <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                    </a>

                                    <a href="" class="nav-link tooltips" id="editFormLink" data-container="body" data-placement="top" data-original-title="<?php echo __(Edit_Form_Info); ?>">
                                        <button id="sample_editable_1_new" class="btn sbold btn-info"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i></button>
                                    </a>
                                    <a href="" class="nav-link tooltips" id="regUserLink" data-container="body" data-placement="top" data-original-title="<?php echo __(registered_users); ?>">
                                        <button id="sample_editable_1_new" class="btn sbold green"> <i class="fa fa-user-plus" aria-hidden="true"></i></button>
                                    </a>
                                    <a href="" class="nav-link tooltips" id="deleteLink" data-container="body" data-placement="top" data-original-title="<?php echo __(delete); ?>" data-target="#myModal" data-toggle="ajaxModal">
                                        <button id="sample_editable_1_new" class="btn sbold btn-danger"> <i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </a>
                                    <?php if (isset($authUser['role']) && in_array( $authUser['role'], ['1', '2']) ) { ?>
                                        <a href="" class="nav-link tooltips" id="planLink" data-container="body" data-placement="top" data-original-title="<?php echo __(changeplan); ?>" data-target="#myModal" data-toggle="ajaxModal">
                                            <button id="sample_editable_1_new" class="btn sbold btn-success"> <i class="fa fa-dollar" aria-hidden="true"></i></button>
                                        </a>
                                        <a href="" class="nav-link tooltips" id="deleteTargetLink" data-container="body" data-placement="top" data-original-title="<?php echo __(delete_target); ?>" data-target="#myModal" data-toggle="ajaxModal">
                                        <button id="sample_editable_1_new" class="btn sbold btn-danger"> <i class="fa fa-trash" aria-hidden="true"></i></button>
                                    </a>
                                    <?php } ?>
                                </span>


                                <!-- <br /> -->
                                <!-- <small><?php echo __(Youcanonlyeditemptydataforms); ?></small> -->
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body form_listing">
                        <table class="table table-striped table-bordered table-hover dataTable" width="100%" id="formBuilderListing">
                            <thead>
                                <tr>
                                    <th><?php echo __(select); ?></th>
                                    <th><?php echo __(SrNo); ?></th>
                                    <th><?php echo __(Formname); ?></th>
                                    <th><?php echo __(OwnerName); ?></th>
                                    <th><?php echo __(CreatedBy); ?></th>
                                    <th><?php echo __(ToEmail); ?></th>
                                    <!-- <th>Embeded Code</th> -->
                                    <th><?php echo __(created); ?></th>
                                    <!-- <th>Actions</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($formListing as $data): ?>
                                <?php //pr($data['user_owner']); ?>
                            <tr>
                                <td><input type="radio" name="selectForm" class="selectForm" form-id="<?php echo $data->form_id; ?>" edit-icon="<?php echo (empty($data->comming_users)) ? 1 : 0; ?>" http-root="<?php echo HTTP_ROOT; ?>"></td>
                                <td><?= $this->Number->format($data->form_id) ?></td>
                                <td><?php echo $data->form_name ?></td>

                                <td><?php echo @$data['user_owner']->name ?></td>
                                <td><?php echo @$data['user_created']->name ?></td>

                                <td><?php echo $data->to_email ?></td>
                                <!-- <td><?php /*echo "&lt;iframe  src='". HTTP_ROOT ."commingusers/view-form?id={$data->form_id}'&gt;&lt;/iframe&gt"; */ ?></td> -->
                                <td><?= date('Y-m-d', strtotime( $data['created_date'] ) ) ?></td>
                                <!-- <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li>
                                                <a target="_blank" href="<?php echo HTTP_ROOT . 'commingusers/view-form?id=' . $data->form_id;?>">  
                                                    <i class="icon-docs"></i> View Form
                                                </a>
                                            </li>
                                            <?php if(empty($data->comming_users)){?>
                                            <li>
                                                <a href="<?php echo HTTP_ROOT.'formbuilder/edit?id='.$data->form_id;?>">                                       
                                                     <i class="icon-docs"></i> Edit
                                                </a>
                                            </li>
                                            <?php } ?>
                                            <li>
                                                <a href="<?php echo HTTP_ROOT.'formbuilder/basic-info?id='.$data->form_id;?>">                                       
                                                     <i class="icon-docs"></i> Basic Info
                                                </a>
                                            </li>                                                 
                                            <li>
                                                <a href="<?php echo HTTP_ROOT.'formbuilder/view-user?id='.$data->form_id;?>">
                                                    <i class="icon-flag"></i>   Form Data 
                                                </a>
                                            </li>   
                                            <li>
                                                <a data-target="#myModal" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/deleteFrom/'.$data->form_id;?>">
                                                    <i class="icon-flag"></i>  Delete 
                                                </a>
                                            </li>                                           
                                        </ul>
                                    </div>
                                </td> -->
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>