<style type="text/css">
  .youtube_official{
    color: #ff0000 !important;
}

.error-hour {
   height: auto;
  width: 1088px;
  font-weight: 400;
  text-align: center;
  font-size: 14px;
  padding: 8px;
}

#connection{
	margin: 10px 0;
}

.line-height{
	line-height: 1.7;
	margin-bottom: 10px;
}

</style>
<?php if ($editor) { ?>
        <link rel="stylesheet" href="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.css">
        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/bootstrap-summernote/summernote.min.js"></script>

        <script type="text/javascript" src="<?php echo HTTP_ROOT;?>assets/global/plugins/emojionearea-master/dist/emojionearea.js"></script>


        <script type="text/javascript">
            var ComponentsEditors = function () {
                var handleSummernote = function () {
                    $('.summernote').summernote({height: 300});
                }
                
                return {
                    //main function to initiate the module
                    init: function () {
                        handleSummernote();
                    }
                };
            }();
            jQuery(document).ready(function() {    
                $("#facebook-title").emojioneArea();
                $("#facebook-description").emojioneArea();
                $(".hint2emoji").emojioneArea();
               ComponentsEditors.init(); 
            });
        </script>

        <?php } ?>
		
        <script> 
        function saveTD(){
          $('#tdModal').modal('show');
        }
        function confirmTD() { 
              document.getElementById("basicInfo").submit();
        } 
    </script> 
<div class="row">
<?php 

  $disable = "";
  if($isStop) { 
   $disable = "disabled";
  }
  $isPAL = false;
  // if(isset($PlanData[0]['auto_limit']) &&  $PlanData[0]['auto_limit'] > 0){
  //   $isPAL =true;
  // }
?>
<!----Modal start---->
<div class="container mt-3">
  <!-- The Modal -->
  <div class="modal fade" id="tdModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><?php echo __(Post_Title_and_Description); ?></h4>
          <button type="button" class="close" data-dismiss="modal">×</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <?php echo __(t_n_d_confirm); ?>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal"><?php echo __(t_n_d_no); ?></button>
          <button type="button" class="btn btn-success" onclick="confirmTD()"><?php echo __(t_n_d_yes); ?></button>
        </div>
        
      </div>
    </div>
  </div>
  
</div>
<!-----Modal end--->
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">                             
            <div class="portlet-title">
                <div class="caption font-dark viewUsertable">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo __(registered_users); ?> </span>
                    <p></p>

                   
                    <div class="table-scrollable">
                      <table class="table table-striped table-bordered order-column"> 
                        <tr> 
                          <th>Url</th>
                          <td><?php echo RTMP_DOMAIN.$applicationData['application_name']; ?> </td>  
                          <th> <?php echo __(stream_name); ?></th> 
                          <td><?php echo $FormSingleData['wowza_source_name']; ?> </td> 
                        </tr>
                        <tr>
                          <th> <?php echo __(Formname); ?></th> 
                          <td><?php echo $FormSingleData['form_name']; ?> </td>  
                          <th> <?php echo  __(Account); ?></th> 
                          <td><?php echo $applicationData['group']['group_name']; ?> </td> 
                        </tr>
                        <tr>
                          <th>Player</th> 
                          <td colspan="3"><?php echo HTTP_ROOT."player/index.html?appname=".$applicationData['application_name']."&streamname=".$FormSingleData['wowza_source_name']; ?> </td>
                        </tr>
                       
                      </table>
                    </div>                                        
                </div>
                
                <!--<a id="refresh_view" href="<?php echo PROTOCOL_TYPE . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"  class="btn btn-primary btn-sm top-right-btn"  ><?php echo __(Refresh); ?></a>-->
                <!--<a href="<?php echo HTTP_ROOT ;?>formbuilder/broadcast/<?php echo $formId; ?>" data-target="#myModal" data-toggle="ajaxModal" class="btn btn-warning btn-sm top-right-btn" style="float: right; margin-right: 1%; margin-top: 3px;"><?php echo __(broadcasts); ?></a>-->
                <?php if ($authUser['role'] == 1){ ?>
                <a target="_blank" <?php echo $disable; ?> href="<?php echo HTTP_ROOT ;?>cron/updateAll/<?php echo $formId; ?>" class="btn btn-danger btn-sm top-right-btn" style="float: right; margin-right: 1%; margin-top: 3px;"><?php echo __(Resetformtargets); ?></a>
                <?php } ?>
                <div class="clearfix"></div>
                     <div class="row">
                       <div class="col-md-12 line-height"><?= __('TARGET_REFESH_ALT'); ?>  <a id = "connection" <?php echo $disable; ?> target="_blank" href="<?php echo HTTP_ROOT ;?>cron/updateAll/<?php echo $formId; ?>" class="btn btn-danger btn-sm top-right-btn" style=" margin-right: 1%;"><?php echo __(PRECN); ?></a></div> 
                        </div>
					<div class="row">
                       <div class="col-md-12 line-height"><?= __('suggested_setting'); ?><span data-toggle="tooltip" data-placement="bottom" title="<?php echo __(suggested_setting_tooltip); ?>"><i class="fa fa-question-circle"></i></span><?= __('suggested_setting_msg'); ?><a" style=" margin-right: 1%; margin-top: 3px;"></a></div> 
                        </div>
				<!--<div class="row">
				<div class="col-md-12"><?= __('editTnDt'); ?><?= __('editTnDMsg'); ?><a" style=" margin-right: 1%; margin-top: 3px;"></a> 
				<a target="_blank" href="<?php echo HTTP_ROOT."formbuilder/basic-info?id=".$formId ;?>" class="nav-link tooltips" id="editFormLink" data-container="body" data-placement="top" data-original-title="<?php echo __(edit)." t&d"; ?>">
                  <button id="sample_editable_1_new" style="margin-top: 4px; margin-left: 0px;" class="btn btn-primary btn-sm top-right-btn"> <?php echo __(editTnD); ?> </button>
                </a></div>
				</div>-->
        <!--------------------Start Title and Description--------------------->
        <div class="row">
	 <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
                <div class="x_title" style="display:none;">
                    <h4><?php echo __(Basic_Details); ?><small></small></h4>
					<div class="clearfix"></div>
			    </div>
				<div class="x_content">
					<?php echo $this->Form->create('$basicInfo', [
						'url' => ['controller' => 'Formbuilder', 'action' => 'basic_info'],
						'class'=>'form-horizontal form-label-left',
						'id'=>'basicInfo',
						'enctype'=>'multipart/form-data',
						'novalidate'=>'novalidate',
						 'autocomplete'=>'off',
						
					]);?>
					
				<!---------------------->
					<div style="display:none;">
				<!---------------------->
					
					<input type="hidden" value="<?php if(!empty($formInfo->form_id)){echo @$formInfo->form_id;} ?>" name="form_id" id="form_id">
					
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(stream_name); ?><span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <?php echo $this->Form->input('wowza_source_name',
							 [
							 'label'=>false,
							 'required' => true,
							 'class'=>'form-control col-md-7 col-xs-12 ',
							 'value'=>$formInfo->wowza_source_name]);?>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Formname); ?><span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <?php echo $this->Form->input('form_name',
							 [
							 'label'=>false,
							 'required' => true,
							 'class'=>'form-control col-md-7 col-xs-12 ',
							 'value'=>@$formInfo->form_name != '' ?$formInfo->form_name:'']);?>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(ToEmail); ?> <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<?php 
						 echo $this->Form->input('to_email',[
								'label' => false,
								'required' => true,
								'class'=>'form-control col-md-7 col-xs-12',
								'value'=>@$formInfo->to_email != '' ?$formInfo->to_email:'']);
						 ?>

						 <p><?php echo __(enternotification)?></p>
						 </div>
						 
					</div>
					<hr />
					<!---------------------->
					</div>	
					<!---------------------->
					<!---------------------->
					<div style="display:none;">
					<!---------------------->
					<h4><?php echo __(Callback_URL); ?></h4>
					
					<div class="item form-group">
						<?php $formUrl = HTTP_ROOT."commingusers/view-form?id=".$formInfo->form_id;
						
						$showFromSuccUrl = false;
						if ( ($formUrl == $formInfo->success_url ) || ( $formInfo->success_url =='' )  )  {
							$showFromSuccUrl = true;
						}
						 ?>
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Success_URL)?><span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12 m-b-sm">
							<label class="radio-inline">
						      <input type="radio" value="form" name="success_radio" <?php if ( $showFromSuccUrl ) { echo 'checked="checked"'; } ?> ><?php echo __(Form); ?>
						    </label>
						    <label class="radio-inline">
						      <input type="radio" value="other" name="success_radio" <?php if ( !$showFromSuccUrl ) { echo 'checked="checked"'; } ?> ><?php echo __(Other); ?>
						    </label>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="hidden" id="form_url" value="<?php echo $formUrl; ?>">
						<input type="hidden" id="other_url" value="<?php echo $formInfo->success_url; ?>">
							 <?php echo $this->Form->input('success_url',
							 [
							 'label'=>false,
							 'required' => true,
							 'class'=>'form-control col-md-7 col-xs-12 ',
							 'id'=>'success_url_in',
							 'value'=>@$formInfo->success_url != '' ?$formInfo->success_url: $formUrl]);?>
						</div>
					</div>
					
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category"><?php echo __(Error_Url)?><span class="required">*</span>
						</label>
						<?php 
							$showFromErrorUrl = false;
							if ( ($formUrl == $formInfo->error_url ) || ( $formInfo->error_url =='' )  )  {
								$showFromErrorUrl = true;
							}
						 ?>
						 <div class="col-md-6 col-sm-6 col-xs-12 m-b-sm">
							<label class="radio-inline">
						      <input type="radio" value="form" name="error_radio" <?php if ( $showFromErrorUrl ) { echo 'checked="checked"'; } ?> ><?php echo __(Form)?> 
						    </label>
						    <label class="radio-inline">
						      <input type="radio" value="other" name="error_radio" <?php if ( !$showFromErrorUrl ) { echo 'checked="checked"'; } ?> ><?php echo __(Other); ?>
						    </label>
						</div>
						<div class='col-md-6 col-sm-6 col-xs-12'>
						<input type="hidden" id="error_other_url" value="<?php echo $formInfo->error_url; ?>">
						 <?php /* echo $this->Form->radio(
								'faqs.faq_type',[												
								["value"=>'sitter','text'=>"Sitter" ,'checked'],
								["value"=>'guest','text'=>"Guest"]
								]); */
							echo $this->Form->input('error_url', [
							'label' => false,											
							'required' => true,
							'id'=>'error_url_in',
							'class'=>'form-control col-md-7 col-xs-12 ',
							'value'=>@$formInfo->error_url != '' ?$formInfo->error_url: $formUrl ]);	
								 
						?>
						</div>
					</div>

					<?php $form_img1 = $formInfo->form_img1 ? $formInfo->form_img1 : 'dummy_image.png';  ?>
					<?php $form_img2 = $formInfo->form_img2 ? $formInfo->form_img2 : 'dummy_image.png';  ?>

					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category"><?php echo __(image); ?> 1 <span class="required"></span>
						</label>
						<div class='col-md-6 col-sm-6 col-xs-12'>
						 <input type="file" name="img1" id="img1">										 
						</div>
						<div class='col-md-3 col-sm-3 col-xs-12'>
							<?php if (!empty($formInfo->form_img1)): ?>
								<span data-img='1' class="delete_image clear_image"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
							<?php endif ?>

							<?php if (strpos($form_img1, 'https') !== false) { ?>
							    <img src="<?php echo $form_img1 ; ?>" height="100" width="100" class="form_image">
							<?php }else{ ?>
								<img src="<?php echo HTTP_ROOT.'img/uploads/formimg/' . $form_img1 ; ?>" height="100" width="100" class="form_image">
							<?php } ?>							
						</div>
					</div>

					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category"><?php echo __(image); ?> 2 <span class="required"></span>
						</label>
						<div class='col-md-6 col-sm-6 col-xs-12'>
						 <input type="file" name="img2" id="img2">										 
						</div>
						<div class='col-md-3 col-sm-3 col-xs-12'>
						<?php if (!empty($formInfo->form_img2)): ?>
						<span data-img='2' class="delete_image clear_image"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
						<?php endif ?>
						<img src="<?php echo HTTP_ROOT	.'img/uploads/formimg/'. $form_img2 ; ?>" height="100" width="100" class="form_image">
						</div>
					</div>
					<hr />
					<!---------------------->
					</div>	
					<!---------------------->
					<!---------------------->
					<div style="display:none;">
					<!---------------------->
					<h4><?php echo __(Form_Title_and_Description); ?></h4>	
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Form_Title); ?><span class="required">*</span>
						</label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<?php // editorToolbar(); ?>
							 <?php echo $this->Form->input('form_title',
							 [
							 'label'=>false,
							 'class'=>'form-control col-md-7 col-xs-12 summernote',
							 'id'	=> '',
							 'type' => 'textarea',
							 'value'=>$form_title]);?>
						</div>
					</div>
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Form_Description); ?><span class="required">*</span>
						</label>
						<div class="col-md-8 col-sm-8 col-xs-12">
							<?php // editorToolbar('1'); ?>
							 <?php echo $this->Form->input('form_description',
							 [
							 'label'=>false,
							 'class'=>'form-control col-md-7 col-xs-12 summernote',
							 'id'	=> '',
							 'type' => 'textarea',
							 'value'=>$form_description_lang]);?>
						</div>
					</div>


					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Show_Title_Description); ?> <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <?php echo $this->Form->radio(
								    't_and_d_status',
								    [
								        ['value' => '1', 'text' => __(yes), 'style' => 'color:red;'],
								        ['value' => '0', 'text' => __(no), 'style' => 'color:blue;'],
								    ],
								    [
									    //'label'=>false,
										//'required' => true,
										'class'=>'form-control col-md-7 col-xs-12 ',
										'value'=>@$formInfo->t_and_d_status
									]);
							?>
						</div>
					</div>


					<?php /****************************/ ?>
      				<hr />
      			<!---------------------->
				</div>	
				<!---------------------->
				
          <div class="row" style="margin-bottom: 20px;margin-top:9px;">
                       <div class="col-md-12"><b><?= __('Post_Title_and_Description'); ?></b> : <?= __('Post_Title_and_msg'); ?><a" style=" margin-right: 1%; margin-top: 3px;"></a></div> 
                        </div>

					 <div class="item form-group" style="display:none;">
							<div class="col-md-6 col-sm-6 col-xs-12 text-center">
								<p><b><?php echo __(Individual_Titles_and_Descriptions_set); ?> </b></p>
							</div>
				    </div> 
				    <div class="item form-group" style="display:none;">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Change_to_Global); ?><span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <?php echo $this->Form->radio(
								    'fb_td_status',
								    [
								        ['value' => '1', 'text' => __(yes), 'style' => 'color:red;', 'class'=>'show_hide_TD'],
								        ['value' => '0', 'text' => __(no), 'style' => 'color:blue;','class'=>'show_hide_TD'],
								    ],
								    [
									    //'label'=>false,
										//'required' => true,
										'class'=>'form-control col-md-7 col-xs-12 ',
										'value'=>@$formInfo->fb_td_status != ""?@$formInfo->fb_td_status:"0"
									]);
							?>
						</div>
					</div>			
					<div class="item form-group ">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(PostTitle); ?>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <?php echo $this->Form->input('facebook_title',
							 [
							 'label'=>false,
							 'class'=>'form-control col-md-7 col-xs-12 ',
							 'value'=>@$formInfo->facebook_title != '' ?$formInfo->facebook_title:'']);?>
						</div>
					</div>
					<div class="item form-group ">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(PostDescription); ?>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<?php echo $this->Form->input('facebook_description', [
							'label'=>false,
							'class'=>'form-control col-md-7 col-xs-12',
							'type' => 'textarea',
							'value'=>@$formInfo->facebook_description != '' ?$formInfo->facebook_description:'']);?>
						</div>
					</div>
					<hr />

					<?php if ($authUser['role'] == 1 || $authUser['role'] == 2){ ?>
						<h4><?php echo __(Hide_share_option); ?></h4>
						<div class="item form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Hide_share_option); ?><span class="required">*</span>
							</label>
							<div class="col-md-6 col-sm-6 col-xs-12">
								 <?php echo $this->Form->radio(
									    'hide_share_option',
									    [
									        ['value' => '1', 'text' => __(yes), 'style' => 'color:red;'],
									        ['value' => '0', 'text' => __(no), 'style' => 'color:blue;'],
									    ],
									    [
										   'class'=>'form-control col-md-7 col-xs-12 ',
											'value'=>@$formInfo->hide_share_option
										]);
								?>
							</div>
						</div>
					<?php } ?>


					<?php if ($authUser['role'] == 1){ ?>

					<hr />

					<h4><?php echo __(Duplicate); ?></h4>

					 <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">  <?php echo __(Duplicate_To_another_Form); ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                             <?php echo $this->Form->radio(
                                    'linked_status',
                                    [
                                        ['value' => '1', 'text' => __(yes), 'style' => 'color:red;', 'class'=>'duplicate_show_hide_TD'],
                                        ['value' => '0', 'text' => __(no), 'style' => 'color:blue;','class'=>'duplicate_show_hide_TD'],
                                    ],
                                    [
                                        'class'=>'form-control col-md-7 col-xs-12 ',
                                        'value'=>@$formInfo->linked_status != ""?@$formInfo->linked_status:"0"
                                    ]);
                            ?>
                        </div>
                    </div>          
                    <div class="item form-group duplicate_hide">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="linked_to"><?php echo __(links_to); ?>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                             <?php echo $this->Form->input('linked_to',
                             [
                             'label'=>false,
                             'class'=>'form-control col-md-7 col-xs-12 ',
                             'value'=>@$formInfo->linked_to != '' ?$formInfo->linked_to:'']);?>
                        </div>
                    </div>


                    <?php } ?>

                    <?php if ($authUser['role'] == 1 || $authUser['role'] == 2) { ?>
						<input type="hidden" id="http_root" value="<?php echo HTTP_ROOT; ?>">
                    <?php } ?>

					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3">
							<!--<button type="button"  class="btn btn-primary" onclick="window.history.go(-1);"  ><?php echo __(cancel); ?></button>-->
							<button id="send" type="button" class="btn btn-success" onclick="saveTD();"><?php echo __(submit); ?></button>
						</div>
					</div>

                    <?php echo $this->form->end(); ?>
                <!-- end form -->


                </div>
            </div>
        </div>
				</div>

                <?php if(($authUser['role'] == 1 || $authUser['role'] == 2) && count($allApps) > 1){ ?>
                  <br/>
                  
                  <div class="row">
                    <div class="col-md-2">
                      <select id="app_select" class="form-control" http-root="<?php echo HTTP_ROOT; ?>">
                        <option value=""><?php echo __(select); ?></option>
                        <?php foreach($allApps as $app){ ?>
                          <option value="<?php echo $app->id; ?>"><?php echo $app->application_name; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php } ?>
                <br/>
                <div class="row">
                  <div class="col-md-12">
                    <?php //reachedTag() ?>
                    <?= __('dashboard_emo_note',[HTTP_ROOT.'commingusers/view-broadcasts']); ?>
                  </div>
                </div>
				<div class="row" style="margin-bottom:15px; color:red;">
                  <div class="col-md-10">
				  <?php if($error_target_count > 0) { ?>
				  	<div> <?php echo __(social_error_profile_msg_1)." ". __($error_target_count)." ".  __(social_error_profile_msg_2)." "?><a  target="_blank" href="<?php echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $formId; ?>" class="nav-link nav-toggle"><?php echo  __(social_error_profile_link_1)." "?></a> <?php echo  __(social_error_profile_msg_3)." "?><a target="_blank" href="<?php echo HTTP_ROOT; ?>formbuilder/error-profiles?id=<?php echo $formId; ?>" class="nav-link nav-toggle"> <?php echo  __(social_error_profile_link_2); ?>.</a></div> 
				  <?php } ?>
				  </div>
				</div>
                <div class="row">
                  <div class="col-md-8">
                    <?php /*<table class="table table-bordered">
                      <p></p>
                      <tr>( <?php echo __(real_time); ?> )</tr>
                      <tr>
                        <th><?php echo __(reached); ?> <span data-toggle="tooltip" title="<?php echo __(sumallreached); ?>"><i class="fa fa-question-circle"></i></span></th>
                        <td class=""><?php echo number_format($followersCount, 0, 0, '.'); ?></td>
                        <th>Waiting <span data-toggle="tooltip" title="<?php echo __(sumallready); ?>"><i class="fa fa-question-circle"></i></span></th>
                        <td class=""><?php echo $waitingCount; ?></td>
                        <th><?php echo __(Error); ?> <span data-toggle="tooltip" title="<?php echo __(sumallnotready); ?>"><i class="fa fa-question-circle"></i></span></th>
                        <td class=""><?php echo $errorCount; ?> </td>
                       <!-- <th><?php echo __(active); ?> <span data-toggle="tooltip" title="<?php echo __(sumallnotreadygoing); ?>"><i class="fa fa-question-circle"></i></span></th>-->
                        <!-- <td class=""><?php //echo $activeCount; ?></td> -->
                      </tr>
                    </table> */ ?>
                    <a data-target="#myModal" class="btn" style="background-color: green;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/detailed-intreaction/'.$FormSingleData->form_id; ?>">
                          <?php echo __(checkstatus); ?> 
                    </a>
                    <a target="_blank" href="<?php echo HTTP_ROOT ;?>commingusers/view-broadcasts" class="btn btn-danger " style=" margin-right: 1%; margin-top: 3px;"><?php echo __(seereports); ?></a>
                  </div> 
                </div>
              </div>
              <button <?php echo $disable; ?> data-target="#myModal" class="btn btn-margin" style="background-color: black;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/delete-all/'.$FormSingleData->form_id;?>">
                          <?php echo __(deleteall); ?> 
                </button>
                 <button <?php echo $disable; ?> data-target="#myModal" class="btn btn-margin" style="background-color: red;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/delete-all-error/'.$FormSingleData->form_id;?>">
                          <?php echo __(deleteallerror); ?> 
                </button>
                 <button <?php echo $disable; ?> data-target="#myModal" class="btn btn-margin" style="background-color: green;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/active-all/'.$FormSingleData->form_id;?>">
                          <?php echo __(trunon); ?> 
                </button>
                 <button <?php echo $disable; ?> data-target="#myModal" class="btn btn-margin" style="background-color: red;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/deactive-all/'.$FormSingleData->form_id;?>">
                          <?php echo __(turnoff); ?> 
                </button>
              <div class="portlet-body portlet-body ">                
                       
                       
                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="target_listing">                    
                    <thead>
                        <tr>
                          <th> # </th>
                          <?php if($isPAL == true ) { ?> 
                          <th class='alignCenter'><?php echo __(premium_live_status); ?> <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('premium_msg'); ?>"><i class="fa fa-question-circle"></i></span></th>
                          <?php } ?>
                         <th class='alignCenter'><?php echo __(live_status); ?><span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('live_msg'); ?>"><i class="fa fa-question-circle"></i></span></th>
                         
                          <th class='alignCenter'><?php echo __(type); ?></th>
                          <th class='alignCenter'><?php echo __(image);?></th>
                          <th class='alignCenter'><?php echo __(Target); ?></th>
                          <th class='alignCenter'><?php echo __(Privacy);?></th>
                          <th class='alignCenter'><?php echo __(name); ?></th>
                          <th class='alignCenter'>ID</th>
                            <th class='alignCenter'><?php echo __(formRegistrationName); ?></th>
                            <th class='alignCenter'><?php echo __(formRegistrationEmail); ?></th>
                            <th class='alignCenter'><?php echo __(formRegistrationCountry); ?></th>
                            <th class='alignCenter'><?php echo __(formRegistrationMobile); ?></th>
                           
                            
                          <th class='alignCenter'><?php echo __(TD);?></th>
                          <th class='alignCenter'><?php echo __(Status);?></th>
                          <th class='alignCenter'><?php echo __(reach); ?></th>
                          <th class='alignCenter'><?php echo __(last_action); ?></th>
                          
                          <th class='alignCenter'><?php echo __(last_update); ?></th>
                          
                          <th>
                            <img src="<?php echo HTTP_ROOT ;?>img/total.png" width="25" data-text="TOTAL">
                            <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('total_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                          </th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/comment.png" width="30" data-text="COMMENT"></th>
                          <!-- <th>
                            <img  src="<?php echo HTTP_ROOT ;?>img/view.png" width="38" data-text="VIEW">
                            <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('view_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                          </th> -->
                          <th><img src="<?php echo HTTP_ROOT ;?>img/like.png" width="35" data-text="LIKE"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/love.png" width="30" data-text="LOVE"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/haha.png" width="30" data-text="HAHA"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/wow.png" width="30" data-text="WOW"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/sad.png" width="30" data-text="SAD"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/angry.png" width="30" data-text="ANGRY"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/share.png" width="30" data-text="SHARE"></th>
                          
                          <?php /*if(!empty($jsonCommingData)){
                          // getting form body in array form
                          $form_body_enoceded  = json_decode(json_decode($FormSingleData['form_body']), true); 
                          ?>
                          <?php   foreach($jsonCommingData as $key =>$value){ ?>
                          <?php if($key == "groups" || $key == "wowza_aplication_id"|| $key == "wowza_aplication" || $key == "wowza_stream_source" ){ continue; }   ?>
                          <?php $key = $form_body_enoceded[array_search($key, array_column($form_body_enoceded, 'name'))]['label'];  ?>
                              <th class='alignCenter'> <?php echo ucfirst($key); ?>  </th>
                          <?php } } */?>

                            <th class='alignCenter'><?php echo  __(actions); ?></th>

                        </tr>
                    </thead>
                         
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>     
<div class="loader"></div>

<style>
.dataTables_scrollBody {
  overflow-y: visible !important;
  overflow-x: initial !important;
}
.dataTables_scrollHead {
  overflow: visible !important;
}
.fullwid{

   width:45%;
   text-align: center;
}
.alignCenter{
    text-align:center;
}
.toggler{
    display: none;
}
.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../img/loader.gif') 50% 50% no-repeat rgb(8,8,8);
    opacity: .8;
    display: none;
}
#refresh_view {
 float: right;

margin-top: 3px;
}

.btn-margin{
	margin: 10px 0;
}
@media screen and (max-width: 640px){
 div.dataTables_length select {
  width: 145px!important;
    float: right;
    margin-left: 9px;
} 
.dataTables_wrapper .dt-buttons a{padding: 6px 35px!important;}
}
@media screen and (max-width:480px){
.bd_info_tb {
    min-width: 600px;
}
.bd_tab_box {
    overflow: scroll;
    overflow-y: hidden;
}
.dataTables_paginate{
    overflow: scroll;
    overflow-y: hidden;  
}
.dataTables_paginate .pagination{
    min-width: 500px;
    text-align: left;

}
}
@media screen and (max-width: 360px)
{
.top-right-btn{
  padding: 5px 7px;
}
}

</style>