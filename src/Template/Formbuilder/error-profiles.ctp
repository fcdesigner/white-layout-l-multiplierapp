<style type="text/css">
  .youtube_official{
    color: #ff0000 !important;
}

.error-hour {
   height: auto;
  width: 1088px;
  font-weight: 400;
  text-align: center;
  font-size: 14px;
  padding: 8px;
    }

.btn{
	margin: 10px 0;
}

.top-right-btn {
    padding: 7px !important;
}

</style>

<div class="row">
<?php 
  $disable = "";
  if($isStop) { 
   $disable = "disabled";
  }
  $isPAL = false;
  // if(isset($PlanData[0]['auto_limit']) &&  $PlanData[0]['auto_limit'] > 0){
  //   $isPAL =true;
  // }
?>

    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">                             
            <div class="portlet-title">
                <div class="caption font-dark viewUsertable">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo __(error_profiles); ?> </span>
                    <p></p>                           
                </div>
                <div class="row">
                    <div class="col-md-12"><?php echo __(error_profiles_message); ?> 
                    </div> 
                </div>
                
                <?php if ($authUser['role'] == 1){ ?>
                   <!-- <a target="_blank" <?php echo $disable; ?> href="<?php echo HTTP_ROOT ;?>cron/updateAllErrorTargetsWithErrorFilter/<?php echo $formId; ?>" class="btn btn-danger btn-sm top-right-btn" style="float: right; margin-right: 1%; margin-top: 3px;"><?php echo __(Resetformtargets); ?></a>-->
                <?php } ?>
                <div class="clearfix"></div>
                <!--<div class="row">
                    <div class="col-md-12"><?= __('TARGET_REFESH_ALT'); ?>  <a <?php echo $disable; ?> target="_blank" href="<?php echo HTTP_ROOT ;?>cron/updateAllErrorTargetsWithErrorFilter/<?php echo $formId; ?>" class="btn btn-danger btn-sm top-right-btn" style=" margin-right: 1%; margin-top: 3px;"><?php echo __(PRECN); ?></a>
                    </div> 
                </div>-->
            </div>
                    
            <div class="portlet-body portlet-body "> 
				<div class = "table-scrollable">
					<table class="table table-striped table-bordered table-hover table-checkable order-column" id="">
						<thead>
							<tr>
							<th class="alignCenter" ></th>
								<?php if($isPAL == true ) { ?> 
								<th class='alignCenter'><?php echo __(premium_live_status); ?> <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('premium_msg'); ?>"><i class="fa fa-question-circle"></i></span></th>
								<?php } ?>
								<th class='alignCenter'><?php echo __(live_status); ?></th>
								<th class=''><?php echo __(error_type);?></th>
								<th class='alignCenter'><?php echo __(total_target_no); ?></th>

							</tr>
						</thead>
						<tbody>
						<?php //echo count($CommingUserData); ?>
						<!--First filter---> 
							<?php if(!empty($CommingUserData1)) {
								$first = 0;
								for($i=0; $i<count($CommingUserData1); $i++){
									$first++;
								}
							?> 
								<tr>
								<td></td>
									<td class='alignCenter'>
										<?php 
											echo $this->Form->checkbox('error_targets_live_status', array(
											'value' => 1,
											'id' => "error_targets_live_status",
											'data-id' => $CommingUserData1[0]->form_id,
											'data-val' => "Session has expired",
											'class'=>"make-switch",
											'data-on-color' => "success",
											'data-off-color' => "danger",
											'checked'=>@$CommingUserData1[0]->live_status,
											'hiddenField' => 0,
											));
										?>
									</td>
									<td class=''><div style="word-wrap: break-word;width: 500px;"><?php echo "Session has expired."; ?></div></td>
									<td class='alignCenter'><?php echo $first; ?></td>
								</tr>
							<?php } ?>

							<!--second filter--->               
							<?php if(!empty($CommingUserData2)) {
								$second = 0;
								for($i=0; $i<count($CommingUserData2); $i++){
									$second++;
								}
							?> 
								<tr>
								<td></td>
									<td class='alignCenter'>
										<?php 
											echo $this->Form->checkbox('error_targets_live_status', array(
											'value' => 1,
											'id' => "error_targets_live_status",
											'data-id' => $CommingUserData2[0]->form_id,
											'data-val' => "Unsupported post request",
											'class'=>"make-switch",
											'data-on-color' => "success",
											'data-off-color' => "danger",
											'checked'=>@$CommingUserData2[0]->live_status,
											'hiddenField' => 0,
											));
										?>
									</td>
									<td class=''><div style="word-wrap: break-word;width: 500px;"><?php echo "Unsupported post request."; ?></div></td>
									<td class='alignCenter'><?php echo $second; ?></td>
								</tr>
							<?php } ?>

							<!--Third filter--->               
							<?php if(!empty($CommingUserData3)) {
								$third = 0;
								for($i=0; $i<count($CommingUserData3); $i++){
									$third++;
								}
							?> 
								<tr>
								<td></td>
									<td class='alignCenter'>
										<?php 
											echo $this->Form->checkbox('error_targets_live_status', array(
											'value' => 1,
											'id' => "error_targets_live_status",
											'data-id' => $CommingUserData3[0]->form_id,
											'data-val' => "violates Facebook policies",
											'class'=>"make-switch",
											'data-on-color' => "success",
											'data-off-color' => "danger",
											'checked'=>@$CommingUserData3[0]->live_status,
											'hiddenField' => 0,
											));
										?>
									</td>
									<td class=''><div style="word-wrap: break-word;width: 500px;"><?php echo "You recently posted something that violates Facebook policies, so you're temporarily blocked from using this feature."; ?></div></td>
									<td class='alignCenter'><?php echo $third; ?></td>
								</tr>
							<?php } ?>

							<!--Fourth filter--->               
							<?php if(!empty($CommingUserData4)) {
								$fourth = 0;
								for($i=0; $i<count($CommingUserData4); $i++){
									$fourth++;
								}
							?> 
								<tr>
								<td></td>
									<td class='alignCenter'>
										<?php 
											echo $this->Form->checkbox('error_targets_live_status', array(
											'value' => 1,
											'id' => "error_targets_live_status",
											'data-id' => $CommingUserData4[0]->form_id,
											'data-val' => "The user has not authorized",
											'class'=>"make-switch",
											'data-on-color' => "success",
											'data-off-color' => "danger",
											'checked'=>@$CommingUserData4[0]->live_status,
											'hiddenField' => 0,
											));
										?>
									</td>
									<td class=''><div style="word-wrap: break-word;width: 500px;"><?php echo "The user has not authorized."; ?></div></td>
									<td class='alignCenter'><?php echo $fourth; ?></td>
								</tr>
							<?php } ?>

							<!--Fifth filter--->               
							<?php if(!empty($CommingUserData5)) {
								$fifth = 0;
								for($i=0; $i<count($CommingUserData5); $i++){
									$fifth++;
								}
							?> 
								<tr>
								<td></td>
									<td class='alignCenter'>
										<?php 
											echo $this->Form->checkbox('error_targets_live_status', array(
											'value' => 1,
											'id' => "error_targets_live_status",
											'data-id' => $CommingUserData5[0]->form_id,
											'data-val' => "the user changed their password",
											'class'=>"make-switch",
											'data-on-color' => "success",
											'data-off-color' => "danger",
											'checked'=>@$CommingUserData5[0]->live_status,
											'hiddenField' => 0,
											));
										?>
									</td>
									<td class=''><div style="word-wrap: break-word;width: 500px;"><?php echo "The session has been invalidated because the user changed their password or Facebook has changed the session for security reasons."; ?></div></td>
									<td class='alignCenter'><?php echo $fifth; ?></td>
								</tr>
							<?php } ?>

							<!--Sixth filter--->               
							<?php if(!empty($CommingUserData6)) {
								$sixth = 0;
								for($i=0; $i<count($CommingUserData6); $i++){
									$sixth++;
								}
							?> 
								<tr>
								<td></td>
									<td class='alignCenter'>
										<?php 
											echo $this->Form->checkbox('error_targets_live_status', array(
											'value' => 1,
											'id' => "error_targets_live_status",
											'data-id' => $CommingUserData6[0]->form_id,
											'data-val' => "#200",
											'class'=>"make-switch",
											'data-on-color' => "success",
											'data-off-color' => "danger",
											'checked'=>@$CommingUserData6[0]->live_status,
											'hiddenField' => 0,
											));
										?>
									</td>
									<td class=''><div style="word-wrap: break-word;width: 500px;"><?php echo "(#200) Permissions error"; ?></div></td>
									<td class='alignCenter'><?php echo $sixth; ?></td>
								</tr>
							<?php } ?>
						</tbody>
								
					</table>
				</div>
                                        <div style="margin-bottom : 10px;">
                <button <?php echo $disable; ?> data-target="#myModal" class="btn" style="background-color: black;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/delete-all-error-target/'.$FormSingleData->form_id;?>">
                          <?php echo __(deleteall); ?> 
                </button>
                 <!--<button <?php echo $disable; ?> data-target="#myModal" class="btn" style="background-color: red;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/delete-all-error/'.$FormSingleData->form_id;?>">
                          <?php echo __(deleteallerror); ?> 
                </button>-->
                 <button <?php echo $disable; ?> data-target="#myModal" class="btn" style="background-color: green;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/active-all-error-target/'.$FormSingleData->form_id;?>">
                          <?php echo __(trunon); ?> 
                </button>

                 <button <?php echo $disable; ?> data-target="#myModal" class="btn" style="background-color: red;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'formbuilder/deactive-all-error-target/'.$FormSingleData->form_id;?>">
                 <?php echo __(turnoff); ?> 
                </button>

                <a <?php echo $disable; ?> target="_blank" href="<?php echo HTTP_ROOT ;?>cron/updateAllErrorTargetsWithErrorFilter/<?php echo $formId; ?>" class="btn btn-danger btn-sm top-right-btn" style=" margin-right: 1%;"><?php echo __(PRECN); ?></a>
                
                                    </div>

                <!--table to show all targets--->
                 <table class="table table-striped table-bordered table-hover table-checkable order-column" id="error_target_listing">                    
                    <thead>
                        <tr>
                          <th> # </th>
                          <?php if($isPAL == true ) { ?> 
                          <th class='alignCenter'><?php echo __(premium_live_status); ?> <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('premium_msg'); ?>"><i class="fa fa-question-circle"></i></span></th>
                          <?php } ?>
                         <th class='alignCenter'><?php echo __(live_status); ?><span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('live_msg'); ?>"><i class="fa fa-question-circle"></i></span></th>
                         
                          <th class='alignCenter'><?php echo __(type); ?></th>
                          <th class='alignCenter'><?php echo __(image);?></th>
                          <th class='alignCenter'><?php echo __(Target); ?></th>
                          <th class='alignCenter'><?php echo __(Privacy);?></th>
                          <th class='alignCenter'><?php echo __(name); ?></th>
                          <th class='alignCenter'>ID</th>
                            <th class='alignCenter'><?php echo __(formRegistrationName); ?></th>
                            <th class='alignCenter'><?php echo __(formRegistrationEmail); ?></th>
                            <th class='alignCenter'><?php echo __(formRegistrationCountry); ?></th>
                            <th class='alignCenter'><?php echo __(formRegistrationMobile); ?></th>
                           
                            
                          <th class='alignCenter'><?php echo __(TD);?></th>
                          <th class='alignCenter'><?php echo __(Status);?></th>
                          <th class='alignCenter'><?php echo __(reach); ?></th>
                          <th class='alignCenter'><?php echo __(last_action); ?></th>
                          
                          <th class='alignCenter'><?php echo __(last_update); ?></th>
                          
                          <th>
                            <img src="<?php echo HTTP_ROOT ;?>img/total.png" width="25" data-text="TOTAL">
                            <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('total_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                          </th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/comment.png" width="30" data-text="COMMENT"></th>
                          
                          <th><img src="<?php echo HTTP_ROOT ;?>img/like.png" width="35" data-text="LIKE"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/love.png" width="30" data-text="LOVE"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/haha.png" width="30" data-text="HAHA"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/wow.png" width="30" data-text="WOW"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/sad.png" width="30" data-text="SAD"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/angry.png" width="30" data-text="ANGRY"></th>
                          <th><img src="<?php echo HTTP_ROOT ;?>img/share.png" width="30" data-text="SHARE"></th>
                          
                          <?php /*if(!empty($jsonCommingData)){
                          // getting form body in array form
                          $form_body_enoceded  = json_decode(json_decode($FormSingleData['form_body']), true); 
                          ?>
                          <?php   foreach($jsonCommingData as $key =>$value){ ?>
                          <?php if($key == "groups" || $key == "wowza_aplication_id"|| $key == "wowza_aplication" || $key == "wowza_stream_source" ){ continue; }   ?>
                          <?php $key = $form_body_enoceded[array_search($key, array_column($form_body_enoceded, 'name'))]['label'];  ?>
                              <th class='alignCenter'> <?php echo ucfirst($key); ?>  </th>
                          <?php } } */?>

                            <th class='alignCenter'><?php echo  __(actions); ?></th>
                            <th class='alignCenter'><?php echo  __(lastsmssent); ?></th>

                        </tr>
                    </thead>
                         
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>     
<div class="loader"></div>

<style>
.dataTables_scrollBody {
  overflow-y: visible !important;
  overflow-x: initial !important;
}
.dataTables_scrollHead {
  overflow: visible !important;
}
.fullwid{

   width:45%;
   text-align: center;
}
.alignCenter{
    text-align:center;
}
.toggler{
    display: none;
}
.loader {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('../img/loader.gif') 50% 50% no-repeat rgb(8,8,8);
    opacity: .8;
    display: none;
}
#refresh_view {
 float: right;

margin-top: 3px;
}
@media screen and (max-width: 640px){
 div.dataTables_length select {
  width: 145px!important;
    float: right;
    margin-left: 9px;
} 
.dataTables_wrapper .dt-buttons a{padding: 6px 35px!important;}
}
@media screen and (max-width:480px){
.bd_info_tb {
    min-width: 600px;
}
.bd_tab_box {
    overflow: scroll;
    overflow-y: hidden;
}
.dataTables_paginate{
    overflow: scroll;
    overflow-y: hidden;  
}
.dataTables_paginate .pagination{
    min-width: 500px;
    text-align: left;

}
}
@media screen and (max-width: 360px)
{
.top-right-btn{
  padding: 5px 7px;
}
}

</style>