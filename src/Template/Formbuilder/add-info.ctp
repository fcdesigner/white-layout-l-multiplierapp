
<div class="">
                   <div class="row">
					 <div class="col-md-12 col-sm-12 col-xs-12">
						    <div class="x_panel">
                                <div class="x_title">
                                    <h4><?php echo __(SMTPDetails); ?><small></small></h4>
									<div class="clearfix"></div>
							    </div>
								<div class="x_content">
							
							
                                    <?php echo $this->Form->create('$smtpInfo', [
										'url' => ['controller' => 'Formbuilder', 'action' => 'add_info'],
										'class'=>'form-horizontal form-label-left',
										'id'=>'smtpsetting',
										'enctype'=>'multipart/form-data',
										'novalidate'=>'novalidate',
										 'autocomplete'=>'off',
										
									]);?>
									
									
									
									
									<!--
									<input type="hidden" value="<?php echo $formId; ?>" name="formId">-->
									<?php if(!empty($smtpId)){ ?>
										<input type="hidden" value="<?php echo $smtpId; ?>" name="smtpId">
									<?php } ?>
									
									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(FromEmail); ?> <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
										 echo $this->Form->input('from_email',[
												'label' => false,
												'required' => true,
												'class'=>'form-control col-md-7 col-xs-12',
												'value'=>@$smtpInfo->from_email != '' ?$smtpInfo->from_email:'']);
										 ?>
										 </div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(FromName); ?> <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
										 echo $this->Form->input('from_name',[
												'label' => false,
												'required' => true,
												'class'=>'form-control col-md-7 col-xs-12',
												'value'=>@$smtpInfo->from_name != '' ?$smtpInfo->from_name:'']);
										 ?>
										 </div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(SMTPPort); ?> <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
										 echo $this->Form->input('smtp_port',[
												'label' => false,
												'required' => true,
												'class'=>'form-control col-md-7 col-xs-12',
												'value'=>@$smtpInfo->smtp_port != '' ?$smtpInfo->smtp_port:'']);
										 ?>
										 </div>
									</div>
							<div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo __(SMTPSecure); ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-radio-inline">
											    <label class="mt-radio">
                                                    <input name="smtp_secure" id="smtpSecure1" value="None" checked  type="radio" <?php if(!empty(@$smtpInfo->smtp_secure) && (@$smtpInfo->smtp_secure == 'None')){ echo "checked";}?>> <?php echo __(none); ?>
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input name="smtp_secure" id="smtpSecure2" value="SSL"  type="radio" <?php if(!empty(@$smtpInfo->smtp_secure) && (@$smtpInfo->smtp_secure == 'SSL')){ echo "checked";}?>> <?php echo __(ssl); ?>
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input name="smtp_secure" id="smtpSecure3" value="TLS" type="radio" <?php if(!empty(@$smtpInfo->smtp_secure) && (@$smtpInfo->smtp_secure == 'TLS')){ echo "checked";}?>> <?php echo __(tcl); ?>
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-3 control-label"><?php echo __(SMTPAuthentication); ?></label>
                                        <div class="col-md-9">
                                            <div class="mt-radio-inline">
											    <label class="mt-radio">
                                                    <input name="smtp_auth" id="smtpAuth1" value="Yes" checked type="radio" <?php if(!empty(@$smtpInfo->smtp_auth) && (@$smtpInfo->smtp_auth == 'Yes')){ echo "checked='checked'";}?>> <?php echo __(yes); ?>
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input name="smtp_auth" id="smtpAuth2" value="No"  type="radio" <?php if(!empty(@$smtpInfo->smtp_auth) && (@$smtpInfo->smtp_auth == 'No')){ echo "checked='checked'";}?>> <?php echo __(no); ?>
                                                    <span></span>
                                                </label>                                               
                                            </div>
                                        </div>
                                    </div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(SMTPHost); ?> <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
										 echo $this->Form->input('smtp_host',[
												'label' => false,
												'required' => true,
												'class'=>'form-control col-md-7 col-xs-12',
												'value'=>@$smtpInfo->smtp_host != '' ?$smtpInfo->smtp_host:'']);
										 ?>
										 </div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(username); ?> <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
										 echo $this->Form->input('username',[
												'label' => false,
												'required' => true,
												'class'=>'form-control col-md-7 col-xs-12',
												'value'=>@$smtpInfo->username != '' ?$smtpInfo->username:'']);
										 ?>
										 </div>
									</div>

									<div class="item form-group">
										<label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?php echo __(password); ?> <span class="required">*</span>
										</label>
										<div class="col-md-6 col-sm-6 col-xs-12">
										<?php 
										 echo $this->Form->input('password',[
												'label' => false,
												'required' => true,
												'class'=>'form-control col-md-7 col-xs-12',
												'value'=>@$smtpInfo->password != '' ?$smtpInfo->password:'']);
										 ?>
										 </div>
									</div>

									
									
									
									
									<hr />
									
									<div class="ln_solid"></div>
									<div class="form-group">
										<div class="col-md-6 col-md-offset-3">
											<button type="button"  class="btn btn-primary" onclick="window.history.go(-1);"  ><?php echo __(cancel); ?></button>
											<button id="send" type="submit" class="btn btn-success"><?php echo __(submit); ?></button>
										</div>
									</div>

                                    <?php echo $this->form->end(); ?>
                                <!-- end form -->


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<style>
				.ct{
				display:none;
				}
               
				@media only screen and (max-device-width: 540px) and (min-device-width: 320px){
					.page-content-wrapper .x_title {
						order: 1;
					}
				}

			   </style>