<!-- END PAGE HEADER-->
<?php $session = $this->request->session(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"><?php echo __(my_plans); ?> </span>
                </div>
            </div>
			<?php echo __(UpgradePlan); ?>
            <?php 
              $autoliveplan = false;
              foreach($PlanData as $plan){ 
                if($plan['auto_limit'] != "" && $plan['auto_limit'] != 0){
                  $autoliveplan = true;
                }
              } ?>
             <div class="row">
                <div class="col-md-12">
                    <div class="table-scrollable">
                      <table class="table table-striped table-bordered order-column"> 
                        <tr> 
                          <th><?php echo __(Plan); ?></th>
                            <th><?php echo __(live_limit); ?></th>
                            <?php 
                              if($autoliveplan){
                            ?>
                            <th><?php echo __(Auto_live_limit); ?></th>
                            <th><?php echo __(Auto_live_per_limit); ?></th>
                            <?php } ?>
                            <th><?php echo __(multiprivate_per_limit); ?></th>
                          <th><?php echo __(DaysLeft); ?></th>
                        </tr>
                        <?php $target_limit = 0;
                          foreach($PlanData as $plan){ 
                            $target_limit += $plan['target_limit'];
                            $auto_limit += $plan['auto_limit'];

                            $date = date('Y-m-d');
                            $date1 = new DateTime($date);
                            $date2 = new DateTime($plan['expiry_date']);
                            $daysLeft = $date2->diff($date1)->format("%a");
                          ?>
                          <tr>
                            <th><?php echo $plan['product_name']; ?></th> 
                            <td><?php echo $plan['target_limit']; ?></td>  
                            <?php 
                              if($autoliveplan){
                            ?>
                            <td><?php echo $plan['auto_limit']; ?></td>
                            <td><?php echo $plan['daily_limit']; ?></td>
                            <?php } ?>
                            <td><?php echo $plan['private_target_limit']; ?></td>
                            <td><?php echo $daysLeft; ?></td>
                          </tr>
                        <?php } ?>
                        <!--<tr>
                          <th><?php echo __(total); ?></th> 
                          <td><?php echo $target_limit; ?></td>  
                           <?php 
                              if($autoliveplan){
                            ?>
                          <td><?php echo $auto_limit; ?></td> 
                           <td>-</td>
                            <?php } ?>
                          <td>-</td> 
                           <td>-</td> 
                        </tr>-->
                      </table>
                </div>                

              </div>
            </div>
        </div>
    </div>
</div>