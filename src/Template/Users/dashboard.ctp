<style>
.logo img{
  margin:auto;
  display:block;
}
.headding h4{
  margin-top:25px;
  font-weight:bold;
  font-size:20px;
}
/*.google_play {
  padding-right: 20px;
}*/
#imgmain {
  margin:auto !important;
  margin-top: 20px !important;
}
.banner{

  width:100%;
 
}
.imgovel{
  width: 55%;
}
.banner img{
  width:80%;
  margin:auto;
  display: block;
}

.sec_hed{
  font-size:23px !important;
  margin-top:15px;
  font-weight:bold;
}
h4{
  font-weight:bolder; margin-top:10px; color:#000;
}
p{
  font-size:15px;
  margin-top:30px;
}
.para_box{
  text-align:center;
  padding-left: 10px;
  padding-right: 10px;
}

.sec_hed{
  font-size:20px !important;
}


h4.video_title{
  color: #FF6600;
}

.video_desc{
  padding: 0;
  margin: 0;
  font-size: 13px;
  padding-left: 40px;
  padding-right: 40px;
  text-align: justify;
}
.custm-cls th{text-align: center;}

@media(max-width: 768px){

	#title{
		text-align: center;
	}

}

</style>
<?php $session = $this->request->session();  ?>
<section>
  <div class="page-bar"></div>                    
  <h3 id = "title"> <?php echo __(dashboard); ?> </h3>
    <!-- <small><?php //echo __(dashboard_static); ?></small> -->
 
  <div class="">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo"> <img src="<?php echo HTTP_ROOT ;?>img/uploads/logo.png" width="150"> </div>
    </div>
    <div class="row">
      <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12 para_box">
        <p><?= __(dashboard_top); ?></p>
        <div  class="app-google">
          <a href="https://play.google.com/store/apps/details?id=com.br.multiplierapp"><img src="<?php echo HTTP_ROOT ;?>img/gstore.png" width="200" class="google_play"></a>
		  <a href="https://itunes.apple.com/app/multiplierapp/id1301426934?ls=1&mt=8"><img src="<?php echo HTTP_ROOT ;?>img/apps.png"" width="200" class="appstore"></a>
          <!--<img src="<?php echo HTTP_ROOT ;?>img/2495806708-iOS.jpg" width="200" class="appstore">-->
        </div>
        <p><strong><?= __(dashboard_one); ?></strong></p>
        <p><?= __(dashboard_two); ?></p>
        <p><?= __('dashboard_front_note',[HTTP_ROOT.'commingusers/view-broadcasts']); ?></p><br>
        <div class="row">
          <div class="table-scrollable col-lg-12 col-md-12 col-sm-12" style="text-align:center;">
          <?php 
           $FakeUsers = @$this->Custom->getFakeUsers();

          $dataTotal =  @$this->Custom->getBroadCastData($form_id); 
          $reached =  @$this->Custom->getBroadcastReached($form_id);
          $expiredUsersData =  @$this->Custom->getExpiredUsersInteractions();
          $archive_data =  @$this->Custom->getArchiveTableData();
          
          if($authUser['role'] == "1" || $authUser['role'] == "2"){
            $reached  +=  $FakeUsers['reached'] + $expiredUsersData->reached + $archive_data->reached;
            $dataTotal->vlike  +=  $dataTotal->vlike+ $FakeUsers['vlike'] + $expiredUsersData->vlike + $archive_data->vlike;
            $dataTotal->haha  +=  $FakeUsers['haha'] + $expiredUsersData->haha + $archive_data->haha;
            $dataTotal->love  +=  $FakeUsers['love'] + $expiredUsersData->love + $archive_data->love;
            $dataTotal->sad  +=  $FakeUsers['sad'] + $expiredUsersData->sad + $archive_data->sad;
            $dataTotal->angry  +=  $FakeUsers['angry'] + $expiredUsersData->angry + $archive_data->angry;
            $dataTotal->wow  +=  $FakeUsers['wow'] + $expiredUsersData->wow + $archive_data->wow;
          }
         
          ?>
            <table class="table custm-cls table-striped table-bordered order-column">
              <thead>
                <tr>
                    <th><img  src="<?php echo HTTP_ROOT ;?>img/total.png" width="25">
                        <span data-toggle="tooltip" title="" data-placement="bottom" data-original-title="<?= __('total_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                    </th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/comment.png" width="30"></th>
                    <!-- <th><img  src="<?php echo HTTP_ROOT ;?>img/view.png" width="38">
                        <span data-toggle="tooltip"  data-placement="bottom" title="" data-original-title="<?= __('view_msg'); ?>"><i class="fa fa-question-circle"></i></span>
                    </th> -->
                    <th><img src="<?php echo HTTP_ROOT ;?>img/like.png" width="35"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/love.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/haha.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/wow.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/sad.png" width="30"></th>
                    <th><img src="<?php echo HTTP_ROOT ;?>img/angry.png" width="30"></th>
                    <th><?= __('reached')?></th>
                    <?php if($authUser['role'] != "1" && $authUser['role'] != "2"){ ?>
                      <th><?= __('Refresh'); ?></th>
                    <?php } ?>
                </tr>
              </thead>
       
              <tbody align="center">
                <tr>
                  <td><?=  @$this->Custom->number_shorten($dataTotal->angry+$dataTotal->comment+$dataTotal->view+$dataTotal->vlike+$dataTotal->love+$dataTotal->haha+$dataTotal->wow+$dataTotal->sad+$dataTotal->dislike, 2); ?></td>
                  <td><?=  ($dataTotal->comment) ? @$this->Custom->number_shorten($dataTotal->comment, 2):'0'; ?></td>
                  <!-- <td><?=  ($dataTotal->view) ? @$this->Custom->number_shorten($dataTotal->view):'0'; ?></td> -->
                  <td><?=  ($dataTotal->vlike) ? @$this->Custom->number_shorten($dataTotal->vlike, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->love) ? @$this->Custom->number_shorten($dataTotal->love, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->haha) ? @$this->Custom->number_shorten($dataTotal->haha, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->wow) ? @$this->Custom->number_shorten($dataTotal->wow, 2):'0'; ?></td>
                  <td><?=  ($dataTotal->sad) ? @$this->Custom->number_shorten($dataTotal->sad, 2):'0'; ?></td>
                  <td><?=  @$this->Custom->number_shorten(($dataTotal->angry) ? $dataTotal->angry:'0'+$dataTotal->dislike, 2); ?></td>
                  <td><?=  ($reached) ? @$this->Custom->number_shorten($reached, 2):'0'; ?></td>
                  <?php if($authUser['role'] != "1" && $authUser['role'] != "2"){ ?>
                    <td><a href="<?php echo HTTP_ROOT.'app/updateStats?form_id='.$form_id; ?>" class=""><img src="<?php echo HTTP_ROOT ;?>img/refresh.png" width="30"></a></td>
                  <?php } ?>
                </tr>
              </tbody>
              <?php  ?>
            </table>
          </div>
        </div>
      </div>      
    </div>
  </div>
</section>
  <section>
    <!-- <div  class="row banner">
      <img class = "img-responsive" src="<?php echo HTTP_ROOT ;?>img/Coelho2.png">
    </div> -->
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12" style="text-align:center;">
        <?php $i = 0;
        foreach($videos as $video){ ?>
          <?php if($i % 3 == 0 ){ ?>
            <div class="clearfix"></div><br/><br/>
          <?php } ?>
          <div class="col-lg-4 col-md-4 col-sm-4">
            <h4 class="video_title">
               <?php if($session->read('language') == "en_US"){
                  echo $video['title_en'];
                }else if($session->read('language') == "en_SP"){
                  echo $video['title_es'];
                }else{
                  echo $video['title_pt'];
                } ?>
            </h4>
            <p class="video_desc">
              <?php if($session->read('language') == "en_US"){
                echo $video['desc_en'];
              }else if($session->read('language') == "en_SP"){
                echo $video['desc_es'];
              }else{
                echo $video['desc_pt'];
              } ?>
            </p>
            <?php $url = "";
            if($session->read('language') == "en_US"){
              $url = $video['url_en'];
            }else if($session->read('language') == "en_SP"){
              $url = $video['url_es'];
            }else{
              $url = $video['url_pt'];
            } ?>
            <iframe style="margin-top: 10px;" width="80%" height="220" src="<?php echo $url; ?>" frameborder="1" allowfullscreen></iframe>
          </div>          
        <?php $i++; } ?>
      </div>
    </div>
  </section>
  <!--<section>
    <div class="row para_box">
      <p><?= __(dashboard_three); ?></p>
       <img class = "img-responsive imgovel" id="imgmain" src="<?php echo HTTP_ROOT ;?>img/1556106773-Affiliates.png">
        <p><?= __(affliate); ?></p>
       <h4><?= __(dashboard_bottom); ?></h4>
    </div>
  </section>-->

</div>