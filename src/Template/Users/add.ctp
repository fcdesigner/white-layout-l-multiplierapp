<!-- src/Template/Users/add.ctp -->

<div class="">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title" style="margin-top:25px;">
                    <h4><?php echo __(add_user); ?><small></small></h4>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php echo $this->Form->create($user,[
							'url' 		=> ['controller' => 'Users', 'action' => 'add'],
							'class'		=>'form-horizontal form-label-left',
							'id'		=>'usersAdd',
							'enctype'	=>'multipart/form-data',
							'novalidate'=>'novalidate',
              'autocomplete'=>'off'
							 // 'autocomplete'=>'off',
									]) ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        	<?php echo __(name); ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('name',[
                                   'label' => false,
                                   'required' => true,
                                   'class'=>'form-control col-md-7 col-xs-12']);
                                ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        	<?php echo __(username); ?><span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('username',[
                                   'label' => false,
                                   'required' => true,
                                   // 'error'	=> false,
                                   // 'format'=>array('after', 'input'),
                                   'class'=>'form-control col-md-7 col-xs-12']);
                                ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        	<?php echo __(email); ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('email',[
                                   'label' => false,
                                   'required' => true,
                                   'class'=>'form-control col-md-7 col-xs-12']);
                                ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                          <?php echo __(phone); ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('phone',[
                                   'label' => false,
                                   'required' => true,
                                   'class'=>'form-control col-md-7 col-xs-12']);
                                ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        	<?php echo __(password); ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('password',[
                                   'label' => false,
                                   'type'	=> 'password',
                                   'required' => false,
                                   'id' => 'password',
                                   'class'=>'form-control col-md-7 col-xs-12']);
                                ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        	<?php echo __(cpassword); ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('passwordConfirm',[
                                   'label' => false,
                                   'required' => false,
                                   'type'	=> 'password',
                                   'class'=>'form-control col-md-7 col-xs-12']);
                                ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">
                        	<?php echo __(role); ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                                echo $this->Form->input('role',[
                                   'label' => false,
                                   'required' => true,
                                   'class'=>'form-control col-md-7 col-xs-12',
                                   'options' => $roles
                                   ]);
                                ?>
                        </div>
                    </div>
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="promo_code"><?= __(Segment) ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                         <?php echo $this->Form->input('segment_id',[
                                'templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'required' => true,
                                'class'=>'form-control col-md-7 col-xs-12',
                                'type'=>'select',
                                'options'=> @$segments,
								'empty' =>__(select_segment)
                                ]);
                        ?>
                        </div>
                    </div>
                    <hr />
                    <br />
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button"  class="btn btn-primary" onclick="window.history.go(-1);"  ><?php echo __(cancel); ?></button>
                            <button id="send" type="submit" class="btn btn-success"><?php echo __(submit); ?></button>
                        </div>
                    </div>
                    <?php echo $this->form->end(); ?>
                    <!-- end form -->
                </div>
            </div>
        </div>
    </div>
</div>
<style>
.ct{
  display:none;
}

@media only screen and (max-device-width: 540px) and (min-device-width: 320px){
		.page-content-wrapper .x_title {
			order: 1;
		}
	}

</style>