<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo __(system_users); ?> </span>
                </div>
            </div> 
            <div class="portlet-body">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
							<?php if($authUser['role'] != 5){ ?>
                                <a href="<?php echo HTTP_ROOT; ?>users/add" class="nav-link ">
                                    <button id="sample_editable_1_new" class="btn sbold green"> <?php echo __(add_new); ?>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </a>
							<?php } ?>	
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body table-responsive1">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="system_user">
                            <thead>
                                <tr>
                                    <th class="all"><?php echo __(id); ?></th>
                                    <th class="all"><?php echo __(name); ?></th>
                                    <th class="min-phone-l"><?php echo __(email); ?></th>
                                    <th class="min-phone-l"><?php echo __(phone); ?></th>
                                    <th class="min-tablet"><?php echo __(role); ?></th>
                                    <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                    <th class="all"><?php echo __(application_name); ?>  </th>
                                    <?php } ?>
                                    <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                    <th><?php echo __(API); ?></th>
                                    <?php } ?>
									<th><?php echo __(Segment); ?></th>
                                    <th class="none"><?php echo __(TargetLimit); ?></th>
                                    <th><?php echo __(last_expire_plan); ?></th>
                                    <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                    <th><?php echo __(last_live); ?></th>
                                    <?php } ?>
                                    <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                    <th><?php echo __(registered_social); ?></th>
                                    <?php } ?>
                                    <th><?php echo __(expire); ?></th>
                                    <th class="none"><?php echo __(created); ?></th>
                                     <th class="none"><?php echo __(pal); ?></th>
                                    <!-- <th class="none"><?php echo __(modified); ?></th> -->
                                    <th class="none"><?php echo __(actions); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($users as $user): ?>
                            <tr>
                                <td><?= $this->Number->format($user->id) ?></td>
                                <td><?= $user->name ?></td>
                                <td><?= $user->email ?></td>
                                <td><?= $user->phone ?></td>
                                <td><?= @$roles[$user->role] ?></td>
                                <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                    <td><a href="<?php echo HTTP_ROOT. 'formbuilder/listing/'. $user->group->id; ?>"><?php echo $user->group->group_name; ?></a></td>
                                <?php } ?>
                                <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                <td><?php echo $user->registerFrom != null ?$user->registerFrom:'-'; ?></td>
                                <?php } ?>
								<td><?php echo $segments[$user->segment_id] != '' ?$segments[$user->segment_id]:'-'; ?></td>
                                <td>
                                    <?php $userId = ($user->role == 1 || $user->role == 3) ? $user->id : $user->parent_id;
                                    echo @$this->Custom->getTargetLimit($userId); ?>
                                </td>
                                <td><?php echo @$this->Custom->getLastExpirePlan($userId); ?></td>
                                <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                <td><?php echo @$this->Custom->getLastlive($user->group->id); ?></td>
                                <?php } ?>
                                <?php if(($authUser['role'] == 1) || ($authUser['role'] == 2)){ ?>
                                <td><?php echo @$this->Custom->getRegisteredSocial($user->group->id); ?></td>
                                <?php } ?>
                                <td><?php echo @$this->Custom->getExpiryDates($userId); ?></td>
                                <!--<td><?php  echo appDate($user->created ); ?></td> -->
                                <td><?= date('Y-m-d', strtotime( $user->created ) ) ?></td>
                                <td><?php echo @$this->Custom->getIsPAL($userId); ?></td>
                                <td class="actions">
                                    <?php if ( $user->status == 1 ){
                                        echo $this->Form->postLink(__('<i class="fa fa-user text-green" aria-hidden="true"></i>'), ['action' => 'changeStatus', $user->id, 'off'], ['title' => __(active), 'escape' => false, 'class' => 'system_user', 'confirm' => __('deactive_confirm', ucfirst( $user->name ) )]); 
                                    }else{
                                        echo $this->Form->postLink(__('<i class="fa fa-user-times text-danger" aria-hidden="true"></i>'), ['action' => 'changeStatus', $user->id, 'on'], ['title' => __(inactive), 'escape' => false, 'class' => 'system_user', 'confirm' => __('active_confirm', ucfirst( $user->name ) )]); 
                                    } ?>                                  

                                    <?= $this->Html->link(__('<i class="fa fa-edit font-dark" aria-hidden="true"></i>'), ['action' => 'edit', $user->id],['title' => __(edit), 'escape' => false, 'class'=> 'system_editIcon'] ) ?>
                                    <?= $this->Html->link(__('<i class="fa fa-unlock-alt aria-hidden="true"></i>'), ['action' => 'changePassword', $user->id], ['title' => __(change_password), 'escape' => false]) ?>
                                    &nbsp;
                                    <a data-target="#myModal" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'users/delete/'.$user->id;?>"><i class="fa fa-trash-o text-danger"></i></a>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>