<div class="">
   <div class="row">
	 <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
                <div class="x_title">
                    <h4><?php echo __(Formdemovideo); ?><small></small></h4>
					<div class="clearfix"></div>
			    </div>
				<div class="x_content">
					<?php echo $this->Form->create('$video', [
						'url' => ['controller' => 'terms', 'action' => 'video'],
						'class'=>'form-horizontal form-label-left',
						'id'=>'video',
						'enctype'=>'multipart/form-data',
						'novalidate'=>'novalidate',
						 'autocomplete'=>'off',
						
					]);?>
					<input type="hidden" value="<?php echo $video['id']?>" name="page_id" id="page_id">					
					<hr />
					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Youtubevideolink); ?></label>
						<div class="col-md-7 col-sm-9 col-xs-12">
							 <input  required="required" class="form-control col-md-7 col-xs-12" type="text" name="title" value="<?php echo $video['title']; ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3">
							<button id="send" type="submit" class="btn btn-success"><?php echo __(save); ?></button>
						</div>
					</div>
                    <?php echo $this->form->end(); ?>
                	<!-- end form -->
                </div>
            </div>
        </div>
    </div>
</div>

<style>
 @media only screen and (max-device-width: 540px) and (min-device-width: 320px){
	.page-content-wrapper .x_title {
		order: 1;
	}
}

</style>