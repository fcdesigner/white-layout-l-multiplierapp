<div class="">
   <div class="row">
	 <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="x_panel">
                <div class="x_title">
                    <h4><?php echo __(termsandconditions); ?><small></small></h4>
					<div class="clearfix"></div>
			    </div>
				<div class="x_content">
					<?php echo $this->Form->create('$termsEdit', [
						'url' => ['controller' => 'terms', 'action' => 'termsEdit'],
						'class'=>'form-horizontal form-label-left',
						'id'=>'termsEdit',
						'enctype'=>'multipart/form-data',
						'novalidate'=>'novalidate',
						 'autocomplete'=>'off',
						
					]);?>
					<input type="hidden" value="<?php echo $terms['id']?>" name="page_id" id="page_id">					
					<hr />
					<h4><?php echo __(PageTitle); ?></h4>	
					<div class="item form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="description"><?php echo __(PageTitle); ?><span class="required">*</span>
						</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							 <input  required="required" class="form-control col-md-7 col-xs-12" type="text" name="title" value="<?php echo $terms['title']; ?>">
						</div>
					</div>
					
      				<hr />
					<h4><?php echo __(Pagecontent); ?></h4>
				    <div class="item form-group">
						<label class="control-label col-md-2 col-sm-2 col-xs-12" for="description"><?php echo __(Pagecontent); ?><span class="required">*</span>
						</label>
						<div class="col-md-9 col-sm-9 col-xs-12">
							 <textarea rows="20" id="" required="required" class="summernote form-control col-md-7 col-xs-12" name="content"><?php echo $terms['content']; ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3">
							<button id="send" type="submit" class="btn btn-success"><?php echo __(save); ?></button>
						</div>
					</div>

                    <?php echo $this->form->end(); ?>
                <!-- end form -->


                </div>
            </div>
        </div>
    </div>
</div>

<style>

@media only screen and (max-device-width: 540px) and (min-device-width: 320px){
	.page-content-wrapper .x_title {
		order: 1;
	}
}

</style>