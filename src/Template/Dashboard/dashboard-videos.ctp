<!-- END PAGE HEADER-->
<?php $session = $this->request->session(); ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo __(videos); ?> </span>
                </div>
            </div> 
            <div class="portlet-body">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
                                <a href="<?php echo HTTP_ROOT; ?>dashboard/add" class="nav-link ">
                                    <button id="sample_editable_1_new" class="btn sbold green"> <?php echo __(add_new); ?>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>                   
                    <div class="portlet-body table-responsive1">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="system_user">
                            <thead>
                                <tr>
                                    <th class="all"><?php echo __(id); ?></th>
                                    <th class="all"><?php echo __(title); ?></th>
                                    <th class="all">URL</th>
                                    <th class="none"><?php echo __(actions); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($videos as $video): ?>
                                <tr>
                                    <td><?= $this->Number->format($video->id) ?></td>
                                    <td>
                                        <?php if($session->read('language') == "en_US"){
                                            echo $video->title_en;
                                        }else if($session->read('language') == "en_SP"){
                                            echo $video->title_es;
                                        }else{
                                            echo $video->title_pt;
                                        } ?>                                        
                                    </td>
                                    <td>
                                        <?php if($session->read('language') == "en_US"){
                                            echo $video->url_en;
                                        }else if($session->read('language') == "en_SP"){
                                            echo $video->url_es;
                                        }else{
                                            echo $video->url_pt;
                                        } ?>     
                                    </td>
                                    <td class="actions">
                                        <?= $this->Html->link(__('<i class="far fa-edit font-dark" aria-hidden="true"></i>'), ['action' => 'edit', $video->id],['title' => __(edit), 'escape' => false, 'class'=> 'system_editIcon'] ) ?>
                                        &nbsp;
                                        <a data-target="#myModal" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'dashboard/delete/'.$video->id;?>"><i class="far fa-trash-alt text-danger"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
		<!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>