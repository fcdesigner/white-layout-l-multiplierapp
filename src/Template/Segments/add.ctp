<div class="">
   <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h4><?= __(AddSegment) ?><small></small></h4>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php echo $this->Form->create($segment,[
                        'url'       => ['controller' => 'Segments', 'action' => 'add'],
                       /* 'class'     =>'form-horizontal form-label-left',*/
                        'id'        =>'segmentInfo',
                        'enctype'   =>'multipart/form-data',
                        'novalidate'=>'novalidate',
                         // 'autocomplete'=>'off',
                                ]); ?>

                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="segment_name"><?= __(name) ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php 
                         echo $this->Form->input('name',[
                                'templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'required' => true,
                                'class'=>'form-control col-md-7 col-xs-12'
                               ]);
                         ?>
                         </div>
                    </div> 
					<br /><br /> 
					<div class="clearfix"></div>
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="trial_days"><?= __(trial_days) ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                             echo $this->Form->input('trial_days',[
                                'templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'required' => true,
                                'min' => 1,
                                'type' => "number",
                                'class'=>'form-control col-md-7 col-xs-12'
                            ]); ?>
                        </div>
                    </div>                
                    <br /><br /> 
                    <div class="clearfix"></div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="target_limit"><?= __(target_limit) ?> <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php 
                             echo $this->Form->input('target_limit',[
                                'templates' => ['inputContainer' => '{{content}}'],
                                'label' => false,
                                'required' => true,
                                'min' => 1,
                                'type' => "number",
                                'class'=>'form-control col-md-7 col-xs-12'
                            ]); ?>
                        </div>
                    </div> 
					<br /><br /> 
					<div class="clearfix"></div>
					<div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="segment_admin"><?= __(segment_admin) ?> <span class="required"></span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php 
							echo $this->Form->input('segment_admin',[
								'templates' => ['inputContainer' => '{{content}}'],
								'label' => false,
								'required' => false,
								'class'=>'form-control col-md-7 col-xs-12 boot_select',
								'type'=>select,
								'options'=> @$usermangerlist
							]);
                         ?>
                         </div>
                    </div> 
                    <hr /><br /><br /> 
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button type="button"  class="btn btn-primary" onclick="window.history.go(-1);"  ><?= __(cancel) ?></button>
                            <button id="send" type="submit" class="btn btn-success"><?= __(submit) ?></button>
                        </div>
                    </div>

                    <?php echo $this->form->end(); ?>
                <!-- end form -->


                </div>
            </div>
        </div>
    </div>
</div>
<style>
.ct{
display:none;
}

@media only screen and (max-device-width: 540px) and (min-device-width: 320px){
	.page-content-wrapper .x_title {
		order: 1;
	}
}

</style>