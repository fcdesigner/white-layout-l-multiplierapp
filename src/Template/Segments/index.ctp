<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
         <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"><?php echo __(Segment); ?></span>
                </div>
               
            </div> 
            <div class="portlet-body">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <div class="btn-group">
                        <a href="<?php echo HTTP_ROOT; ?>segments/add" class="nav-link ">
                            <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </button>
                        </a>
                    </div>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="table-scrollable portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="segmentListing">
                    <thead>
                        <tr>
                            <th><?php echo '#' ?></th>
                            <th><?php echo  __(name); ?></th>                            
                            <th><?php echo __(segment_admin); ?></th>                            
                            <th><?php echo __(trial_days); ?></th>                            
                            <th><?php echo __(target_limit); ?></th>                            
                            <th><?php echo __(created); ?></th>
                            <th><?php echo __(action); ?></th>                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if(!empty($segments)){
					$i=0;	
                    foreach ($segments as $segment): 
					$i++;
					?>
                    <tr>
                        <td><?= $i; ?></td>
                        <td>   <?= $segment->name ?></td>
                        <td>   <?= $usermangerlist[$segment->segment_admin] ?></td>
                        <td><?= $segment->trial_days ?></td>
                        <td><?= $segment->target_limit ?></td>
                        <td><?= date('Y-m-d', strtotime( $segment->created ) ) ?></td>
                        <td class="actions"> 
						<?php if($segment->name !='Other'){ ?>
                        <?php if ($authUser['role'] == 1): ?>
                            <a data-target="#myModal" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'segments/delete/'.$segment->id;?>"><i class="fa fa-trash-alt text-danger pull-right"></i></a>
                        <?php endif ?>
                        <?= $this->Html->link(__('<i class="fa fa-edit text-dark pull-right" aria-hidden="true"></i>'), ['controller'=>'segments','action' => 'edit', $segment->id], ['title' => 'Edit', 'escape' => false, 'class' => 'system_deleteIcon']) ?>
						<?php } ?>
                        </td>
                    </tr>
                    <?php endforeach;
                    } ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>


<style>

	.fa-trash-alt{
		padding-top: 5px;
	}

</style>