<link rel="stylesheet" type="text/css" media="screen" href="<?php echo HTTP_ROOT; ?>assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css">
<link href="<?php echo HTTP_ROOT; ?>assets/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
<style>
label { padding-right:10px;}

label input {margin-right: 7px;
    margin-top: 8px;}
.ct{
display:none;
}
 .registered_user_table td.nowrap{
    white-space: nowrap;
    width: 1%;
         font-weight:bold;
} 
 .registered_user_table td.nowrap a{
 margin-right:0
 }

#title{
	margin-top: 5px;
}

#small_text{
	margin: 0 10px;
}

 @media only screen and (max-device-width: 540px) and (min-device-width: 320px){
	.page-content-wrapper .x_title {
		order: 1;
	}
}

</style>
<script>
var check_source_name = <?php echo !empty(@$formInfo->wowza_source_name) ? "true": "false"; ?>;
</script>
<div class="">
   <div class="row web_reg_form">
	 <div class="col-md-12 col-sm-12 col-xs-12">
		    <div class="table-scrollable portlet light bordered x_panel">
                <div class="x_title">
                    <h4 id = "title"><b><?php echo __(Form_Title_and_Description); ?></b></h4>
					<p id = "small_text"><small><?php echo __(Form_Title_and_Description_Msg); ?></small></p>
					<div class="clearfix"></div>
					 
			    </div>
				<div class="x_content">

                    <?php 
					$session = $this->request->session();
					$language='';
					if($session->read('language') == "en_US"){
					  $language = "english";
					}else if($session->read('language') == "en_SP"){
					  $language = "spanish";
					}else{
					  $language = "portuguese";
					} ?>
                    <div class="row">
                            <div class="caption font-dark viewUsertable">
                                <div class="table-scrollable">
                                  <table class="table table-striped table-bordered order-column registered_user_table"> 
                                    <tr> 
                                      <td class="nowrap"><?php echo __(accesspage); ?></td>
                                       <td class="nowrap"><a  href="<?php echo WEB_REGISTRATION_URL.md5($applicationId).'?lang='.$language; ?>" target="_blank"  class="btn btn-success"><?php echo __(see); ?></a> </td> 
                                      <td><?php echo WEB_REGISTRATION_URL.md5($applicationId).'?lang='.$language; ?> </td>  
                                              
                                    </tr>
                                  </table>
                                </div>                                        
                            </div>
                        </div>
					<?php echo $this->Form->create('$basicInfo', [
						'url' => ['controller' => 'Webregistration', 'action' => 'editForm/'.$applicationId],
						'class'=>'form-horizontal form-label-left',
						'id'=>'editForm',
						'enctype'=>'multipart/form-data',
						'novalidate'=>'novalidate',
						 'autocomplete'=>'off',

					]);?>

					<input type="hidden" value="<?php if(!empty($formInfo->id)){echo @$formInfo->id;} ?>" name="form_id" id="form_id">
                    <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Form_Title); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('form_title',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12 summernote',
                    							 'id'	=> '',
                    							 'type' => 'textarea',
                    							 'value'=>@$formInfo->title != '' ?$formInfo->title:'']);?>
                    						</div>
                    					</div>
                    					<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(Form_Description); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar('1'); ?>
                    							 <?php echo $this->Form->input('form_description',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12 summernote',
                    							 'id'	=> '',
                    							 'type' => 'textarea',
                    							 'value'=>@$formInfo->description != '' ?$formInfo->description:'']);?>
                    						</div>
                    					</div>
							<hr />
					<h4><?php echo __(Company); ?></h4>

									 <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_name"><?php echo __(Name); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('company_name',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->company_name != '' ?$formInfo->company_name:'']);?>
                    						</div>
                    					</div>
										
										 <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="company_email"><?php echo __(Email); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('company_email',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'email',
                    							 'value'=>@$formInfo->company_email != '' ?$formInfo->company_email:'']);?>
                    						</div>
                    					</div>
						
										 <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="facebook_url"><?php echo __(Facebook); ?> <?php echo __(Url); ?>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('facebook_url',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->facebook_url != '' ?$formInfo->facebook_url:'']);?>
                    						</div>
                    					</div>
										
										
										 <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="twitter_url "><?php echo __(Twitter); ?> <?php echo __(Url); ?>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('twitter_url',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->twitter_url  != '' ?$formInfo->twitter_url :'']);?>
                    						</div>
                    					</div>
										
										<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="website_url "><?php echo __(Website); ?> <?php echo __(Url); ?>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('website_url',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->website_url  != '' ?$formInfo->website_url :'']);?>
                    						</div>
                    					</div>
										
										<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="skype_url "><?php echo __(Skype); ?> <?php echo __(Url); ?>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('skype_url',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->skype_url  != '' ?$formInfo->skype_url :'']);?>
                    						</div>
                    					</div>
										
										<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="linkedIn_url "><?php echo __(LinkedIn); ?> <?php echo __(Url); ?>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('linkedIn_url',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->linkedIn_url  != '' ?$formInfo->linkedIn_url :'']);?>
                    						</div>
                    					</div>
										
										<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="youtube_url "><?php echo __(Youtube); ?> <?php echo __(Url); ?>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('youtube_url',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->youtube_url  != '' ?$formInfo->youtube_url :'']);?>
                    						</div>
                    					</div>
					<hr />
					<h4><?php echo __(Footer); ?></h4>				
										
										 <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(title); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('footer_title',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12 ',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->footer_title != '' ?$formInfo->footer_title:'']);?>
                    						</div>
                    					</div>
										
										
										 <div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="footer_sub_title"><?php echo __(Sub); ?> <?php echo __(title); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-8 col-sm-8 col-xs-12">
                    							<?php // editorToolbar(); ?>
                    							 <?php echo $this->Form->input('footer_sub_title',
                    							 [
                    							 'label'=>false,
                    							 'class'=>'form-control col-md-7 col-xs-12',
                    							 'id'	=> '',
                    							 'type' => 'text',
                    							 'value'=>@$formInfo->footer_sub_title != '' ?$formInfo->footer_sub_title:'']);?>
                    						</div>
                    					</div>
					<hr />
					<h4><?php echo __(image); ?></h4>

					<?php $logoimage = ($formInfo->logo && $formInfo->logo!='') ? HTTP_ROOT.'img/uploads/webregimg/logo/' . $formInfo->logo : HTTP_ROOT.'img/uploads/formimg/dummy_image.png'; ?>
					<?php $bannerimage = ($formInfo->cover_image && $formInfo->cover_image!='') ? HTTP_ROOT.'img/uploads/webregimg/banner/' . $formInfo->cover_image : HTTP_ROOT.'img/uploads/formimg/dummy_image.png';  ?>

					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category"><?php echo __(logoimage); ?>  <span class="required"></span>
						</label>
						<div class='col-md-6 col-sm-6 col-xs-12'>
						 <input type="file" name="logo" id="logo">
						</div>
						<div class='col-md-3 col-sm-3 col-xs-12'>
							<?php if (!empty($formInfo->logo)): ?>
								<span data-img='1' class="delete_image clear_image"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
							<?php endif ?>

							<?php if (strpos($logoimage, 'https') !== false) { ?>
							    <img src="<?php echo $logoimage ; ?>" height="100" width="100" class="form_image">
							<?php }else{ ?>
								<img src="<?php echo $logoimage ; ?>" height="100" width="100" class="form_image">
							<?php } ?>							
						</div>
					</div>

					<div class="item form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Category"><?php echo __(bannerimage); ?>  <span class="required"></span>
						</label>
						<div class='col-md-6 col-sm-6 col-xs-12'>
						 <input type="file" name="bannerimage" id="bannerimage">
						</div>
						<div class='col-md-3 col-sm-3 col-xs-12'>
						<?php if (!empty($formInfo->cover_image)): ?>
						<span data-img='2' class="delete_image clear_image"><i class="fa fa-trash-o" aria-hidden="true"></i></span>
						<?php endif ?>
						<img src="<?php echo $bannerimage ; ?>" height="100" width="100" class="form_image">
						</div>
					</div>
					<hr />
					<h4><?php echo __(Theme_Details); ?></h4>

					<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(text_color); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-6 col-sm-6 col-xs-12">

<div class="input-group color colorpicker-default" data-color="<?php echo $formInfo->text_color != '' ?$formInfo->text_color:'#fff';?>" data-color-format="rgba">
                    							 <?php echo $this->Form->input('text_color',
                    							 [
                    							 'label'=>false,
                    							 'required' => true,
                    							 'class'=>'form-control col-md-7 col-xs-12 ',
                    							 'value'=>@$formInfo->text_color != '' ?$formInfo->text_color:'']);?>
                    							  <span class="input-group-btn">
                                                                                                                                                                  <button class="btn default" type="button">
                                                                                                                                                                      <i style="background-color: #fff;"></i>&nbsp;</button>
                                                                                                                                                              </span>
                                                                                                                                                          </div>
                    						</div>
                    					</div>
                    					<div class="item form-group">
                    						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="description"><?php echo __(theme_color); ?><span class="required">*</span>
                    						</label>
                    						<div class="col-md-6 col-sm-6 col-xs-12">

<div class="input-group color colorpicker-default" data-color="<?php echo $formInfo->theme_color != '' ?$formInfo->theme_color:'#000';?>" data-color-format="rgba">
                    							 <?php echo $this->Form->input('theme_color',
                    							 [
                    							 'label'=>false,
                    							 'required' => true,
                    							 'class'=>'form-control col-md-7 col-xs-12 ',
                    							 'value'=>@$formInfo->theme_color != '' ?$formInfo->theme_color:'']);?>
                    							 <span class="input-group-btn">
                                                                                                                 <button class="btn default" type="button">
                                                                                                                     <i style="background-color: #000;"></i>&nbsp;</button>
                                                                                                             </span>
                                                                                                         </div>
                    						</div>
                    					</div>
					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3">
							<button type="button"  class="btn btn-primary" onclick="window.history.go(-1);"  ><?php echo __(cancel); ?></button>
							<button id="send" type="submit" class="btn btn-success"><?php echo __(submit); ?></button>
						</div>
					</div>
                    <?php echo $this->form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo HTTP_ROOT; ?>assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<script src="<?php echo HTTP_ROOT; ?>assets/global/plugins/jquery-minicolors/jquery.minicolors.min.js" type="text/javascript"></script>
<script>
$(document).ready(function(){
    $(".colorpicker-default").colorpicker({format:"hex"});
});
</script>