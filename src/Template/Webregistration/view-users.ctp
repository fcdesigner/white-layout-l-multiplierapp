<!-- END PAGE HEADER-->
<style>
 .registered_user_table td.nowrap{
    white-space: nowrap;
    width: 1%;
         font-weight:bold;
} 
 .registered_user_table td.nowrap a{
 margin-right:0
 }
.registered_user_table td.see_btn{
     white-space: nowrap;
    width: 1%;
}
.registered_user_table td.see_btn a.btn {
     margin-right:0px;
}

.btn-action{
	margin: 10px 15px;
}

</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">

                    <span class="caption-subject bold uppercase"><?php echo __(registered_users); ?> </span> &nbsp;&nbsp;
                </div>
            </div>


            <div class="portlet-body">
                <div class="portlet light bordered">
                    <div class="portlet-title">
						
					<?php 
						$session = $this->request->session();
					$language='';
					if($session->read('language') == "en_US"){
					  $language = "english";
					}else if($session->read('language') == "en_SP"){
					  $language = "spanish";
					}else{
					  $language = "portuguese";
					} ?>
					<div class="row">
                            <div class="caption font-dark viewUsertable">
                                <div class="table-scrollable">
                                  <table class="table table-striped table-bordered order-column registered_user_table"> 
                                    <tr> 
                                      <td class="nowrap"><?php echo __(accesspage); ?></td>
                                       <td class="nowrap"><a  href="<?php echo WEB_REGISTRATION_URL.md5($application_id).'?lang='.$language; ?>" target="_blank" class="btn btn-success"><?php echo __(see); ?></a> </td>
                                      <td><?php echo WEB_REGISTRATION_URL.md5($application_id).'?lang='.$language; ?> </td>  
									 
                                    </tr>
                                  </table>
                                </div>                                        
                            </div>
                        </div>  
                        <div class="row">
                                <button data-target="#myModal" class="btn btn-action" style="background-color: black;color:#FFF" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'webregistration/delete-chat/'.$application_id;?>">
                                <?php echo __(deletechat); ?> 
                        </button>

                        <a class="btn btn-action" style="background-color: green;color:#FFF" href="<?php echo CHAT_CSV_URL.'chatfiles/exportchat/'.$application_id;?>">
                                <?php echo __(downloadchatcsv); ?> 
                        </a>

                        <a class="btn btn-action" style="background-color: green;color:#FFF" href="<?php echo CHAT_CSV_URL.'chatfiles/exportchattxt/'.$application_id;?>">
                                <?php echo __(downloadchattxt); ?> 
                        </a>

                        </div>
						<div class="caption font-dark">
                        <a class="btn red btn-outline sbold" data-toggle="modal" href="#import"> <?php echo __(ImportUsers); ?></a>
						<a  class="btn green btn-outline sbold" href="<?php echo HTTP_ROOT.'webregistration/addUserForm/'.$application_id;?>"><i class="fa fa-plus"></i></a>
						</div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body form_listing">
                        <table class="table table-striped table-bordered table-hover dataTable" width="100%" id="webRegistrationViewUser">
                           <thead>
                                                   <tr>
                                                       <th><?php echo __(Name); ?></th>
                                                       <th><?php echo __(phone); ?></th>
                                                       <th><?php echo __(Email); ?></th>
                                                       <th><?php echo __(ManagerName); ?></th>
                                                       <th><?php echo __(created); ?></th>
                                                       <th><?php echo __(Status); ?></th>
                                                       <th><?php echo __(chat); ?></th>
                                                       <th><?php echo __(action); ?></th>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                               <?php
                                               if(!empty($webUser)){
                                               foreach ($webUser as $user): ?>
                                               <tr>
                                                   <td><?php echo  $user->firstName; ?></td>
                                                   <td><?php echo  $user->userMobile;?></td>
                                                   <td><?php if($user->userEmail != '') echo  $user->userEmail; else echo "-";?></td>
                                                   <td><?= @$user->user->name ?></td>
                                                   <td><?php echo  date('d-m-Y', strtotime( $user->created_datetime ) ) ?></td>
                                                   <td><?php
                                                   /*if (isset($authUser['role']) && (in_array($authUser['role'],['1','5']))){*/
                                                       if($user->userStatus==true){?>
                                                                <a href='<?php echo HTTP_ROOT."webregistration/approval/".$application_id.'/'.$user->userId.'/0';?>' class='label label-success'><?php echo __(Approved); ?></a>
                                                        <?php }else{?>
                                                            <a href='<?php echo HTTP_ROOT."webregistration/approval/".$application_id.'/'.$user->userId.'/1';?>' class='label label-danger'><?php echo __(Not_Approved); ?></a>
                                                        <?php }
                                                    /* }else {
                                                         if($user->userStatus==true){?>
                                                                                                                        <a href='#' class='label label-success'>Approved</a>
                                                                                                                <?php }else{?>
                                                                                                                    <a href='#' class='label label-danger'>Not-Approved</a>
                                                                                                                <?php }
                                                     }*/?></td>

                                                    <td><?php

                                                    if($user->blockStatus==true){?>
                                                    <a href='<?php echo HTTP_ROOT."webregistration/blockusers/".$application_id.'/'.$user->userId.'/0';?>' class='label label-success'><?php echo __(unblock); ?></a>
                                                    <?php }else{?>
                                                    <a href='<?php echo HTTP_ROOT."webregistration/blockusers/".$application_id.'/'.$user->userId.'/1';?>' class='label label-danger'><?php echo __(block); ?></a>
                                                    <?php }
                                                    ?></td>

                                                   <td><a href="<?php echo HTTP_ROOT.'webregistration/editUserForm/'.$application_id.'/'.$user->userId;?>"  class="btn btn-xs btn-primary"><i class="fa fa-edit"></i></a> <a href="<?php echo HTTP_ROOT.'webregistration/delete/'.$application_id.'/'.$user->userId;?>" onclick="return confirm('Do you want to delete?')" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></a></td>
                                               </tr>
                                               <?php endforeach;
                                               } ?>

                                               </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
<div class="modal fade" id="import" tabindex="-1" role="basic" aria-hidden="true" style="display: none;">
                                           <?php echo $this->Form->create('$basicInfo', [
                                           						'url' => ['controller' => 'Webregistration', 'action' => 'importUser'],
                                           						'class'=>'form-horizontal form-label-left',
                                           						'id'=>'importUser',
                                           						'enctype'=>'multipart/form-data',
                                           						'novalidate'=>'novalidate',
                                           						 'autocomplete'=>'off',

                                           					]);?>
                                           	<input type="hidden" value="<?php echo $application_id; ?>" name="application_id" id="application_id">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                        <h4 class="modal-title"><?php echo __(Upload); ?> .csv file</h4>
														<p><small><?php echo __(Upload_msg); ?></small></p>
                                                    </div>
                                                    <div class="modal-body">
                                                     <div class="item form-group">
                                                     	<div class='col-md-6 col-sm-6 col-xs-12'>
                                                     			<input type="file" name="import" id="import" accept=".csv" onchange="checkfile(this);">
                                                     	</div>
                                                     </div>
                                                     </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Close</button>
                                                        <button class="btn green" type="submit">Import</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                            <?php echo $this->form->end(); ?>
                                        </div>
										
										
<script>
function checkfile(sender) {
    var validExts = new Array(".csv");
    var fileExt = sender.value;
    fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
    if (validExts.indexOf(fileExt) < 0) {
      alert("Invalid file selected, valid files are of " + validExts.toString() + " types.");
	  $("#importUser")[0].reset;
	  document.getElementById('importUser').reset();
      return false; 
    }
    else return true;
}
</script>