<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
         <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"><?php echo __(LiveNow); ?></span>
                </div>
               
            </div> 
            <div class="portlet-body">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="tools"> </div>
            </div>
            <div class="table-scrollable portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%">
                    <thead>
                        <tr>
                            <th><?php echo __(id); ?></th>
                            <th><?php echo __(ApplicationName); ?></th>                            
                            <th><?php echo __(Account); ?></th>                           
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					//echo "<pre>"; print_r($applications); die;
                    if(!empty($applications)){
                    foreach ($applications as $application): ?>
                    <tr>
                        <td><?= $this->Number->format($application->id) ?></td>
                        <td>   <?= $application->application_name ?></td>
                        <td><a href="<?php echo HTTP_ROOT.'formbuilder/view-user?id='.$FormSingleData[$application->user->id] ?>"><?= @$application->group->group_name ?></a></td>
                    </tr>
                    <?php endforeach;
                    } ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>