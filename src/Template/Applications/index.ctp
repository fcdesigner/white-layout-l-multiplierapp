<!-- END PAGE HEADER-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
         <div class="portlet-title">
                <div class="caption font-dark">
                    <span class="caption-subject bold uppercase"><?php echo __(applications); ?></span>
                </div>
               
            </div> 
            <div class="portlet-body">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <div class="btn-group">
					<?php if($authUser['role'] != 5){ ?>
                        <a href="<?php echo HTTP_ROOT; ?>applications/add" class="nav-link ">
                            <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                <i class="fa fa-plus"></i>
                            </button>
                        </a>
					<?php } ?>	
                    </div>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="applicationListing">
                    <thead>
                        <tr>
                            <th><?php echo __(id); ?></th>
                            <th><?php echo __(ApplicationName); ?></th>                            
                            <th><?php echo __(Account); ?></th>                            
                            <th><?php echo __(ManagerName); ?></th>                            
                            <th><?php echo __(Segment); ?></th>                            
                            <th><?php echo __(broadcasts_made); ?></th>                            
                            <th><?php echo __(TargetLimit); ?></th>
                            <th><?php echo __(last_expire_plan); ?></th>
                            <th><?php echo __(expire); ?></th>
                            <th><?php echo __(created); ?></th>
                            <th><?php echo __(action); ?></th>                            
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    if(!empty($applications)){
                    foreach ($applications as $application): ?>
                    <tr>
                        <td><?= $this->Number->format($application->id) ?></td>
                        <td>   <?= $application->application_name ?></td>
                        <td><a href="<?php echo HTTP_ROOT.'formbuilder/listing/'.@$application->group->id ?>"><?= @$application->group->group_name ?></a></td>
                        <td><?= @$application->user->name ?></td>
                        <td><?php echo $segments[$application->user->segment_id] != '' ?$segments[$application->user->segment_id]:'-'; ?></td>
                        <td><a href="<?php echo HTTP_ROOT.'applications/redirect-live-made/'.@$application->manager_id; ?>"><?php echo @$this->Custom->getLiveBroadcastCount($application->group_id); ?></a></td>
                        <td><?php echo @$this->Custom->getTargetLimit($application->manager_id); ?></td>
                        <td><?php echo @$this->Custom->getLastExpirePlan($application->manager_id); ?></td>
                        <td><?php echo @$this->Custom->getExpiryDates($application->manager_id); ?></td>
                        <td><?= date('Y-m-d', strtotime( $application->created ) ) ?></td>
                        <td class="actions"> 
                        <?php if ($authUser['role'] == 1): ?>
                            <a data-target="#myModal" data-toggle="ajaxModal" href="<?php echo HTTP_ROOT.'applications/delete/'.$application->id;?>"><i class="far fa-trash-alt text-danger pull-right"></i></a>
                        <?php endif ?>
                        <?= $this->Form->postLink(__('<i class="fa fa-edit text-dark pull-right" aria-hidden="true"></i>'), ['action' => 'edit-manager', $application->id,$application->group_id, $application->group->owner_id], ['title' => 'Edit', 'escape' => false, 'class' => 'system_deleteIcon']) ?>
                        </td>
                    </tr>
                    <?php endforeach;
                    } ?>
                    
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>

<style>

.system_deleteIcon i {
    line-height: 14px !important;
}

[class*=" fa-"]:not(.fa-stack), [class*=" glyphicon-"], [class*=" icon-"], [class^=fa-]:not(.fa-stack), [class^=glyphicon-], [class^=icon-] {
    margin: 0 5px;
}

</style>