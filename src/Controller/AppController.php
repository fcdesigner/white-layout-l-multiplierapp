<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
// use Cake\Network\Email\Email;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Datasource\ConnectionManager; // For Custom query
use Cake\I18n\I18n;
use Cake\Routing\Router;
use Mautic\MauticApi;
use Mautic\Auth\ApiAuth;
use Cake\Cache\Cache;
use Cake\Controller\Component\CookieComponent;
use Aws\S3\S3Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
  var $components = array('Cookie'); 
  public function beforeFilter(Event $event){
    $this->Cookie->name = 'regulamin';
    $this->Cookie->time =  time() + (10 * 365 * 24 * 60 * 60);
    $cookie = $this->Cookie->read('gdpr_cookies');
    $this->set('cookie', $cookie);
  	Cache::disable();
    ini_set('memory_limit', '-1');
    parent::beforeFilter($event); 
	$userId = $this->Auth->user('id');
	$userRole = $this->Auth->user('role');
	$userSegment = $this->Auth->user('segment_id');
	$SegmentModel = TableRegistry::get('Segments');
	$SegmentData = $SegmentModel->find('all')->where(['id'=>$userSegment])->first();
	$userSegmentId=0;
	if(!empty($SegmentData)){
	  $userSegmentId = $SegmentData->segment_admin;
	}
  $userModel = TableRegistry::get('Users');
	$allUsers = $userModel->find('all')->where(['role IN' =>['2','3','4','5']])->toArray();
	if(($userRole==5 && empty($this->request->session()->read('AdminUsers'))) || !empty($this->request->session()->read('segmentAdminUsers'))){
	  if($userId==$userSegmentId || !empty($this->request->session()->read('segmentAdminUsers'))){	
      $allUsers = $userModel->find('all')->where(['role IN' =>['3','4'],'segment_id'=>$userSegment])->toArray();
  		$this->request->session()->write('segmentAdminUsers', $allUsers);
  	    $users[0] = 'Login as';
  	    $users[$userSegmentId] = 'Segment Admin';
  	  }else{
  		  $allUsers =array();
  	    $users[0] = 'Login as';
  	  }  
    }elseif($userRole==1 || !empty($this->request->session()->read('AdminUsers'))){
  		$allUsers = $userModel->find('all')->where(['role IN' =>['2','3','4','5']])->toArray();
  		$this->request->session()->write('AdminUsers', $allUsers);
      $users[0] = 'Login as';
      $users[3] =  'Admin';
  	}	
    if(!empty($allUsers)){
      foreach($allUsers as $key => $value)
      {
          $users[$value['id']] = ucfirst( $value['name'] );
      }        
  	}
    $this->set('managers', $users);

    $FormListingModel = TableRegistry::get("form_listings");  
    $FormSingleData=$FormListingModel->find('all')->where(['owner_id'=>$userId])->first();
    if($FormSingleData['isBlockStream'] == 1){
      $this->set('errormessagehour12New',__(errormessagehour12));
    }

    //$roles = Configure::read('sonoconfig.roles');   
    if($this->request->session()->read('language') == "en_US"){
      $roles = Configure::read('sonoconfig.roles_en');
    }else if($this->request->session()->read('language') == "en_SP"){
      $roles = Configure::read('sonoconfig.roles_es');
    }else{
      $roles = Configure::read('sonoconfig.roles_pt');
    } 

    if($this->Auth->user('role')!=null && $roles[$this->Auth->user('role')]) {
      $this->set('authRole', $roles[$this->Auth->user('role')]);  
    }
    $this->set('authUser', $this->Auth->user());  
    if(!empty($this->request->data) && $this->request->data('managers')){
      $manager = $this->request->data('managers');
      $managerData = $userModel->find('all')->where(['id' =>$manager, 'status'=> 1])->first();
      if($managerData){
        $managerData['is_admin'] = '1';          
        $this->Auth->setUser(clean($managerData,true));
      }else{
         $this->Flash->error(__(Account_deactivated));
      }
      if($this->Auth->user('role') == 4){
        return $this->redirect(['controller' => 'Formbuilder', 'action' => 'listing']);
      }else{
        return $this->redirect(['controller' => 'Users', 'action' => 'index']);
      }
    }
    $this->Auth->allow(['changeLanguage']);
    if($this->request->session()->read('language') != ""){
          I18n::locale($this->request->session()->read('language'));
    }
  }


  /**
   * Initialization hook method.
   *
   * Use this method to add common initialization code like loading components.
   *
   * e.g. `$this->loadComponent('Security');`
   *
   * @return void
   */
  public function initialize(){
    $this->loadComponent('Flash');
    $this->loadComponent('Auth', [
        'authorize' => ['Controller'], // Added this line
        'loginRedirect' => [
            'controller' => 'users',
            'action' => 'dashboard'
        ],
        'logoutRedirect' => [
            'controller' => 'users',
            'action' => 'login',
            'home'
        ],
        'Form' => [
         'scope' => ['Users.status' => 0]
      ]
    ]);  
   
    $AdminsModel = TableRegistry::get('Users');
    
    $admindata = $AdminsModel->find('all');
    $AllAdmins = $admindata->all()->first(); 
    $this->set('admins_info',$AllAdmins);
  }

  public function isAuthorized($user){
    if ((isset($user['role']) && in_array( $user['role'], ['1', '2', '3','4','5']) ) && (isset($user['status']) && $user['status'] == "1")) {
      return true;
    }
    $this->Flash->error(__(Account_banned));
    return false;
  }
	
  public function beforeRender(Event $event){
    if (!array_key_exists('_serialize', $this->viewVars) && in_array($this->response->type(), ['application/json', 'application/xml'])) {
      $this->set('_serialize', true);
	}

	$this->set('controller_requested', $event->_subject->request->controller);
	$this->set('action_requested', $event->_subject->request->action);
  }

  //function to generate auto password
  function randomPassword($length=6) {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < $length; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

  /**
  * Common mail function
  */  

  function send_email($process = "", $replace_fields = array(), $replace_with = array(), $email_template = null, $to = null, $extraTemplate = null ){
    $EmailsModel = TableRegistry::get('EmailTemplates');  
    $getTemplateData = $EmailsModel->find('all',['conditions' => ['EmailTemplates.alias' => trim($email_template)]]);
    $template =  $getTemplateData->first();
    if ($extraTemplate != ''){
      $template_data = $extraTemplate;
    }else{
      $template_data = $template->description;    
    }
    
    $replace_fields = array_merge($replace_fields, array('/sitterguide/img/logo.png','/sitterguide/img/front/mobile_nav_logo.png'));
    $logoSrc = HTTP_ROOT.'img/logo.jpg';
    
    $replace_with = array_merge($replace_with, array($logoSrc));
    $template_info = str_replace($replace_fields,$replace_with,$template_data);
    
    if($_SERVER['HTTP_HOST']=='localhost')
    { 
      //echo $template_info; die;
    }
    $SmtpSettingsModel = TableRegistry::get('SmtpSettings');
    $smtpData = $SmtpSettingsModel->find('all')->first();
  
    $from_email  = $smtpData->from_email;
    $from_name      = $smtpData->from_name;
    $smtp_port      = $smtpData->smtp_port;
    $smtp_secure    = $smtpData->smtp_secure;
    $smtp_auth      = $smtpData->smtp_auth;
    $smtp_host      = $smtpData->smtp_host;
    $username       = $smtpData->username;
    $password       = $smtpData->password;
 
     // Sample smtp configuration.
    Email::configTransport('gmailSMTP', [
      'className' => 'SMTP',
      //The following keys are used in SMTP transports
      'host' => $smtp_host,
      'port' => $smtp_port,
      'username' => $username,
      'password' => $password,
      'timeout' => 30,
      'client' => null,
      'tls' => null,
      'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
    ]);
    $this->Email = new Email();

    try {
      ob_start();
          $res = $this->Email->from([$from_email => $from_name])
        ->emailFormat('both') 
                ->to([$to => $from_name])
                ->subject($template->subject)                   
                ->send($template_info);
      ob_end_clean();
      
      return $res;
    }catch (Exception $e){
      echo 'Exception : ',  $e->getMessage(), "\n";
    }
  }

   //function to update mautic data
   public function updateMauticData($segment_id){
        $configData=Configure::read('sonoconfig.mautic');
        // ApiAuth->newAuth() will accept an array of Auth settings
        $settings = array(
            'userName'   => $configData['userName'],             // Create a new user
            'password'   => $configData['password']              // Make it a secure password
        );
        // Initiate the auth object specifying to use BasicAuth
        $initAuth = new ApiAuth();
        $auth = $initAuth->newAuth($settings, 'BasicAuth');
        $apiUrl     = $configData['apiUrl'];
        $api        = new MauticApi();
        $segmentApi = $api->newApi("segments", $auth, $apiUrl);
        $segment = $segmentApi->get($segment_id);
        if(isset($segment['list']) && !empty($segment['list'])){
            $contactApi= $api->newApi("contacts", $auth, $apiUrl);
            if($segment_id==$configData['account_subscriber_segment_id']){
                $userModel = TableRegistry::get('Users');
                $allUsers = $userModel->find('all')->where(['is_migrated_to_mutaic' =>false])->toArray();
                if(!empty($allUsers)){
                    foreach($allUsers as $allUser){
                        $data = array(
                            'title' => $allUser->name,
                            'firstname' => $allUser->name,
                            'phone' => $allUser->phone,
                            'email'     => $allUser->email,
                            'ipAddress' => $_SERVER['REMOTE_ADDR']
                        );
                        $contact = $contactApi->create($data);
                        if(!empty($contact)){
                            $contactId=$contact['contact']['id'];
                            $response = $segmentApi->addContact($segment_id, $contactId);
                            if (isset($response['success'])) {
                                $allUser->id;
                                $userData=$userModel->newEntity();
                                $userData->id=$allUser->id;
                                $userData->is_migrated_to_mutaic=true;
                                $userModel->save($userData);
                            }
                        }
                    }
                }
            }elseif($segment_id==$configData['registration_form_segment_id']){
                $userModel = TableRegistry::get('Comming_users');
                $allUsers = $userModel->find('all')->where(['is_migrated_to_mutaic' =>false])->toArray();
                if(!empty($allUsers)){
                    foreach($allUsers as $allUser){
                        $data = array(
                            'title' => $allUser->fb_name,
                            'firstname' => $allUser->fb_name,
                            'email'     => $allUser->fb_email,
                            'ipAddress' => $_SERVER['REMOTE_ADDR']
                        );
                        $contact = $contactApi->create($data);
                        if(!empty($contact)){
                            $contactId=$contact['contact']['id'];
                            $response = $segmentApi->addContact($segment_id, $contactId);
                            if (isset($response['success'])) {
                                $allUser->id;
                                $userData=$userModel->newEntity();
                                $userData->id=$allUser->id;
                                $userData->is_migrated_to_mutaic=true;
                                $userModel->save($userData);
                            }
                        }
                    }
                }
            }
        }
        return true;
  }


  function getUsername($userId){
    $UsersModel = TableRegistry::get('Users');
    $userdata = $UsersModel->find('all')->where(['id'=> $userId])->first();
    if(!empty($userdata) && $userdata['name']){
      return $userdata['name'];
    }else{
      return "";
    }
  }

  function getFullname($userId){
    $UsersModel = TableRegistry::get('Users');
    $userdata = $UsersModel->find('all')->where(['id'=> $userId])->first();
    if(!empty($userdata) && $userdata['name']){
      return $userdata['name'];
    }else{
      return "";
    }
  }

  function send_email_html($data='',$type=''){
    $SmtpSettingsModel = TableRegistry::get('SmtpSettings');
    $smtpData = $SmtpSettingsModel->find('all')->first();
    $from_email     = $smtpData->from_email;
    $from_name      = $smtpData->from_name;
    $smtp_port      = $smtpData->smtp_port;
    $smtp_secure    = $smtpData->smtp_secure;
    $smtp_auth      = $smtpData->smtp_auth;
    $smtp_host      = $smtpData->smtp_host;
    $username       = $smtpData->username;
    $password       = $smtpData->password;
   
    // Sample smtp configuration.
    Email::configTransport('gmailSMTP', [
        'className' => 'SMTP',
        //The following keys are used in SMTP transports
        'host' => $smtp_host,
        'port' => $smtp_port,
        'username' => $username,
        'password' => $password,
        'timeout' => 30,
        'client' => null,
        'tls' => null,
        'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
    ]);
    $subject = SITE_TITLE.' - '.$data['subject'];
    $this->Email = new Email();
    $this->Email->emailFormat('both');

    if($type == 'form_submited' || $type == 'auto_broadcast' || $type == 'targets_error' || $type == 'auto_live_error'){
      for ($i=0; $i < count($data['email']); $i++) { 
        $arr = explode("@" , $data['email'][$i]);
        $name = ucwords(str_replace("_", " ", trim($arr[0])));
        $email = trim($data['email'][$i]);
        if($i == 0){
          $this->Email->to([$email => $name]);
        }else{
          $this->Email->addTo([$email => $name]);    
        }
      }
    }else{
      $this->Email->to([$data['email'] => $data['name']]);        
    }
    if($type == 'auto_live_error'){
      $this->Email->cc(['suporte@multiplierapp.com.br' => 'Multiplierapp']);
    }
        //#728 - BCC to suporte@multiplierapp.com.br
        $this->Email->bcc(['suporte@multiplierapp.com.br' => 'Suporte MultiplierApp']);

    $this->Email->subject($subject);
    $this->Email->replyTo([$from_email => $from_name]);
    $this->Email->from([$from_email => $from_name]);
    $this->Email->viewVars($data);
    $this->Email->template('/Email/html/'.$type);
    try 
    {
      $return = $this->Email->send();
      Email::dropTransport('gmailSMTP');
      return $return;
    } catch (\Exception $e) {
    //  echo 'Exception : ',  $e->getMessage(), "\n"; die();
    }
  }
  // below are helper funtions user over the project
  function user($param)
  {
    return $this->Auth->user($param);
  }

  function admin_access()
  {    
    if (  ( $this->user('role') != 1 ) && ( $this->user('role') != 2 ) )  
    {
        $this->Flash->error(__(error_403));
        return $this->redirect(['controller' => 'users']);
    }else{
      return true;
    }
  }

  function updateAccessToken($refreshToken){
    $url_token = 'https://www.googleapis.com/oauth2/v4/token';      
    $curlPost = 'client_id='.GMAIL_APP_ID.'&client_secret='.GMAIL_APP_SECRET.'&refresh_token='.$refreshToken.'&grant_type=refresh_token';
    $ch = curl_init();    
    curl_setopt($ch, CURLOPT_URL, $url_token);    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
    curl_setopt($ch, CURLOPT_POST, 1);    
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);  
    $data = json_decode(curl_exec($ch), true);  
    $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);    
    return $data;
  }

  function updateFacebookRmtp($stream_key, $trial_sentence="",$form_id = ""){
    $commingUsersModel = TableRegistry::get("CommingUsers");
    $FormListingModel = TableRegistry::get("form_listings");

    if($form_id != ""){
      $stream = $commingUsersModel->find('all')->where(['id' =>$form_id])->first();
    }else{
        $stream = $commingUsersModel->find('all')->where(['stream_key' =>$stream_key])->first();
      
    }
  
    if(!$stream){
      return array('status' => 'error', 'message' => 'No response found'  );
    }

    $formId = $stream['form_id'];
    $FormSingleData = $FormListingModel->find('all')->where(['form_id'=>$formId])->first();   
    $form_source_name =  $FormSingleData['wowza_source_name'];

    /************* Updating stream key ***************/

    $access_token = $stream['access_token'];
    $stream_title = $stream['stream_title'];
    $stream_description = $stream['stream_description'];

    if(!empty($trial_sentence)){
      $stream_description .= " ".$trial_sentence;
    }

    $stream_title = $this->cleanString($stream_title);
    $stream_description = $this->cleanString($stream_description);

    $flag = $stream['flag'];
    /*Remove This if loop*/
    if($flag  == "youtube"){
     // echo "<pre>"; print_r($stream);
       $ac = $stream->access_token;
       $option = array(
            'part' => 'id,snippet,contentDetails,status', 
            'mine' => 'true',
            'broadcastType' => 'persistent'
        );
          $headerArr = array('Authorization: Bearer '.$ac);
          $postBroadcastResponse = $this->getCurl('https://www.googleapis.com/youtube/v3/liveBroadcasts?'.http_build_query($option, 'a', '&'),$headerArr);
          $broadcastOp = json_decode($postBroadcastResponse);
          if(isset($broadcastOp->error) ){

            if(isset($broadcastOp->error)){
             if($stream->reftoken != ""){

              $tokenRes = $this->updateAccessToken($stream->reftoken);
             if(isset($tokenRes['error'])){
           // print_r($tokenRes);   die("sdasd");
                $commingUsersData = $commingUsersModel->newEntity();
                $commingUsersData->id       = $stream['id'];
                $commingUsersData->wowza_stream_status  = 'Error';
                $commingUsersData->wowza_status_message  = $tokenRes['error_description'];
                $commingUsersData->live_status  = 0;
                $commingUsersData->disabled  = 1;

                $commingUsersData->last_disabled_error= $tokenRes['error_description'];
                $commingUsersData->last_update = getUtcTime();
                if($commingUsersModel->save($commingUsersData)){
                  $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
                }
                return array('status' => 'error', 'message' =>  $tokenRes['error_description'], 'fb_response' => $postBroadcastResponse, 'target_id' =>  $stream->id  );

              }else{
                 if(isset($tokenRes['access_token'])){
                   $commingUsersData = $commingUsersModel->newEntity();
                   $commingUsersData->id       = $stream['id'];
                   $commingUsersData->last_update = getUtcTime();
                   $ac = $commingUsersData->access_token       = $tokenRes['access_token'];
                    if($commingUsersModel->save($commingUsersData)){
                       $headerArr = array('Authorization: Bearer '.$tokenRes['access_token']);
                      $postBroadcastResponse = $this->getCurl('https://www.googleapis.com/youtube/v3/liveBroadcasts?'.http_build_query($option, 'a', '&'),$headerArr);
                      $broadcastOp = json_decode($postBroadcastResponse);
                    }


                 }
              }
            
             }
            }else{
              
              $commingUsersData = $commingUsersModel->newEntity();
              $commingUsersData->id       = $stream['id'];
              $commingUsersData->wowza_stream_status  = 'Error';
              $commingUsersData->wowza_status_message  = $broadcastOp->error->errors[0]->message;
              $commingUsersData->live_status  = 0;
              $commingUsersData->disabled  = 1;
              $commingUsersData->last_disabled_error= $broadcastOp->error->errors[0]->message;
              $commingUsersData->last_update = getUtcTime();
              if($commingUsersModel->save($commingUsersData)){
                $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
              }
              return array('status' => 'error', 'message' => $broadcastOp->error->errors[0]->message, 'fb_response' => $postBroadcastResponse, 'target_id' =>  $stream->id  );
            }
          }
          $response =array();
         
          $boundStreamId = $broadcastOp->items[0]->contentDetails->boundStreamId;
          $option = array(
              'part' => 'snippet,cdn', 
              'id' => $boundStreamId
          );
          $postStreamResponse = $this->getCurl('https://www.googleapis.com/youtube/v3/liveStreams?'.http_build_query($option, 'a', '&'),$headerArr);
          $streamOp = json_decode($postStreamResponse);
          $response['rtmpUrl'] = $streamOp->items[0]->cdn->ingestionInfo->ingestionAddress;
          $response['streamName'] = $streamOp->items[0]->cdn->ingestionInfo->streamName;
          $response['streamId'] = $streamOp->items[0]->id;
          
          if(isset($streamOp->error) ){

              $commingUsersData = $commingUsersModel->newEntity();
              $commingUsersData->id       = $stream['id'];
              $commingUsersData->wowza_stream_status  = 'Error';
              $commingUsersData->wowza_status_message  = $streamOp->error->errors[0]->message;
              $commingUsersData->last_disabled_error= $streamOp->error->errors[0]->message;
              $commingUsersData->live_status  = 0;
              $commingUsersData->disabled  = 1;
              $commingUsersData->last_update = getUtcTime();
              if($commingUsersModel->save($commingUsersData)){
                $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
              }
              return array('status' => 'error', 'message' => $streamOp->error->errors[0]->message, 'fb_response' => $postStreamResponse, 'target_id' =>  $stream->id  );
          }else{
              $commingUsersData = $commingUsersModel->newEntity();
              $commingUsersData->id       = $stream['id'];
              $commingUsersData->stream_key       = $response['streamName'];
              $option = array(
              'part' => 'snippet,statistics', 
              'id' => $stream['channelId']
            );
            $channelInfo = $this->getCurl('https://www.googleapis.com/youtube/v3/channels?'.http_build_query($option, 'a', '&'),$headerArr);
            $channelInfo = json_decode($channelInfo);
            //$response['channelName'] = $channelInfo->items[0]->snippet->title;
            
            $commingUsersData->followers = $channelInfo->items[0]->statistics->subscriberCount;
            $commingUsersData->last_update = getUtcTime();
            $commingUsersModel->save($commingUsersData);
          }
          $liveBroadcast->id = $broadcastOp->items[0]->id;
          $liveBroadcast->snippet['title']         = htmlspecialchars($stream_title);
          $liveBroadcast->status['privacyStatus']         = 'public';
          $liveBroadcast->snippet['publishedAt']         =  $broadcastOp->items[0]->snippet->publishedAt;
          $liveBroadcast->snippet['description']         = htmlspecialchars($stream_description);
          $liveBroadcast->contentDetails['enableEmbed']         = false;
          $liveBroadcast->contentDetails['enableDvr']         = true;
          $liveBroadcast->contentDetails['recordFromStart']         = true;
          $liveBroadcast->contentDetails['enableContentEncryption']         = '';
          $liveBroadcast->contentDetails['startWithSlate']         = '';
          $liveBroadcast->contentDetails['monitorStream']['enableMonitorStream'] =  true;
          $liveBroadcast->contentDetails['monitorStream']['broadcastStreamDelayMs'] =  0;
          $postParms = json_encode($liveBroadcast);

          $headerArr = array('Content-Type: application/json','Authorization: Bearer '.$ac);
          $postBroadcastResponse = $this->putCurl('https://www.googleapis.com/youtube/v3/liveBroadcasts?part=id,snippet,contentDetails,status',$postParms,$headerArr);
          $broadcastOp = json_decode($postBroadcastResponse);
          if(isset($broadcastOp->error) ){
            $commingUsersData = $commingUsersModel->newEntity();
              $commingUsersData->id       = $stream['id'];
              $commingUsersData->wowza_stream_status  = 'Error';
              $commingUsersData->wowza_status_message  = $broadcastOp->error->errors[0]->message;
              $commingUsersData->last_disabled_error= $broadcastOp->error->errors[0]->message;
              $commingUsersData->live_status  = 0;
              $commingUsersData->disabled  = 1;
              $commingUsersData->last_update = getUtcTime();
              if($commingUsersModel->save($commingUsersData)){
                $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
              }
              return array('status' => 'error', 'message' => $broadcastOp->error->errors[0]->message, 'fb_response' => $postBroadcastResponse, 'target_id' =>  $stream->id  );
          }
          $rdata =  $stream['app_name'].' '.$form_source_name.' '.$response['rtmpUrl']; 
            return array('status' => 'success', 'message' => $rdata, 'key' => $response['streamName'] );
      }else if($flag == 'timeline') {
      $option = $stream['flag_option'];
      $identifier =  trim($stream['user_identifier_id']);
      
      $result = shell_exec("curl " . FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded; charset=utf8mb4;' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description."&privacy=%7B%22value%22%3A%22".$option."%22%7D&suppress_http_code=1'");
      $data = json_decode($result,true);
      
      if($data['stream_url'] == ''){
        $commingUsersData = $commingUsersModel->newEntity();
        $commingUsersData->id       = $stream['id'];
        $commingUsersData->wowza_stream_status  = 'Error';
        $commingUsersData->wowza_status_message  = $data['error']['message'];
        $commingUsersData->last_disabled_error= $data['error']['message'];
        $commingUsersData->live_status  = 0;
        $commingUsersData->disabled  = 1;
        $commingUsersData->last_update = getUtcTime();
        if($commingUsersModel->save($commingUsersData)){
          $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
        }
        return array('status' => 'error', 'message' => 'No rtmp found', 'fb_response' => $data, 'target_id' =>  $stream->id  );
      }

      $new_stream_url = $data['stream_url'];
      /* Get Stream Key */
      $explode_stream = explode('/rtmp/',$new_stream_url);
      $stream_key = $explode_stream[1];
      /* ************ */
      $new_stream_url_secure = $data['secure_stream_url'];
      $rmtp_stream_id = $data['id'];    
      
    } else if($flag == 'page' || $flag == 'group' || $flag == 'event') {
        $option = $stream['flag_option'];
        $identifier = $option;
        $access_token = $stream['access_token'];
        
        $result = shell_exec("curl " . FB_GRAPH_API_HOST . $identifier."'/live_videos?access_token=".$access_token."' -H 'Host: graph.facebook.com' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64; rv:47.0) Gecko/20100101 Firefox/47.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5'  -H 'Content-Type: application/x-www-form-urlencoded; charset=utf8mb4' -H 'Referer: https://developers.facebook.com' -H 'origin: https://developers.facebook.com' --data 'debug=all&format=json&method=post&pretty=0&title=".$stream_title."&description=".$stream_description."&suppress_http_code=1'");
                  
        $data = json_decode($result,true);

        if($data['stream_url'] == ''){
          $commingUsersData = $commingUsersModel->newEntity();
          $commingUsersData->id       = $stream['id'];
          $commingUsersData->wowza_stream_status  = 'Error';
          $commingUsersData->last_disabled_error=$commingUsersData->wowza_status_message  = $data['error']['message'];
          $commingUsersData->live_status  = 0;
          $commingUsersData->disabled  = 1;
          $commingUsersData->last_update = getUtcTime();
          if($commingUsersModel->save($commingUsersData)){
            $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
          }
          return array('status' => 'error', 'message' => 'No rtmp found', 'fb_response' => $data , 'target_id' =>  $stream->id );
        }
        $new_stream_url = $data['stream_url'];
        /* Get Stream Key */
        $explode_stream = explode('/rtmp/',$new_stream_url);
        $stream_key = $explode_stream[1];
        /**************/
        $new_stream_url_secure = $data['secure_stream_url'];
        $rmtp_stream_id = $data['id'];        
      } else {
        $identifier =  str_replace('?fields=cover','',$_SESSION["identifier"]);
        $accesstoken2 = $_SESSION["accesstoken"];
        $new_token = substr($accesstoken2, strpos($accesstoken2, "access_token="));
        $access_token = str_replace("access_token=","",$new_token);
        
        $result = shell_exec('curl -k -X POST '. FB_GRAPH_API_HOST .$identifier.'/live_videos \
        -F "access_token='.$access_token.'" \
        -F "published=true"');
        $data = json_decode($result,true);
        
        if($data['stream_url'] == ''){
          $commingUsersData = $commingUsersModel->newEntity();
          $commingUsersData->id = $stream['id'];
          $commingUsersData->wowza_stream_status  = 'Error';
          $commingUsersData->last_disabled_error=$commingUsersData->wowza_status_message  = $data['error']['message'];
          $commingUsersData->live_status  = 0;
          $commingUsersData->disabled  = 1;
          $commingUsersData->last_update = getUtcTime();
          if($commingUsersModel->save($commingUsersData)){
            $this->deactivateStreamTarget($stream->app_name, $stream->target_name);
          }
          return array('status' => 'error', 'message' => 'No rtmp found', 'fb_response' => $data , 'target_id' =>  $stream->id );
        }

        $new_stream_url = $data['stream_url'];
        /* Get Stream Key */
        $explode_stream = explode('/rtmp/',$new_stream_url);
        $stream_key = $explode_stream[1];
        /* ************ */
        $new_stream_url_secure = $data['secure_stream_url'];
        $rmtp_stream_id = $data['id'];  
      }


      // Extend FB token
      $long_access_token  = $this->extendFbToken($access_token,$stream['flag'],$identifier);
      
      $access_token  = $stream['access_token'];

      if ($long_access_token){
        $access_token  = $long_access_token;
      }

      //$followerCount   = $this->get_follower($stream['user_identifier_id'],$stream['access_token'],$stream['flag']);
      $followerCount   = $this->get_follower($stream['user_identifier_id'], $access_token, $stream['flag']);

      // updating in database
      $commingUsersData               = $commingUsersModel->newEntity();
      $commingUsersData->id           = $stream['id'];
      $commingUsersData->rtmp_url     = $new_stream_url;
       $commingUsersData->new_stream_url_secure = $new_stream_url_secure;
      //$commingUsersData->stream_key   = $new_stream_key[1];
      $commingUsersData->stream_key   = $stream_key;
      $commingUsersData->access_token = $access_token;
      $commingUsersData->followers    = $followerCount;
      $commingUsersData->wowza_status_message  = "Success";
      $commingUsersData->wowza_stream_status  = 'Waiting...';
      $commingUsersData->followers_updated_on = getUtcTime();
      $commingUsersData->last_update = getUtcTime();
      if ($commingUsersModel->save($commingUsersData)) {
        // remove old stream
         $removed = $this->removeWowzaStream($stream['app_name'], $stream['target_name'], $form_source_name, $new_stream_url_secure);
        $enabled = "false";
        if($stream['live_status'] == 1){
          $enabled = "true";
        }
        $added = $this->addWowzaStream($stream['app_name'], $stream['target_name'], $form_source_name, $new_stream_url_secure, $enabled);
        if ( $removed && $added ) {
        	$rdata =  $stream['app_name'].' '.$form_source_name.' '.$new_stream_url; 
        	return array('status' => 'success', 'message' => $rdata, 'key' => $stream_key );
        }else{
        	return array('status' => 'error', 'message' => 'Wowza api error'  );
        }
        }else{
        return array('status' => 'error', 'message' => 'unable to update in database'  );
      }
  }

  function postCurl($url, $data_string , $headerArr = false){
    if(!$headerArr){
      $headerArr = array('Content-Type:application/json', 'Content-Length: ' . strlen($data_string));
    }
    $ch=curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER,$headerArr);
    $result = curl_exec($ch);
    if(curl_error($ch))
    {
        echo 'error:' . curl_error($ch);
    }
    $result = str_replace("\xEF\xBB\xBF",'',$result);
    return $result;
   }

  function getCurl($url, $headerArr = false){
    if(!$headerArr){
        $headerArr = array("cache-control: no-cache");
    }
    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => $headerArr,
    ));
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;    
  }

  function putCurl($url,$data ,$headerArr = false){
    $curl = curl_init($url);
    if(!$headerArr){
        $headerArr =  array('Content-Type: application/json');
      }
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");   
    curl_setopt($curl, CURLOPT_HTTPHEADER,$headerArr);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curl);
    $err = curl_error($curl);
    curl_close($curl);
    return $response;    
  }

  function removeWowzaStream($appName, $targetName, $sourceName, $new_stream_url=''){
    $url_remove = WOWZA_DOMAIN ."api/v1/removeStream";
    $targetName = clearTarget($targetName);
    $data_remove = array('app_name' => $appName,'target_name' => $targetName, 'source_name' => $sourceName);
    $result_remove = $this->postCurl($url_remove, json_encode($data_remove));
    $result = json_decode($result_remove);
    if ($result->status == "success") {
    	return true;
    }
    return false;
  }

  function activateStreamTarget($appName, $targetName){
    $url = WOWZA_DOMAIN ."api/v1/activateStreamTarget";
    $targetName = clearTarget($targetName);
    $data = array('app_name'=> $appName, 'target_name'=> [$targetName]);
    $result = $this->postCurl($url, json_encode($data));
    $result = json_decode($result);
    //print_r($result); die;
    if($result->status == "success") {
      return true;
    }else{
      return false;
    }
  }

  function deactivateStreamTarget($appName, $targetName){
    $url = WOWZA_DOMAIN ."api/v1/deactivateStreamTarget";
    $targetName = clearTarget($targetName);
    $data = array('app_name'=> $appName, 'target_name'=> [$targetName]);
    $result = $this->postCurl($url, json_encode($data));
    $result = json_decode($result);
    if($result->status == "success") {
      return true;
    }else{
      return false;
    }
  }

  function addWowzaStream($appName, $targetName, $sourceName, $new_stream_url, $enabled){    
    $url_add = WOWZA_DOMAIN ."api/v1/addStream";
    $targetName = clearTarget($targetName);
    $data_add = array('app_name' => $appName,'target_name' => $targetName, 'source_name' => $sourceName, 'rtmp_url' => $new_stream_url, 'enabled' => $enabled );
    $result_add = $this->postCurl($url_add, json_encode($data_add));
    $result = json_decode($result_add);
    if ($result->status == "success") {
    	return true;
    }
    return false;
  }

  function addWowzaYoutubeStream($appName, $targetName, $sourceName, $stream_key,$enabled){   
    $url_add = WOWZA_DOMAIN ."api/v1/addYTStream";
    $targetName = clearTarget($targetName);
    $data_add = array('app_name' => $appName,'target_name' => $targetName, 'source_name' => $sourceName, 'stream_key' => $stream_key, 'enabled' => $enabled );
   
    $result_add = $this->postCurl($url_add, json_encode($data_add));
    $result = json_decode($result_add);
    if ($result->status == "success") {
      return true;
    }
    return false;
  }

  /*function addWowzaChaining($appName, $targetName, $sourceName, $destAppName){
    
    $url = WOWZA_DOMAIN ."api/v1/addChainning";
    $targetName = clearTarget($targetName);

    $data = array('source_name' => $sourceName, 'target_name' => $sourceName, 'app_name' => $appName, 'dest_app_name' => $destAppName, 'host_ip'=> '127.0.0.1');
    $response = $this->postCurl($url, json_encode($data));
    $result = json_decode($response);
    if ($result->status == "success") {
      return true;
    }
    return false;
  }*/

  function get_follower($identifier,$access_token,$type){
    $count = 0;
    /************************/
    switch ($type) {
      case 'timeline':
        //$apiUrl = "https://graph.facebook.com/v2.8/".$identifier.'/?fields=friends&access_token='.$access_token;
        $apiUrl = FB_GRAPH_API_HOST .$identifier.'/?fields=friends&access_token='.$access_token;
        $followingResult = json_decode( $this->getCurl($apiUrl));                
        if ( !empty($followingResult->friends->summary) ) { 
          $count = $followingResult->friends->summary->total_count;
        }/*else{ $count = 0;}*/
        break;
      case 'page':
        //$apiUrl = "https://graph.facebook.com/v2.8/".$identifier.'/?fields=fan_count&access_token='.$access_token;
        $apiUrl = FB_GRAPH_API_HOST .$identifier.'/?fields=fan_count&access_token='.$access_token;
        $followingResult = json_decode( $this->getCurl($apiUrl));        
        if (empty($followingResult->error)) { 
          $count = $followingResult->fan_count;
        }/*else{ $count = 0;}*/
        break;
      case 'group':
        //$apiUrl = "https://graph.facebook.com/v2.8/".$identifier.'/members?pretty=0&limit=1000&access_token='.$access_token;        
        $apiUrl = FB_GRAPH_API_HOST .$identifier.'/members?pretty=0&limit=1000&access_token='.$access_token;
        return $xx = $this->getMembersCount($apiUrl, $count = array());
        break;
      case 'event':
        //$apiUrl = "https://graph.facebook.com/v2.8/".$identifier.'/?fields=attending_count&access_token='.$access_token;
        $apiUrl = FB_GRAPH_API_HOST .$identifier.'/?fields=attending_count&access_token='.$access_token;
        $followingResult = json_decode( $this->getCurl($apiUrl));
        if (empty($followingResult->error)) { 
          $count = $followingResult->attending_count;
        }/*else{ $count = 0;}*/
        break;
      default:
        $count = 0;
        break;
    }
    /************************/
    return $count;
  }


  function getMembersCount( $url, $count = array()){
    $followingResult = json_decode( $this->getCurl( $url ) );
    $count[] = sizeof($followingResult->data);

    if (!empty($followingResult->paging->next)) 
    {        
      return $this->getMembersCount( $followingResult->paging->next, $count );
    }
    else
    {
      return $sum = array_sum($count); 
    }
  }


  function getManager(){
    if ($this->user('role') == 3 ) {
      return $this->user('id');
    }else{
      return $this->user('parent_id');
    }
  }

  function createApplication($group_id,$manager_id, $applicationName){
    //$url = WOWZA_DOMAIN.'api/v1/createApp';
    $url = WOWZA_DOMAIN.'api/v1/createAutoBroadcastApplicaton';
    $data = array('app_name' => $applicationName);
    $result = $this->postCurl( $url, json_encode($data) );
    $api_responcs = json_decode( $result );  // {  "status": "success"} else {  "status": "failure"}

    if ($api_responcs->status == 'success') {
      $applicationsModel = TableRegistry::get('Applications');
      $application = $applicationsModel->newEntity();

      $application->application_name = $applicationName;
      $application->group_id = $group_id;
      $application->manager_id = $manager_id;
      $result = $applicationsModel->save($application);
      return $result->id;     
    }
    return false;
  }

  /*function createCascadeApplication($group_id,$manager_id, $applicationName)
  {
    //$url = WOWZA_DOMAIN.'api/v1/createApp';
    $url = WOWZA_DOMAIN.'api/v1/createCascadeApplicaton';
    $data = array('app_name' => $applicationName);
    $result = $this->postCurl( $url, json_encode($data) );
    $api_responcs = json_decode( $result );  // {  "status": "success"} else {  "status": "failure"}

    if ($api_responcs->status == 'success') 
    {
      $applicationsModel = TableRegistry::get('Applications');
      $application = $applicationsModel->newEntity();

      $application->application_name = $applicationName;
      $application->group_id = $group_id;
      $application->manager_id = $manager_id;
      $result = $applicationsModel->save($application);
      return $result->id;     
    }
    return false;
  }*/

  function getNextApps($app, $length){
    $applicationsModel = TableRegistry::get('Applications');
    $app = $applicationsModel->find('all')->contain(['Groups'])->where( ['application_name' => $app])->first();      
    $groupName = $app->group->group_name;
    $appName = $app; 
    $appsArray = array();    
    for($i = 0; $i < $length; $i++) { 
      $appName = getNextApplicatoinName($appName, $groupName);
      array_push($appsArray, $appName);
    }
    return $appsArray;
  }

  function createCascadeApp($group_id,$manager_id, $applicationName, $parentCascadeApp, $isChild){
    $url = WOWZA_DOMAIN.'api/v1/createCascadeApplicaton';
    $data = array('app_name' => $applicationName);
    $result = $this->postCurl( $url, json_encode($data) );
    $api_responcs = json_decode( $result );  // {  "status": "success"} else {  "status": "failure"}
  

    if ($api_responcs->status == 'success'){
      $applicationsModel = TableRegistry::get('Applications');
      $application = $applicationsModel->newEntity();
      $application->application_name = $applicationName;
      $application->group_id = $group_id;
      $application->manager_id = $manager_id;
      $application->parent_cascade_app = $parentCascadeApp;
      $application->is_child_app = $isChild;
      $result = $applicationsModel->save($application);
      return $result->id;     
    }
    return false;
  }


  function appsChaining($appName, $targetName, $sourceName, $destAppName, $isRTSPUser){    
    $url = WOWZA_DOMAIN ."api/v1/addChainning";
    $targetName = clearTarget($targetName);

    $appLastString = substr($appName, -6);

    if($isRTSPUser &&  $appLastString == '000001'){
      $sourceName = $sourceName.".stream";
    }

    //echo $appName.'---'.$targetName.'---'.$sourceName.'....'.$destAppName;die;

    $data = array('source_name' => $sourceName, 'target_name' => $targetName, 'app_name' => $appName, 'dest_app_name' => $destAppName, 'host_ip'=> '127.0.0.1');
    $response = $this->postCurl($url, json_encode($data));
    $result = json_decode($response);



    $text = PHP_EOL. date("Y-m-d H:i:s")."-> URL -> ".$url." -> Data -> ". print_r($data, true) . " -> Response ->". print_r($result, true);
    $myfile = fopen("../webroot/cascade_logs.txt", "a") or die("Unable to open file!");
    fwrite($myfile, $text);        
    fclose($myfile);


    if ($result->status == "success") {
      return true;
    }
    return false;
  }

  function executeMultipleCascadeApps($fromApp, $parentCascadeApp, $manager_id, $targetName, $wowzaSourceName, $group_id, $isRTSPUser=0){
    $mergeAppsArray = array();
    $nextAppsArray = $this->getNextApps($fromApp, 2);

    if(substr($fromApp, -1) == '1'){
      array_push($mergeAppsArray, $fromApp);
    }else{
      $replacedApp = substr($fromApp, 0, -1)."1";
      array_push($mergeAppsArray, $replacedApp);
    }


    $isChildApp = 0;
    for($i = 0; $i < count($nextAppsArray); $i++){
      if($i > 0){
        $isChildApp = 1;
      }
      $parentCascadeApp = $this->createCascadeApp($group_id, $manager_id, $nextAppsArray[$i], $parentCascadeApp, $isChildApp);
      array_push($mergeAppsArray, $nextAppsArray[$i]);
    }
  

    if(!empty($mergeAppsArray)){
      for ($i = 0; $i < (count($mergeAppsArray) - 1); $i++) { 
        
        $customTargetName = "MasterRepeater".rand(1,999).rand(1,999);
        //$customTargetNameNew = "MasterRepeater".rand(1,999).rand(1,999);

        $this->appsChaining($mergeAppsArray[$i], $customTargetName, $wowzaSourceName, $mergeAppsArray[$i+1], $isRTSPUser);

        // Add new repeater for playermapp
        /*$appLastString = substr($mergeAppsArray[$i], -6);

        if($appLastString == '000001'){
          $this->appsChaining($mergeAppsArray[$i], $customTargetNameNew, $wowzaSourceName, "playermapp", $isRTSPUser);
        }*/
      }
    }
    return true;
  }


  function getApplicationForUser($group_id, $targetName, $wowzaSourceName){
    /*
    * Get Application from group id 
    */
    $applicationsModel = TableRegistry::get('Applications');
    $applications = $applicationsModel->find('all')->contain(['CommingUsers'])->where(['group_id'=> $group_id])->toArray();
    
    /*
    * If found only 1 application
    */
    if(count($applications) == 1){

      foreach (clean($applications) as $app ){

        $FormListingModel = TableRegistry::get("form_listings");
        $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$app->manager_id])->first();

        $this->executeMultipleCascadeApps($app->application_name, $app->id, $app->manager_id, $targetName, $wowzaSourceName, $group_id, $FormSingleData['isRTSPUser']);
      }

      $lastChildApp = $applicationsModel->find('all')->where(['group_id'=> $group_id, 'is_child_app'=> 1])->order(['Applications.created' => 'DESC'])->first();
      return (object) array('group_id'=> $group_id, 'manager_id'=> $lastChildApp['manager_id'], 'app_id'=> $lastChildApp['id'], 'app_name'=> $lastChildApp['application_name']);
    }else{
      /*
      * If found only multiple applications
      * Get last child app
      */
      //$lastChildApp = $applicationsModel->find('all')->contain(['CommingUsers'])->where(['group_id'=> $group_id, 'is_child_app'=> 1])->order(['Applications.created' => 'DESC'])->first();

      $lastChildApp = $applicationsModel->find('all')->where(['group_id'=> $group_id, 'is_child_app'=> 1])->order(['Applications.created' => 'DESC'])->first();

      if(!empty($lastChildApp)){

        $conn = ConnectionManager::get('default');

        $SQL = "SELECT COUNT( user.id ) AS user_count, app . * FROM applications AS app
        LEFT JOIN comming_users AS user ON app.id = user.app_id WHERE app.group_id = ". $group_id ." AND app.is_child_app = 1 GROUP BY app.application_name HAVING COUNT( user.id ) < " . APP_USER_LIMIT . " ORDER BY app.created ASC LIMIT 1";

        $childApp = $conn->execute( $SQL )->fetchAll('assoc');
        /*
        * Check current child app having more or less traget from user limit
        */
        //if(count($lastChildApp['comming_users']) < APP_USER_LIMIT){
        if(!empty($childApp) && !empty($childApp[0]) && count($childApp[0]['user_count']) < APP_USER_LIMIT){
          /*
          * Return object if target less than limit 
          */
          return (object) array('group_id'=> $group_id, 'manager_id'=> $childApp[0]['manager_id'], 'app_id'=> $childApp[0]['id'], 'app_name'=> $childApp[0]['application_name']);
        }else{
          /*
          * If targets count greater than limit
          * Check cascade second parent apps limit in less or greater from predefined limit 
          */
          $parentAppCount = $applicationsModel->find('all')->where(['parent_cascade_app'=> $lastChildApp['parent_cascade_app']])->count();
          if($parentAppCount < APP_LIMIT){
            /*
            * If apps count less than limit
            * Generate new single app for same parent & return object
            */
            $nextAppsArray = $this->getNextApps($lastChildApp['application_name'], 1);
            if(count($nextAppsArray) > 0){
              $app_id = $this->createCascadeApp($group_id, $lastChildApp['manager_id'], $nextAppsArray[0], $lastChildApp['parent_cascade_app'], 1);


              $parentApp = $applicationsModel->find('all')->where(['group_id'=> $group_id, 'is_child_app'=> 0, 'id'=> $lastChildApp['parent_cascade_app']])->first();

              $customTargetName = "Repeater".rand(1,999).rand(1,999);
              $this->appsChaining($parentApp['application_name'], $customTargetName, $wowzaSourceName, $nextAppsArray[0], 0);
            }
            return (object) array('group_id'=> $group_id, 'manager_id'=> $lastChildApp['manager_id'], 'app_id'=> $app_id, 'app_name'=> $nextAppsArray[0]);
          }else{
            /*
            * If apps count more than limit
            * Execute previous steps for create multiple app and process them according to logic and object return also 
            */
            $parentApp = $applicationsModel->find('all')->where(['group_id'=> $group_id, 'is_child_app'=> 0, 'parent_cascade_app'=> 0])->order(['Applications.created' => 'ASC'])->first();
            
            $FormListingModel = TableRegistry::get("form_listings");
            $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$lastChildApp['manager_id']])->first();

            $this->executeMultipleCascadeApps($lastChildApp['application_name'], $parentApp['id'], $lastChildApp['manager_id'], $targetName, $wowzaSourceName, $group_id, $FormSingleData['isRTSPUser']);
            
            $lastChildApp = $applicationsModel->find('all')->where(['group_id'=> $group_id, 'is_child_app'=> 1])->order(['Applications.created' => 'DESC'])->first();
            return (object) array('group_id'=> $group_id, 'manager_id'=> $lastChildApp['manager_id'], 'app_id'=> $lastChildApp['id'], 'app_name'=> $lastChildApp['application_name']);
          }
        }
      }else{
        $getLastApp = $applicationsModel->find('all')->where(['group_id'=> $group_id, 'parent_cascade_app !='=> 0])->order(['Applications.created' => 'DESC'])->first();
        $nextAppsArray = $this->getNextApps($getLastApp['application_name'], 1);
        if(count($nextAppsArray) > 0){
          $app_id = $this->createCascadeApp($group_id, $getLastApp['manager_id'], $nextAppsArray[0], $getLastApp['id'], 1);
          $customTargetName = "Repeater".rand(1,999).rand(1,999);
          $this->appsChaining($getLastApp['application_name'], $customTargetName, $wowzaSourceName, $nextAppsArray[0], 0);
        }
        return (object) array('group_id'=> $group_id, 'manager_id'=> $getLastApp['manager_id'], 'app_id'=> $app_id, 'app_name'=> $nextAppsArray[0]);
      }
    }
  }


  /*function getApplicationForUser($group_id, $targetName, $wowzaSourceName){
    $applicationsModel = TableRegistry::get('Applications');
    $applications = $applicationsModel->find('all')->contain(['CommingUsers'])->where( ['group_id' => $group_id])->toArray();

    foreach (clean($applications) as $app ){
      $appUserLimit = APP_USER_LIMIT;
      if(substr($app->application_name, -1) == '1'){
        $appUserLimit = 4;
      }
      $manager_id     = $app->manager_id;
      // application has user less then limit
      if ( count($app->comming_users) < $appUserLimit){ 
        $text = PHP_EOL. date("Y-m-d H:i:s")." -> Target Count - ". count($app->comming_users) . " -> API not called";
        $myfile = fopen("../webroot/target_log.txt", "a") or die("Unable to open file!");
        fwrite($myfile, $text);        
        fclose($myfile);

        $userApplicationName  = $app->application_name;
        $app_id         = $app->id;
        return (object) array('group_id'=> $group_id, 'manager_id'=> $manager_id, 'app_id'=> $app_id, 'app_name'=> $userApplicationName);
      }else{
        $lastFilledApp = $app;
      }
    }
    if (empty($userApplicationName) && empty($userApplicationName) ){
      $userApplicationName = $this->nextApplicatoinName($lastFilledApp->application_name);
      //$app_id = $this->createApplication($lastFilledApp->group_id,$lastFilledApp->manager_id, $userApplicationName);
      $app_id = $this->createCascadeApplication($lastFilledApp->group_id,$lastFilledApp->manager_id, $userApplicationName);

      // Call chaining api of wowza for cascading
      $response = $this->addWowzaChaining($lastFilledApp->application_name, $targetName, $wowzaSourceName, $userApplicationName);



      // Set logs
      $params = array('source_name' => $wowzaSourceName, 'target_name' => $wowzaSourceName, 'app_name' => $lastFilledApp->application_name, 'dest_app_name' => $userApplicationName, 'host_ip'=> '127.0.0.1');
      $text = PHP_EOL. date("Y-m-d H:i:s")." -> Target Count - ". count($app->comming_users) . " -> API called; Parameters -> ".print_r($params, true)."; API Response -> ".print_r($response, true) . "New App registered -> ID :" . $app_id;
      $myfile = fopen("../webroot/target_log.txt", "a") or die("Unable to open file!");
      fwrite($myfile, $text);        
      fclose($myfile);    
    }
    return (object) array('group_id'=> $group_id, 'manager_id'=> $manager_id, 'app_id'=> $app_id, 'app_name'=> $userApplicationName);
  }

  function nextApplicatoinName($applicationName='',$groupName='')
  {
    if (empty($groupName)) 
    { 
      $applicationsModel = TableRegistry::get('Applications');
      $app = $applicationsModel->find('all')->contain(['Groups'])->where( ['application_name' => $applicationName])->first();      
      $groupName = $app->group->group_name;
    }
    return getNextApplicatoinName($applicationName,$groupName);
  }*/

  function deleteApplication($appName){
      $appLastString = substr($appName, -6);
      if ($appLastString == '000001') {
        return true;
      }else{
        if( $this->deleteApp($appName) )
        {
          $applicationsModel = TableRegistry::get('Applications');
          $application = $applicationsModel->find('all')->where(['application_name' => $appName])->first();
          $applicationsModel->delete($application);
        }
      }
  }

  function deleteApp($appName){
    $url = WOWZA_DOMAIN.'api/v1/removeApp';
    $data = array('app_name' => $appName);
    $data_string = json_encode($data);
    $result = $this->postCurl( $url, json_encode($data) );
    $response = json_decode( $result );
    return $response->status =='success' ? true : false;
  }

  function resetApplcationStock($groupId,$appName=''){ 
    $applicationsModel = TableRegistry::get('Applications');
    $applications = clean( $applicationsModel->find('all')->contain(['CommingUsers'])->where( ['group_id' => $groupId])->order(['Applications.id' => 'DESC'])->toArray(), true);
    
    if (!empty($applications) && sizeof( $applications > 2) ) 
    {
      $lastApp = $applications[0];
      if (sizeof($lastApp['comming_users']) == 0 ){
        $this->deleteApplication($lastApp['application_name']);
      } 
    }
  }

  function extendFbToken($access_token='',$type='',$identifier){
    $extend_url = "https://graph.facebook.com/oauth/access_token?client_id=".FB_APP_ID."&client_secret=".FB_APP_SECRET."&grant_type=fb_exchange_token&fb_exchange_token=".$access_token;
    $responseLongLive = json_decode($this->getCurl($extend_url));
    return $responseLongLive->access_token;
  }

  function right($txt='',$clear=false) {
    if ($clear) {
      $myfile = fopen("./log.txt", "w") or die("Unable to open file!");
      fwrite($myfile, $txt);        
      fclose($myfile);
    }else{
      $beforAdded =  file_get_contents('./log.txt');
      $newText = $beforAdded."\n".$txt;
      $myfile = fopen("./log.txt", "w") or die("Unable to open file!");
      fwrite($myfile, $newText);        
      fclose($myfile);
    }
  }

  function changeLanguage($language = null){
   if ($this->request->is('post') || isset($_GET['language'])) {
      $session = $this->request->session();
      if(isset($_GET['language'])){
         $lang = $_GET['language'];
      }else{
         $lang = $this->request->data('language');
      }
     
      $selectedlang = '';
      switch ($lang) {
          case 1:
             $selectedlang = "en_US";
              break;
          case 2:
              $selectedlang = "en_BR";
              break;
          case 3:
              $selectedlang = "en_SP";
              break;
          
      }    
      Configure::write('Config.language', $selectedlang);
      $session->write('language', $selectedlang); 
      return $this->redirect($this->referer());
   }     
  }
  
  //function to return site current language
  function siteLanguage(){
  		$session = $this->request->session();
    	$siteLanguage = $session->read('language');
    	$selectedlang = "Portuguese";
  	   	switch ($siteLanguage) {
          case "en_US":
             $selectedlang = "English";
              break;
          case "en_BR":
              $selectedlang = "Portuguese";
              break;
          case "en_SP":
              $selectedlang = "Spanish";
              break;
      	}
		return $selectedlang;
  }
  
  function changeLanguageForEmail($id = ""){
    if($id == ""){
        $id = $this->Auth->user('id');
    }
    $session = $this->request->session();
    $siteLanguage = $session->read('language');
    $language = "Portuguese";
    $FormListingModel = TableRegistry::get("form_listings");  
    $FormSingleData = $FormListingModel->find('all')->where(['owner_id'=>$id])->first();
    if($FormSingleData != ""){
        $language = $FormSingleData['language'];
    }
   if (isset($language) && !empty($language) && trim($language)!='') {
        switch ($language) {
          case "English":
             $selectedlang = "en_US";
              break;
          case "Portuguese":
              $selectedlang = "en_BR";
              break;
          case "Spanish":
              $selectedlang = "en_SP";
              break;
          
      }    
         I18n::locale($selectedlang);
        Configure::write('Config.language', $selectedlang);
       }
   
   return $siteLanguage;     
  }

  function changeLanguageAfterEmail($language = "en_BR"){
       $session = $this->request->session();
        if (isset($language) && !empty($language) && trim($language)!='') {
        $session->write('language', $language);
        I18n::locale($language);
        Configure::write('Config.language', $language);
       }
  }

  function removeWowzaApp($appName){
    $url = WOWZA_DOMAIN ."api/v1/removeApp";
    $data = array('app_name' => $appName);
    $response = $this->postCurl($url, json_encode($data));
    $result = json_decode($response);

    if ($result->status == "failure") {
      return false;
    }else{
      return true;
    }
  }

  public function cleanString($string){
    $string =  str_replace("'","",$string);
    $string =  str_replace('"','',$string);
    $string =  str_replace('&','',$string);
    return $string;
  }

  public function allowRestrictedContent(){
    $segment_id = $this->Auth->user('segment_id'); 
    if($segment_id != 0){
          $segmentModel = TableRegistry::get("segments");
          $segmentData = $segmentModel->find()->where(['id' => $segment_id])->first()->toArray();
        //  echo "<pre>"; print_r($segmentData); die;
          if(($segmentData['name'] == "fe" || $segmentData['name'] == "Fé")){
              return true;
          }
      }
    if(in_array($this->Auth->user('role'), ['1', '2'])){
      return true;
    }else if (in_array($this->Auth->user('role'), ['3', '4'])) {
      $userId = $this->Auth->user('id');
      if($this->Auth->user('role') == 4){
        $userId = $this->Auth->user('parent_id');
      }

      $date = date("Y-m-d");
      $UserPlansModel = TableRegistry::get("user_plans");
      $PlanDataCount = $UserPlansModel->find('all')->where(['user_id' => $userId, 'DATE(start_date) <=' => $date, 'DATE(expiry_date) >=' => $date])->count();
      if($PlanDataCount > 0){
        return true;
      }else{
        return false;        
      }
    }else if (in_array($this->Auth->user('role'), ['5'])) {
      $userId = $this->Auth->user('id');
      $date = date("Y-m-d");
      $UserPlansModel = TableRegistry::get("user_plans");
      $PlanDataCount = $UserPlansModel->find('all')->where(['user_id' => $userId, 'DATE(start_date) <=' => $date, 'DATE(expiry_date) >=' => $date])->count();
      if($PlanDataCount > 0){
        return true;
      }else{
        return false;        
      }
    }
  }

   public function isAutoPrem(){
    if(in_array($this->Auth->user('role'), ['1', '2'])){
      return true;
    }else if (in_array($this->Auth->user('role'), ['3', '4'])) {
      $userId = $this->Auth->user('id');
      if($this->Auth->user('role') == 4){
        $userId = $this->Auth->user('parent_id');
      }

      $date = date("Y-m-d");
      $UserPlansModel = TableRegistry::get("user_plans");
      $PlanDataCount = $UserPlansModel->find('all')->where(['user_id' => $userId, 'DATE(start_date) <=' => $date, 'DATE(expiry_date) >=' => $date,'auto_limit >' => 0])->count();
      if($PlanDataCount > 0){
        return true;
      }else{
        return false;        
      }
    }
  }

  public function loggedAsManager($manager_id){
    if($this->Auth->user('role') == 1){
      $userModel = TableRegistry::get('Users');
      $managerData = $userModel->find('all')->where(['id' =>$manager_id, 'status'=> 1])->first();
      if($managerData){
        $managerData['is_admin'] = '1';          
        $this->Auth->setUser(clean($managerData,true));
      }else{
         $this->Flash->error(__(Account_deactivated));
      }      
      return $this->redirect(['controller' => 'Commingusers', 'action' => 'viewBroadcasts']);     
    }else{
      return false;
    }
  }

  public function updateBroadcastDetails($group_id, $streamID,$isAutoLive,$eventID= false){

    if($group_id && $streamID){
      $broadcastDetailsModel = TableRegistry::get("broadcast_details");
      $commingUserModel = TableRegistry::get('comming_users');
      if($eventID){
           $commingUserData = $commingUserModel->find()->where(['group_id' => $group_id, 'auto_live'=> 1, 'live_status'=>1])->toArray();
          $commingUserModel->updateAll(['is_premium_update' => 0], ['group_id' =>  $group_id]);
           $eventModels = TableRegistry::get("events");
           $eventDetails = $eventModels->find('all')->where(['id' => $eventID])->first();
           $otherParam = json_decode($eventDetails->other_param);
           if($otherParam->old_enable != ""){
              $old_enable = explode(',', $otherParam->old_enable);
              $UserListingModel = TableRegistry::get("users");  
              $userSingleData = $UserListingModel->find('all')->where(['id'=>$eventDetails->added_by])->first();
              $userId = $userSingleData->id;
              if($userSingleData->parent_id != 0){
                    $userId = $userSingleData->parent_id;
              }
              $applicationsModel = TableRegistry::get("applications");  
              $applicationsSingleData = $applicationsModel->find('all')->where([' manager_id'=> $userId])->first();
              $appName = $applicationsSingleData->application_name;
                foreach ($old_enable as $targetId) {
                    $commingUsersModel = TableRegistry::get("comming_users");
                    $commingUsers = $commingUsersModel->find('all')->where(['id'=>$targetId])->first(); 
                    $commingUsers->live_status = 1;
                    $commingUsersModel->save($commingUsers);

                    $status = $this->activateStreamTarget($commingUsers->app_name, $commingUsers->target_name);
                }
           }

      }else{
      $commingUserData = $commingUserModel->find()->where(['group_id' => $group_id, 'live_status'=> 1])->toArray();

      }
      

      foreach ($commingUserData as $user) {
        $broadcastDetails = $broadcastDetailsModel->newEntity(); 
        $broadcastDetails->user_identifier = $user['user_identifier_id'];
        $broadcastDetails->form_id = $user['form_id'];
        $broadcastDetails->target_id = $user['id'];
        $broadcastDetails->stream_id = $streamID;
        $broadcastDetails->broadcast_id = 0;
        $broadcastDetails->fb_title = $user['stream_title'];
        $broadcastDetails->fb_desc = $user['stream_description'];

        if($user['channelId'] != "" && $user['flag'] == "youtube"){
          $broadcastDetails->type = "yt";
          $channelInfo = $this->getCurl('https://www.googleapis.com/youtube/v3/search?part=snippet&channelId='.$user['channelId'].'&order=date&type=video&eventType=live&videoSyndicated=true&key=AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI');
          $broadcastDetails->response = $channelInfo; 
          $channelInfo = json_decode($channelInfo);
          if(isset($channelInfo->items[0]) && $channelInfo->items[0]->id->videoId){
            $broadcastDetails->video_id = $channelInfo->items[0]->id->videoId;
          }
        }else{
          $broadcastDetails->type = "fb";
          if($isAutoLive == 0){
            // Get data from targets_video_info table
            $targetVideoInfo_model = TableRegistry::get("targets_video_info");
            $targetVideoInfoData = $targetVideoInfo_model->find('all')->where(['target_id'=> $user['id'], 'stream_id' => $streamID])->first();
            $broadcastDetails->video_id = $targetVideoInfoData->videoId;

          }else{
          if($user['rtmp_url']){
            $breakUrl = explode('/rtmp/',$user['rtmp_url']);
            if(isset($breakUrl[1])){
              $videoUrl = explode('?ds',$breakUrl[1]);
              if(isset($breakUrl[0])){
                $broadcastDetails->video_id = $videoUrl[0];
              }
            }
          }          
        }
                  
        }
        $broadcastDetailsModel->save($broadcastDetails);

        // Set last action date for targets
        $CommingUserData            = $commingUserModel->newEntity();
        $CommingUserData->id        = $user['id'];
        $CommingUserData->last_action = getUtcTime();
        $commingUserModel->save($CommingUserData);
      }
    }
  }


 /*
  Method : updateStats()
  Title : Used to call updateInteractions by using form_id or stream_id
  author : Sandhya Ghatoda
  Created Date : 19-09-2019
  */
  function updateStats(){
    if(isset($_GET['form_id'])){
      if($_GET['form_id'] != 0){
        $formID = $_GET['form_id'];
        $this->updateInteractions('',$formID);
      }else{
         $this->updateInteractions();
      }
    }else if(isset($_GET['stream_id'])){
      if($_GET['stream_id'] != 0){
        $streamID = $_GET['stream_id'];
        $this->updateInteractions($streamID);
      }else{
         $this->updateInteractions();
      }
    }
    $this->redirect( Router::url( $this->referer(), true ) );
  }

  function updateVideoStatOld($streamID = "",$formId = ""){
    $broadCastStatesModel = TableRegistry::get("broadcast_details");
      if($streamID != ""){
        $broadCastStates = $broadCastStatesModel->find('all')->where(['stream_id' => $streamID, 'error'=> 0])->toArray();
      } else if($formId != ""){
        $broadCastStates = $broadCastStatesModel->find('all')->where(['form_id' => $formId, 'error'=> 0])->toArray();
      }else{
        $broadCastStates = $broadCastStatesModel->find('all')->where(['error'=> 0])->toArray(); 
      } 
      foreach ($broadCastStates as $broadCastStats) {
        $targetModel = TableRegistry::get("comming_users");
        $target = $targetModel->find('all')->where(['id' => $broadCastStats->target_id])->first(); 
        if($broadCastStats->type == "yt"){
          if($broadCastStats->video_id != ""){
            $url = "https://www.googleapis.com/youtube/v3/videos?id=".$broadCastStats->video_id."&key=AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI&part=statistics";
            $restResult = $this->getCurl( $url);
            if(!empty($restResult)){
              $broadCastStats->reaction_response = $restResult;
              $response = json_decode( $restResult );
              $broadCastStats->status = "VOD";
              $broadCastStats->error = 0;
              if(isset($response->items[0]) && isset($response->items[0]->statistics)){
                $broadCastStats->view =    isset($response->items[0]->statistics->viewCount) ? $response->items[0]->statistics->viewCount : 0;
                $broadCastStats->vlike =    isset($response->items[0]->statistics->likeCount) ? $response->items[0]->statistics->likeCount : 0;
                $broadCastStats->dislike =    isset($response->items[0]->statistics->dislikeCount) ? $response->items[0]->statistics->dislikeCount : 0;
                $broadCastStats->comment =    isset($response->items[0]->statistics->commentCount) ? $response->items[0]->statistics->commentCount : 0;
              }
            }else{
            $broadCastStats->error = 1;
              if($broadCastStats->status == "" || $broadCastStats->status != "VOD"){
              $broadCastStats->status = "UNPUBLISHED";
            }
          }
          }
        }else if($broadCastStats->type == "fb"){
          if($broadCastStats->video_id != ""){
            if (strpos($broadCastStats->video_id, '?') !== false) {
               $videos =  explode("?", $broadCastStats->video_id); 
               if(isset($videos[0])){
                $videoID = $videos[0];
               }
            }else{
              $videoID =  $broadCastStats->video_id;
            }
            $url = FB_GRAPH_API_HOST .$videoID."?fields=status,live_views,permalink_url,comments.summary(total_count).limit(0).as(comments),reactions.type(LIKE).summary(total_count).limit(0).as(like),reactions.type(LOVE).summary(total_count).limit(0).as(love),reactions.type(WOW).summary(total_count).limit(0).as(wow),reactions.type(HAHA).summary(total_count).limit(0).as(haha),reactions.type(SAD).summary(total_count).limit(0).as(sad),reactions.type(ANGRY).summary(total_count).limit(0).as(angry),reactions.type(NONE).summary(total_count).limit(0).as(none),reactions.type(THANKFUL).summary(total_count).limit(0).as(thankful),reactions.type(PRIDE).summary(total_count).limit(0).as(pride)&access_token=".$target->access_token;
              //NONE, LIKE, LOVE, WOW, HAHA, SAD, ANGRY, THANKFUL, PRIDE,comment,view
            $restResult = $this->getCurl( $url);
            $broadCastStats->reaction_response = $restResult;
            $response = json_decode( $restResult );
            if(!empty($response->error)){
              $broadCastStats->message = $response->error->message;
              $broadCastStats->error = 1;
              if($broadCastStats->status == "" || $broadCastStats->status != "VOD"){
                $broadCastStats->status = "UNPUBLISHED";
              }
            }else{
              $broadCastStats->error = 0;
              $broadCastStats->status = $response->status;
            }
            if($response->permalink_url != ""){
              $insight = explode('videos',$response->permalink_url);
              if(isset($insight[1])){
                 $insight =  str_replace('/','',$insight[1]);
                 $viewUrl = FB_GRAPH_API_HOST .$insight."?fields=video_insights{period,values,live_views,description,name,title}&access_token=".$target->access_token;
                  //NONE, LIKE, LOVE, WOW, HAHA, SAD, ANGRY, THANKFUL, PRIDE,comment,view
                  $viewRestResult = $this->getCurl( $viewUrl);
                  $broadCastStats->reaction_response .= $viewRestResult;
                  $viewResponse = json_decode( $viewRestResult );
                  if(isset($viewResponse->video_insights->data[0])){
                    $totalView = $viewResponse->video_insights->data[0]->values;
                    if(isset($totalView[0])){
                      $broadCastStats->view = $totalView[0]->value;
                    }
                  }
              }
            }
            if(isset($response->comments)){
              $broadCastStats->view = $response->live_views;
              $broadCastStats->vlike = $response->like->summary->total_count;
              $broadCastStats->comment =  $response->comments->summary->total_count;
              $broadCastStats->love = $response->love->summary->total_count;
              $broadCastStats->none = $response->none->summary->total_count;
              $broadCastStats->wow = $response->wow->summary->total_count;
              $broadCastStats->pride = $response->pride->summary->total_count;
              $broadCastStats->thankful = $response->thankful->summary->total_count;
              $broadCastStats->angry = $response->angry->summary->total_count;
              $broadCastStats->sad = $response->sad->summary->total_count;
              $broadCastStats->haha = $response->haha->summary->total_count;
            }
          }
        }
        $broadCastStats->updated_date = date("Y-m-d H:i:s");
        $broadCastStatesModel->save($broadCastStats);
      }
    }

    function  getEventTimeInformation($userID,$checkStatus = false){
        $isStop = false;
        $userModel = TableRegistry::get("users"); 
        $usersID = array();
        $userAllData = $userModel->find('all')->select(['id'])->where(['id'=>$userID])->orWhere(['parent_id' => $userID])->all()->toArray();
        if(!empty($userAllData)){
          foreach ($userAllData as $user) {
              $usersID[]  = $user->id;
          }
        }
      

        $eventModel = TableRegistry::get("events");
        date_default_timezone_set('America/Sao_Paulo');
        $daysAfter = date('Y-m-d H:i:s');

        //'server_time <' => $daysAfter,
        $currentStatus = array(3,4); //3-completed & 4-stoped
        $where = array('added_by IN'=> $usersID,'is_preminum' => 1,'is_live' => 0,'current_status NOT IN'=> $currentStatus);
        
        $eventModel = $eventModel->find('all')->where($where)->toArray();

        foreach ($eventModel as $event) {
      
            $to_time = strtotime($daysAfter);
            $from_time = strtotime($event['server_time']->format('Y-m-d H:i:s'));
            $timeDiff = round(( $from_time - $to_time) / 60,0);
            
            if($timeDiff < 30){
              $isStop = true;
            }
        }
        return $isStop;
      
    }


  public function getAllTypeMessages($constant, $param1 = "", $param2 = ""){
    $messages = array();
    $langArr = array("en_msg"=> "en_US", "pt_msg"=> "en_BR", "sp_msg"=> "en_SP");


    foreach ($langArr as $key => $value) { 

      I18n::locale($value);
      Configure::write('Config.language', $value);
      
      $params = array();
      
      if(!empty($param1)){
        array_push($params, $param1);
      }

      if(!empty($param2)){
        array_push($params, __($param2));
      }

      if(count($params) > 0){
        $messages[$key] = __($constant, $params);
      }else{
        $messages[$key] = __($constant);
      }

    }

    return $messages;
  }


    public function sendPush($user_id, $title, $message, $langMessages, $type, $identifier = 0, $thumb = "",$url="",$content,$additionalData=array()){
    $AuthTokensModel = TableRegistry::get("auth_tokens");

    $AuthData = $AuthTokensModel->find('all')->where(["user_id"=> $user_id])->first();
    if(count($AuthData) > 0){
      $fcmMsg = array(
        'body' => $message,
        'title' => $title,
        'sound' => "default",
        'color' => "#203E78",
		'priority'=> 'high'
      );

      /*** Send JSON Payload ***/
      $NotificationsModel = TableRegistry::get("notifications");
      $unreadCount = $NotificationsModel->find("all")->where(["user_id"=> $user_id, "unread"=> 1, "status"=> 1])->count();

      $unreadCount = $unreadCount + 1;      

      $fcmPayload = array(
        "title"=> $title,
        "message"=>$message,
        "type"=> $type,
        "identifier"=> $identifier,
        "user_id"=> $user_id,
        "unread"=> $unreadCount,
        "url"=> $url,
        "content"=> $content,
        "thumb"=> $thumb,
        "en_msg"=> "",
        "pt_msg"=> "",
        "sp_msg"=> "",
		"mediaType"=>$additionalData['mediaType'],
		"mediaImage"=> $additionalData['mediaImage'],
		"show_in_foreground"=>true,
		"show_in_background"=>true

      );

      if(!empty($langMessages)){
        $fcmPayload['en_msg'] = $langMessages['en_msg'];
        $fcmPayload['pt_msg'] = $langMessages['pt_msg'];
        $fcmPayload['sp_msg'] = $langMessages['sp_msg'];
      }

      $fcmFields = array(
        'to' => $AuthData['device_token'],
        'priority' => 'high',
        'notification' => $fcmMsg,
        'data' => $fcmPayload
      );

      $headers = array(
        'Authorization: key=' . FIREBASE_API_KEY,
        'Content-Type: application/json'
      );
       
      $ch = curl_init();
      curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
      curl_setopt( $ch,CURLOPT_POST, true );
      curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
      curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
      curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
      curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
      $result = curl_exec($ch );
      curl_close( $ch );
      $result = json_decode($result);
      $NotificationsModel = TableRegistry::get("notifications");
      if(count($result) > 0){
        $notification = $NotificationsModel->newEntity();
        $notification->user_id = $user_id;
        $notification->message = $message;
        
        if(!empty($langMessages)){
          $notification->en_msg = $langMessages['en_msg'];
          $notification->pt_msg = $langMessages['pt_msg'];
          $notification->sp_msg = $langMessages['sp_msg'];
        }

          if(!empty($additionalData)){
              if(isset($additionalData['mediaType'])){
                $notification->mediaType = $additionalData['mediaType'];
              }
              if(isset($additionalData['mediaImage'])){
                $notification->mediaImage = $additionalData['mediaImage'];
              }

          }


        $notification->identifier = $identifier;
        $notification->type = $type;
        $notification->unread = 1;
        $notification->url = $url;
        $notification->content = $content;
        $notification->thumb = $thumb;
		$notification->mediaType = $additionalData['mediaType'];
		$notification->mediaImage = $additionalData['mediaImage'];
        $notification->datetime = date("Y-m-d H:i:s");
        if($result->success){
          $notification->message_id = $result->results[0]->message_id;
          $notification->status = 1;
        }else{
          $notification->status = 0;
        }
        $NotificationsModel->save($notification);
      }
    }    
    return true;
  }


   public function updatePremiumAutolive($FormSingleData,$event,$eventModelObj,$first = false){
    $limit = 30;
   $CommingModel = TableRegistry::get("comming_users");  
    $i= 1;
    $this->updateSetDeactive($FormSingleData->form_id);
     $eventliveModel = TableRegistry::get("event_live");  
    if($first == false){
 
      $lastID = $eventliveModel->find('all')->where(['form_id'=>$FormSingleData->form_id])->order(['id' => 'DESC'])->first();
     
      $i = $lastID['live_order'];
      $target_id = $lastID['target_id'];

 
      $conn = ConnectionManager::get('default');
        $SQL = "SELECT * FROM comming_users where form_id = ".$FormSingleData->form_id." AND wowza_stream_status != 'Error' AND auto_live = 1 AND is_premium_update = 1 AND id > ".$target_id." LIMIT ".$limit;

        $CommmingUsersData = $conn->execute( $SQL )->fetchAll('assoc');
         $currentLive = array();
    // echo "<pre>"; print_r($CommmingUsersData); die;
    foreach ($CommmingUsersData as  $CommmingUserData) {
        if($first){
          if($CommmingUserData['live_status']){
              $currentLive[] = $CommmingUserData['id'];
          }
          $otherParm = array('old_stream_key' =>$FormSingleData->wowza_source_name, 'old_enable' =>implode(',', $currentLive) );
          $EventMainData = $eventModelObj->newEntity();
          $EventMainData->id = $event->id;
          $EventMainData->other_param = json_encode($otherParm);
          $eventModelObj->save($EventMainData);
        }
       
        $status = $this->activateStreamTarget($CommmingUserData['app_name'], $CommmingUserData['target_name']);
        if($status){
            $CommingUser = $CommingModel->newEntity();
            $CommingUser->id = $CommmingUserData['id'];
            $CommingUser->live_status = 0;
            $CommingModel->save($CommingUser);   
        }

        if($CommmingUserData['auto_live']){
            $EventData = $eventliveModel->newEntity();
            $EventData->event_id = $event->id;
            $EventData->target_id = $CommmingUserData['id'];
            $EventData->event_time = date('Y/m/d H:i:s', strtotime($event->server_time));
            $EventData->target_name = $CommmingUserData['target_name'];
            $EventData->real_name = $CommmingUserData['fb_name'];
            $EventData->form_id = $FormSingleData->form_id;
            $EventData->online = 0;
            $EventData->target_type = $CommmingUserData['flag'];
            $EventData->is_error = $CommmingUserData['wowza_stream_status'];
            $EventData->error_type = $CommmingUserData['wowza_status_message'];
            $EventData->live_order = $i+1;
            $EventData->user_identifier_id = ($CommmingUserData['flag'] == "youtube") ? $CommmingUserData['channelId'] : $CommmingUserData['user_identifier_id'];
      //      echo "<pre>"; print_r($EventData); die;
            $eventliveModel->save($EventData);
            
        }
       
    }
   }else{
    $CommmingUsersData = $CommingModel->find('all')->where(['form_id'=>$FormSingleData->form_id,'wowza_stream_status !=' => 'Error','auto_live' => 1,'is_premium_update' => 1])->limit($limit)->toArray();
    $repeatEvent = $eventliveModel->find('all')->where(['event_time'=>date('Y/m/d H:i:s', strtotime($event->server_time)),'form_id'=>$FormSingleData->form_id])->count();

    if($repeatEvent > 0){
        return false;
    }
     $currentLive = array();
    if($first){
       $CommmingUsersLiveData = $CommingModel->find('all')->where(['form_id'=>$FormSingleData->form_id,'live_status' => 1])->toArray();
        foreach ($CommmingUsersLiveData as  $CommmingUserLiveData) {
                $currentLive[] = $CommmingUserLiveData->id;
        }
        $otherParm = array('old_stream_key' =>$FormSingleData->wowza_source_name, 'old_enable' =>implode(',', $currentLive) );
        $EventMainData = $eventModelObj->newEntity();
        $EventMainData->id = $event->id;
        $EventMainData->other_param = json_encode($otherParm);
        $eventModelObj->save($EventMainData);
       
     
    }
   
    // echo "<pre>"; print_r($CommmingUsersData); die;
    foreach ($CommmingUsersData as  $CommmingUserData) {
        $status = $this->activateStreamTarget($CommmingUserData->app_name, $CommmingUserData->target_name);
        if($status){
            $CommingUser = $CommingModel->newEntity();
            $CommingUser->id = $CommmingUserData->id;
            $CommingUser->live_status = 0;
            $CommingModel->save($CommingUser);   
        }

        if($CommmingUserData->auto_live){
            $EventData = $eventliveModel->newEntity();
            $EventData->event_id = $event->id;
            $EventData->target_id = $CommmingUserData->id;
            $EventData->event_time = date('Y/m/d H:i:s', strtotime($event->server_time));
            $EventData->target_name = $CommmingUserData->target_name;
            $EventData->real_name = $CommmingUserData->fb_name;
            $EventData->form_id = $FormSingleData->form_id;
            $EventData->online = 0;
            $EventData->target_type = $CommmingUserData->flag;
            $EventData->is_error = $CommmingUserData->wowza_stream_status;
            $EventData->error_type = $CommmingUserData->wowza_status_message;
            $EventData->live_order = $i++;
            $EventData->user_identifier_id = ($CommmingUserData->flag == "youtube") ? $CommmingUserData->channelId : $CommmingUserData->user_identifier_id;
      //      echo "<pre>"; print_r($EventData); die;
            $eventliveModel->save($EventData);
            
        }
       
    }
    
    
   }
   return true;  
}

  public function getSegmentName($segmentId){
    $SegmentModel = TableRegistry::get('segments');
    $segments = $SegmentModel->find('all')->where(['id'=> $segmentId])->first();
    if(!empty($segments)){
      return $segments['name'];      
    }else{
      return "";
    }
  }

   public function updateSetDeactive($form_id){
     $CommingModel = TableRegistry::get("comming_users");
      $CommmingDeactiveUsersData = $CommingModel->find('all')->where(['form_id'=>$form_id,'wowza_stream_status !=' => 'Error'])->toArray();
      foreach($CommmingDeactiveUsersData as $CommmingDeactiveUsers){
       $status = $this->deactivateStreamTarget($CommmingDeactiveUsers->app_name, $CommmingDeactiveUsers->target_name);
      }
  }

  /*public function checkConnection(){
     $connected = fopen("http://www.google.com:80/","r");
      if($connected)
      {
        echo 1;
      } else {
       echo 2;
      }
     die();
  }*/
public function checkConnection(){
     echo 1;
     die();
  }


  /*function shareTargetCalc($form_id, $type){
    $commingUsersModel = TableRegistry::get('comming_users');
    $usersLiveModel = TableRegistry::get('comming_users_live');

    $CommingUserData = $commingUsersModel->find('all')->select(['id'])->where(['form_id'=> $form_id, 'live_status'=> 1])->toArray();
    
    if(!empty($CommingUserData)){
      if($type == 'disable'){
        $userIds = [];
        foreach ($CommingUserData as $value) {
          array_push($userIds, $value['id']);
        }

        $strIds = implode(',', $userIds);

        $liveData = $usersLiveModel->newEntity();

        $liveData->form_id = $form_id;
        $liveData->data = $strIds;
        if($usersLiveModel->save($liveData)){
          foreach ($CommingUserData as $value) {
            $status = $this->deactivateStreamTarget($value['app_name'], $value['target_name']);
            if($status){
              $CommingUser = $commingUsersModel->newEntity();
              $CommingUser->id = $value['id'];
              $CommingUser->live_status = 0;
              $commingUsersModel->save($CommingUser);   
            }
          }

          $UserData = $commingUsersModel->find('all')->select(['id'])->where(['form_id'=> $form_id, 'live_share'=> 1])->toArray();
          foreach ($UserData as $value) {
            $status = $this->activateStreamTarget($value['app_name'], $value['target_name']);
            if($status){
              $CommingUser = $commingUsersModel->newEntity();
              $CommingUser->id = $value['id'];
              $CommingUser->live_status = 1;
              $commingUsersModel->save($CommingUser);   
            }
          }
          return true;
        }else{
          return false;
        }
      }else{
        $LiveData = $usersLiveModel->find('all')->where(['form_id'=> $form_id])->first();
        $targetIds = explode(',', $LiveData['data']);

        foreach ($CommingUserData as $value) {
          $status = $this->deactivateStreamTarget($value['app_name'], $value['target_name']);
          if($status){
            $CommingUser = $commingUsersModel->newEntity();
            $CommingUser->id = $value['id'];
            $CommingUser->live_status = 0;
            $commingUsersModel->save($CommingUser);   
          }
        }

        $UserData = $commingUsersModel->find('all')->select(['id'])->where(['id IN'=> $targetIds])->toArray();
        foreach ($UserData as $value) {
          $status = $this->activateStreamTarget($value['app_name'], $value['target_name']);
          if($status){
            $CommingUser = $commingUsersModel->newEntity();
            $CommingUser->id = $value['id'];
            $CommingUser->live_status = 1; 
            $commingUsersModel->save($CommingUser);   
          }
        }
      }
    }else{
      return true;
    }

  }*/

  public function getGlobalBroadcastDetails($appId,$userList){
    $userRole = $this->Auth->user('role');
    $where = "";
    if($userRole == 3 || $userRole == 4){
      $owner_id = $this->getManager();
      $where = " where owner_id = ".$owner_id;
    }else if($userRole == 5){
       $imploadeUserList =implode(",",$userList); 
        $where = " where owner_id IN (".$imploadeUserList.")";
    }
    $conn = ConnectionManager::get('default');
    $totalStatsQuery = "SELECT SUM(total_broadcast) as total_broadcast,SUM(total_success) as total_success ,SUM(total_broadcast_time) as total_broadcast_time FROM `form_listings`".$where;
          $totalStats = $conn->execute( $totalStatsQuery )->fetch('assoc');
    return $totalStats;
  }

  public function putGlobalBroadcastDetails($LiveBroadcast){
    
    $groupModel = TableRegistry::get('groups');
    $groupData = $groupModel->find('all')->where(['id'=> $LiveBroadcast->group_id])->first();
    if(!empty($groupData)){
      $formModel = TableRegistry::get('form_listings');
      $formData = $formModel->find('all')->where(['owner_id'=> $groupData->owner_id])->first();
      if(!empty($groupData)){
          $conn = ConnectionManager::get('default');
          $TotalBroadcastTimeQuery = "SELECT SUM(TIME_TO_SEC(TIMEDIFF(LiveStream.stream_ends, LiveStream.stream_started))) as diff FROM `live_streams` as LiveStream where group_id =".$groupData->id;
          $broadcastTimeResults = $conn->execute( $TotalBroadcastTimeQuery )->fetch('assoc');
          $TotalSuccessQuery = "SELECT IFNULL(SUM(user.followers),0) as Reached FROM `live_streams` as LiveStream LEFT JOIN `broadcast_details` as detail ON detail.stream_id = LiveStream.id LEFT JOIN `comming_users` as user ON detail.target_id = user.id where stream_status = 2 AND LiveStream.group_id =".$groupData->id;
           $broadcastSuccessResults = $conn->execute( $TotalSuccessQuery )->fetch('assoc');
          $formData->total_broadcast_time  += $broadcastTimeResults['diff'];
          $formData->total_success  += $broadcastSuccessResults['Reached'];
           $formData->total_broadcast  += 1;
          $formModel->save($formData);
         return;
      }
    }
  }
/*public function getTrialSentence($owner_id){
    $GroupModel = TableRegistry::get('groups');
    $AppModel = TableRegistry::get('applications');

    $GroupData = $GroupModel->find("all")->where(["owner_id"=> $owner_id])->first();
    if(!empty($GroupData)){
      $AppData = $AppModel->find('all')->where(['group_id'=> $GroupData['group_id']])->order(['created' => 'ASC'])->first();
      if(!empty($AppData)){
        return $AppData['trial_sentence'];
      }else{
        return "";
        }
    }else{
      return ""; 
            }
          }*/

          public function getTrialSentence($owner_id){
    
    $GroupModel = TableRegistry::get('groups');
    $AppModel = TableRegistry::get('applications');

    $groupdata = $GroupModel->find("all")->where(["owner_id"=> $owner_id])->first();
    
    if(!empty($groupdata)){
      $appdata = $AppModel->find('all')->where(['group_id'=> $groupdata['id']])->order(['created' => 'ASC'])->first();
      if(!empty($appdata)){
        return $appdata['trial_sentence'];
      }else{
        return "";
        }
    }else{
      return ""; 
            }
          }

  public function uploadVideo($file,$tempPath){
    $filename = str_replace(' ', '',basename($file['file']['name']));
    $ext = pathinfo($filename, PATHINFO_EXTENSION);
    $filename = time().".".$ext;
    $video = $file["file"]["name"];
    $type = $file['file']['type'];
    $size = $file["file"]["size"];
    $temp_name = $file["file"]["tmp_name"];
    $error = $file["file"]["error"];
    $vdate = date("Y.m.d H:i:s"); // Insert date time into database.
    $mimetypes = array('video/mov', 'video/flv', 'video/avi', 'video/mpeg', 'video/mpeg4', 'video/mpg', 'video/wmv', 'video/vid', 'video/x-msvideo', 'video/3gpp', 'video/quicktime','video/x-ms-wmv', 'video/mp4','video/*');

    if($video){
      if (!in_array($type, $mimetypes)){
          return false;
      }else{
        $alphanum = "abcdefghijkmnpqrstuvwxyz23456789";
        $codekey = substr(str_shuffle($alphanum), 0, 8);
        $filename = $codekey.$filename;
        /*$image_path_stored = $uploadPath.$filename;
        move_uploaded_file($file['file']['tmp_name'], $image_path_stored);*/

         /*------------------------------------------*/
        $s3_region='sa-east-1';
        $s3_bucket='multiplierapp-video';
        $config = [
           's3' => [
               'key' => 'AKIAUKMIEK7TCYNVUWPW',
               'secret' => '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',
               'bucket' => $s3_bucket
           ]
        ];
        $s3 = S3Client::factory([
            'credentials' => [
               'key' => $config['s3']['key'],
               'secret' => $config['s3']['secret']
            ],
        'region' => $s3_region,
        'version' => 'latest'
        ]);

        $obj=$s3->putObject([
                        'Bucket'       => $config['s3']['bucket'],
                        'Key'          => $filename,
                        'SourceFile'   => $file['file']['tmp_name'],
                        'ACL'          => 'public-read',
                        'StorageClass' => 'REDUCED_REDUNDANCY'
                    ]);
        /*------------------------------------------*/

        $uploadPath = "https://multiplierapp-video.s3-sa-east-1.amazonaws.com/";

        $pathinfo = pathinfo($uploadPath.$filename);
        $fileext = $pathinfo['extension'];
        $video_name = basename($filename, $fileext);

        // Compress Video

        $filenameWithPath = $uploadPath.$filename;
        $newfilename = time().'_'.$video_name . "mp4";

        $newFilenameWithPath = $uploadPath.$newfilename;


        $finalVideo = 'ffmpeg -re -i '.$filenameWithPath.' -pix_fmt yuv420p -vcodec libx264 -preset medium -b:v 500k -maxrate 1200k -bufsize 800k -acodec aac -ac 2 -b:a 128k -ar 48000 -filter:a loudnorm -r 30 -g 60 -force_key_frames 1 -profile:v baseline -level 3.0 '.$newFilenameWithPath;

        exec($finalVideo);

        /*------------------------------------------*/

        $obj=$s3->putObject([
                        'Bucket'       => $config['s3']['bucket'],
                        'Key'          => $newfilename,
                        'SourceFile'   => $file['file']['tmp_name'],
                        'ACL'          => 'public-read',
                        'StorageClass' => 'REDUCED_REDUNDANCY'
                    ]);
        /*------------------------------------------*/

        //unlink($filenameWithPath);
        return $newfilename;
        }
      }
    }

  public function uploadYTVideo($link, $uploadPath){
        $vidId = explode('?v=', $link);
-       $filename = $title = $vidId[1];
        // exec('php '.ROOT.'/plugins/AutoBroadcast/src/Controller/php-youtube-dl-cli.php -f 18 -t '.$title.' -p promotional '. $link, $output);
         exec('/usr/local/bin/youtube-dl -f 43 -o "'.$uploadPath.'%(id)s.%(ext)s" '.$link);
        //$vidId = explode('?v=', $link);
        $filename = $title. '.webm';

        $filenameWithPath = $uploadPath.$filename;
        $fileName = $title.'_'.$vidId[1];
        $newfilename = $fileName. ".mp4";
        $newfilenameTs =  "ts_".$fileName. "ts";
        $newFilenameTsWithPath = $uploadPath.$newfilenameTs;
        $newFilenameWithPath = $uploadPath.$newfilename;
        $finalVideo = 'ffmpeg -re -i '.$filenameWithPath.' -pix_fmt yuv420p -vcodec libx264 -preset medium -b:v 500k -maxrate 1200k -bufsize 800k -acodec aac -ac 2 -b:a 128k -ar 48000 -filter:a loudnorm -r 30 -g 60 -force_key_frames 1 -profile:v baseline -level 3.0 '.$newFilenameWithPath;

        exec($finalVideo);
        $tsfile = "ffmpeg -i ".$newFilenameWithPath." -c copy -bsf:v h264_mp4toannexb -f mpegts ".$newFilenameTsWithPath;
        exec($tsfile);
        unlink($filenameWithPath);
        return $newfilename;
    }


    //function to return all country code
      public function getCountryCodesWithDialCode(){
        $countryAarray=json_decode('[ { "name": "Brazil", "dial_code": "+55", "code": "BR" },{"name": "Afghanistan", "dial_code": "+93", "code": "AF"},{"name": "Åland Islands", "dial_code": "+358", "code": "AX" }, { "name": "Albania", "dial_code": "+355", "code": "AL" }, { "name": "Algeria", "dial_code": "+213", "code": "DZ" }, { "name": "American Samoa", "dial_code": "+1684", "code": "AS" }, { "name": "Andorra", "dial_code": "+376", "code": "AD" }, { "name": "Angola", "dial_code": "+244", "code": "AO" }, { "name": "Anguilla", "dial_code": "+1264", "code": "AI" }, { "name": "Antarctica", "dial_code": "+672", "code": "AQ" }, { "name": "Antigua and Barbuda", "dial_code": "+1268", "code": "AG" }, { "name": "Argentina", "dial_code": "+54", "code": "AR" }, { "name": "Armenia", "dial_code": "+374", "code": "AM" }, { "name": "Aruba", "dial_code": "+297", "code": "AW" }, { "name": "Australia", "dial_code": "+61", "code": "AU" }, { "name": "Austria", "dial_code": "+43", "code": "AT" }, { "name": "Azerbaijan", "dial_code": "+994", "code": "AZ" }, { "name": "Bahamas", "dial_code": "+1242", "code": "BS" }, { "name": "Bahrain", "dial_code": "+973", "code": "BH" }, { "name": "Bangladesh", "dial_code": "+880", "code": "BD" }, { "name": "Barbados", "dial_code": "+1246", "code": "BB" }, { "name": "Belarus", "dial_code": "+375", "code": "BY" }, { "name": "Belgium", "dial_code": "+32", "code": "BE" }, { "name": "Belize", "dial_code": "+501", "code": "BZ" }, { "name": "Benin", "dial_code": "+229", "code": "BJ" }, { "name": "Bermuda", "dial_code": "+1441", "code": "BM" }, { "name": "Bhutan", "dial_code": "+975", "code": "BT" }, { "name": "Bolivia, Plurinational State of bolivia", "dial_code": "+591", "code": "BO" }, { "name": "Bosnia and Herzegovina", "dial_code": "+387", "code": "BA" }, { "name": "Botswana", "dial_code": "+267", "code": "BW" }, { "name": "Bouvet Island", "dial_code": "+47", "code": "BV" }, { "name": "British Indian Ocean Territory", "dial_code": "+246", "code": "IO" }, { "name": "Brunei Darussalam", "dial_code": "+673", "code": "BN" }, { "name": "Bulgaria", "dial_code": "+359", "code": "BG" }, { "name": "Burkina Faso", "dial_code": "+226", "code": "BF" }, { "name": "Burundi", "dial_code": "+257", "code": "BI" }, { "name": "Cambodia", "dial_code": "+855", "code": "KH" }, { "name": "Cameroon", "dial_code": "+237", "code": "CM" }, { "name": "Canada", "dial_code": "+1", "code": "CA" }, { "name": "Cape Verde", "dial_code": "+238", "code": "CV" }, { "name": "Cayman Islands", "dial_code": "+ 345", "code": "KY" }, { "name": "Central African Republic", "dial_code": "+236", "code": "CF" }, { "name": "Chad", "dial_code": "+235", "code": "TD" }, { "name": "Chile", "dial_code": "+56", "code": "CL" }, { "name": "China", "dial_code": "+86", "code": "CN" }, { "name": "Christmas Island", "dial_code": "+61", "code": "CX" }, { "name": "Cocos (Keeling) Islands", "dial_code": "+61", "code": "CC" }, { "name": "Colombia", "dial_code": "+57", "code": "CO" }, { "name": "Comoros", "dial_code": "+269", "code": "KM" }, { "name": "Congo", "dial_code": "+242", "code": "CG" }, { "name": "Congo, The Democratic Republic of the Congo", "dial_code": "+243", "code": "CD" }, { "name": "Cook Islands", "dial_code": "+682", "code": "CK" }, { "name": "Costa Rica", "dial_code": "+506", "code": "CR" }, { "name": "Cote dIvoire", "dial_code": "+225", "code": "CI" }, { "name": "Croatia", "dial_code": "+385", "code": "HR" }, { "name": "Cuba", "dial_code": "+53", "code": "CU" }, { "name": "Cyprus", "dial_code": "+357", "code": "CY" }, { "name": "Czech Republic", "dial_code": "+420", "code": "CZ" }, { "name": "Denmark", "dial_code": "+45", "code": "DK" }, { "name": "Djibouti", "dial_code": "+253", "code": "DJ" }, { "name": "Dominica", "dial_code": "+1767", "code": "DM" }, { "name": "Dominican Republic", "dial_code": "+1849", "code": "DO" }, { "name": "Ecuador", "dial_code": "+593", "code": "EC" }, { "name": "Egypt", "dial_code": "+20", "code": "EG" }, { "name": "El Salvador", "dial_code": "+503", "code": "SV" }, { "name": "Equatorial Guinea", "dial_code": "+240", "code": "GQ" }, { "name": "Eritrea", "dial_code": "+291", "code": "ER" }, { "name": "Estonia", "dial_code": "+372", "code": "EE" }, { "name": "Ethiopia", "dial_code": "+251", "code": "ET" }, { "name": "Falkland Islands (Malvinas)", "dial_code": "+500", "code": "FK" }, { "name": "Faroe Islands", "dial_code": "+298", "code": "FO" }, { "name": "Fiji", "dial_code": "+679", "code": "FJ" }, { "name": "Finland", "dial_code": "+358", "code": "FI" }, { "name": "France", "dial_code": "+33", "code": "FR" }, { "name": "French Guiana", "dial_code": "+594", "code": "GF" }, { "name": "French Polynesia", "dial_code": "+689", "code": "PF" }, { "name": "French Southern Territories", "dial_code": "+262", "code": "TF" }, { "name": "Gabon", "dial_code": "+241", "code": "GA" }, { "name": "Gambia", "dial_code": "+220", "code": "GM" }, { "name": "Georgia", "dial_code": "+995", "code": "GE" }, { "name": "Germany", "dial_code": "+49", "code": "DE" }, { "name": "Ghana", "dial_code": "+233", "code": "GH" }, { "name": "Gibraltar", "dial_code": "+350", "code": "GI" }, { "name": "Greece", "dial_code": "+30", "code": "GR" }, { "name": "Greenland", "dial_code": "+299", "code": "GL" }, { "name": "Grenada", "dial_code": "+1473", "code": "GD" }, { "name": "Guadeloupe", "dial_code": "+590", "code": "GP" }, { "name": "Guam", "dial_code": "+1671", "code": "GU" }, { "name": "Guatemala", "dial_code": "+502", "code": "GT" }, { "name": "Guernsey", "dial_code": "+44", "code": "GG" }, { "name": "Guinea", "dial_code": "+224", "code": "GN" }, { "name": "Guinea-Bissau", "dial_code": "+245", "code": "GW" }, { "name": "Guyana", "dial_code": "+592", "code": "GY" }, { "name": "Haiti", "dial_code": "+509", "code": "HT" }, { "name": "Heard Island and Mcdonald Islands", "dial_code": "+0", "code": "HM" }, { "name": "Holy See (Vatican City State)", "dial_code": "+379", "code": "VA" }, { "name": "Honduras", "dial_code": "+504", "code": "HN" }, { "name": "Hong Kong", "dial_code": "+852", "code": "HK" }, { "name": "Hungary", "dial_code": "+36", "code": "HU" }, { "name": "Iceland", "dial_code": "+354", "code": "IS" }, { "name": "India", "dial_code": "+91", "code": "IN" }, { "name": "Indonesia", "dial_code": "+62", "code": "ID" }, { "name": "Iran, Islamic Republic of Persian Gulf", "dial_code": "+98", "code": "IR" }, { "name": "Iraq", "dial_code": "+964", "code": "IQ" }, { "name": "Ireland", "dial_code": "+353", "code": "IE" }, { "name": "Isle of Man", "dial_code": "+44", "code": "IM" }, { "name": "Israel", "dial_code": "+972", "code": "IL" }, { "name": "Italy", "dial_code": "+39", "code": "IT" }, { "name": "Jamaica", "dial_code": "+1876", "code": "JM" }, { "name": "Japan", "dial_code": "+81", "code": "JP" }, { "name": "Jersey", "dial_code": "+44", "code": "JE" }, { "name": "Jordan", "dial_code": "+962", "code": "JO" }, { "name": "Kazakhstan", "dial_code": "+7", "code": "KZ" }, { "name": "Kenya", "dial_code": "+254", "code": "KE" }, { "name": "Kiribati", "dial_code": "+686", "code": "KI" }, { "name": "Korea, Democratic Peoples Republic of Korea", "dial_code": "+850", "code": "KP" }, { "name": "Korea, Republic of South Korea", "dial_code": "+82", "code": "KR" }, { "name": "Kosovo", "dial_code": "+383", "code": "XK" }, { "name": "Kuwait", "dial_code": "+965", "code": "KW" }, { "name": "Kyrgyzstan", "dial_code": "+996", "code": "KG" }, { "name": "Laos", "dial_code": "+856", "code": "LA" }, { "name": "Latvia", "dial_code": "+371", "code": "LV" }, { "name": "Lebanon", "dial_code": "+961", "code": "LB" }, { "name": "Lesotho", "dial_code": "+266", "code": "LS" }, { "name": "Liberia", "dial_code": "+231", "code": "LR" }, { "name": "Libyan Arab Jamahiriya", "dial_code": "+218", "code": "LY" }, { "name": "Liechtenstein", "dial_code": "+423", "code": "LI" }, { "name": "Lithuania", "dial_code": "+370", "code": "LT" }, { "name": "Luxembourg", "dial_code": "+352", "code": "LU" }, { "name": "Macao", "dial_code": "+853", "code": "MO" }, { "name": "Macedonia", "dial_code": "+389", "code": "MK" }, { "name": "Madagascar", "dial_code": "+261", "code": "MG" }, { "name": "Malawi", "dial_code": "+265", "code": "MW" }, { "name": "Malaysia", "dial_code": "+60", "code": "MY" }, { "name": "Maldives", "dial_code": "+960", "code": "MV" }, { "name": "Mali", "dial_code": "+223", "code": "ML" }, { "name": "Malta", "dial_code": "+356", "code": "MT" }, { "name": "Marshall Islands", "dial_code": "+692", "code": "MH" }, { "name": "Martinique", "dial_code": "+596", "code": "MQ" }, { "name": "Mauritania", "dial_code": "+222", "code": "MR" }, { "name": "Mauritius", "dial_code": "+230", "code": "MU" }, { "name": "Mayotte", "dial_code": "+262", "code": "YT" }, { "name": "Mexico", "dial_code": "+52", "code": "MX" }, { "name": "Micronesia, Federated States of Micronesia", "dial_code": "+691", "code": "FM" }, { "name": "Moldova", "dial_code": "+373", "code": "MD" }, { "name": "Monaco", "dial_code": "+377", "code": "MC" }, { "name": "Mongolia", "dial_code": "+976", "code": "MN" }, { "name": "Montenegro", "dial_code": "+382", "code": "ME" }, { "name": "Montserrat", "dial_code": "+1664", "code": "MS" }, { "name": "Morocco", "dial_code": "+212", "code": "MA" }, { "name": "Mozambique", "dial_code": "+258", "code": "MZ" }, { "name": "Myanmar", "dial_code": "+95", "code": "MM" }, { "name": "Namibia", "dial_code": "+264", "code": "NA" }, { "name": "Nauru", "dial_code": "+674", "code": "NR" }, { "name": "Nepal", "dial_code": "+977", "code": "NP" }, { "name": "Netherlands", "dial_code": "+31", "code": "NL" }, { "name": "Netherlands Antilles", "dial_code": "+599", "code": "AN" }, { "name": "New Caledonia", "dial_code": "+687", "code": "NC" }, { "name": "New Zealand", "dial_code": "+64", "code": "NZ" }, { "name": "Nicaragua", "dial_code": "+505", "code": "NI" }, { "name": "Niger", "dial_code": "+227", "code": "NE" }, { "name": "Nigeria", "dial_code": "+234", "code": "NG" }, { "name": "Niue", "dial_code": "+683", "code": "NU" }, { "name": "Norfolk Island", "dial_code": "+672", "code": "NF" }, { "name": "Northern Mariana Islands", "dial_code": "+1670", "code": "MP" }, { "name": "Norway", "dial_code": "+47", "code": "NO" }, { "name": "Oman", "dial_code": "+968", "code": "OM" }, { "name": "Pakistan", "dial_code": "+92", "code": "PK" }, { "name": "Palau", "dial_code": "+680", "code": "PW" }, { "name": "Palestinian Territory, Occupied", "dial_code": "+970", "code": "PS" }, { "name": "Panama", "dial_code": "+507", "code": "PA" }, { "name": "Papua New Guinea", "dial_code": "+675", "code": "PG" }, { "name": "Paraguay", "dial_code": "+595", "code": "PY" }, { "name": "Peru", "dial_code": "+51", "code": "PE" }, { "name": "Philippines", "dial_code": "+63", "code": "PH" }, { "name": "Pitcairn", "dial_code": "+64", "code": "PN" }, { "name": "Poland", "dial_code": "+48", "code": "PL" }, { "name": "Portugal", "dial_code": "+351", "code": "PT" }, { "name": "Puerto Rico", "dial_code": "+1939", "code": "PR" }, { "name": "Qatar", "dial_code": "+974", "code": "QA" }, { "name": "Romania", "dial_code": "+40", "code": "RO" }, { "name": "Russia", "dial_code": "+7", "code": "RU" }, { "name": "Rwanda", "dial_code": "+250", "code": "RW" }, { "name": "Reunion", "dial_code": "+262", "code": "RE" }, { "name": "Saint Barthelemy", "dial_code": "+590", "code": "BL" }, { "name": "Saint Helena, Ascension and Tristan Da Cunha", "dial_code": "+290", "code": "SH" }, { "name": "Saint Kitts and Nevis", "dial_code": "+1869", "code": "KN" }, { "name": "Saint Lucia", "dial_code": "+1758", "code": "LC" }, { "name": "Saint Martin", "dial_code": "+590", "code": "MF" }, { "name": "Saint Pierre and Miquelon", "dial_code": "+508", "code": "PM" }, { "name": "Saint Vincent and the Grenadines", "dial_code": "+1784", "code": "VC" }, { "name": "Samoa", "dial_code": "+685", "code": "WS" }, { "name": "San Marino", "dial_code": "+378", "code": "SM" }, { "name": "Sao Tome and Principe", "dial_code": "+239", "code": "ST" }, { "name": "Saudi Arabia", "dial_code": "+966", "code": "SA" }, { "name": "Senegal", "dial_code": "+221", "code": "SN" }, { "name": "Serbia", "dial_code": "+381", "code": "RS" }, { "name": "Seychelles", "dial_code": "+248", "code": "SC" }, { "name": "Sierra Leone", "dial_code": "+232", "code": "SL" }, { "name": "Singapore", "dial_code": "+65", "code": "SG" }, { "name": "Slovakia", "dial_code": "+421", "code": "SK" }, { "name": "Slovenia", "dial_code": "+386", "code": "SI" }, { "name": "Solomon Islands", "dial_code": "+677", "code": "SB" }, { "name": "Somalia", "dial_code": "+252", "code": "SO" }, { "name": "South Africa", "dial_code": "+27", "code": "ZA" }, { "name": "South Sudan", "dial_code": "+211", "code": "SS" }, { "name": "South Georgia and the South Sandwich Islands", "dial_code": "+500", "code": "GS" }, { "name": "Spain", "dial_code": "+34", "code": "ES" }, { "name": "Sri Lanka", "dial_code": "+94", "code": "LK" }, { "name": "Sudan", "dial_code": "+249", "code": "SD" }, { "name": "Suriname", "dial_code": "+597", "code": "SR" }, { "name": "Svalbard and Jan Mayen", "dial_code": "+47", "code": "SJ" }, { "name": "Swaziland", "dial_code": "+268", "code": "SZ" }, { "name": "Sweden", "dial_code": "+46", "code": "SE" }, { "name": "Switzerland", "dial_code": "+41", "code": "CH" }, { "name": "Syrian Arab Republic", "dial_code": "+963", "code": "SY" }, { "name": "Taiwan", "dial_code": "+886", "code": "TW" }, { "name": "Tajikistan", "dial_code": "+992", "code": "TJ" }, { "name": "Tanzania, United Republic of Tanzania", "dial_code": "+255", "code": "TZ" }, { "name": "Thailand", "dial_code": "+66", "code": "TH" }, { "name": "Timor-Leste", "dial_code": "+670", "code": "TL" }, { "name": "Togo", "dial_code": "+228", "code": "TG" }, { "name": "Tokelau", "dial_code": "+690", "code": "TK" }, { "name": "Tonga", "dial_code": "+676", "code": "TO" }, { "name": "Trinidad and Tobago", "dial_code": "+1868", "code": "TT" }, { "name": "Tunisia", "dial_code": "+216", "code": "TN" }, { "name": "Turkey", "dial_code": "+90", "code": "TR" }, { "name": "Turkmenistan", "dial_code": "+993", "code": "TM" }, { "name": "Turks and Caicos Islands", "dial_code": "+1649", "code": "TC" }, { "name": "Tuvalu", "dial_code": "+688", "code": "TV" }, { "name": "Uganda", "dial_code": "+256", "code": "UG" }, { "name": "Ukraine", "dial_code": "+380", "code": "UA" }, { "name": "United Arab Emirates", "dial_code": "+971", "code": "AE" }, { "name": "United Kingdom", "dial_code": "+44", "code": "GB" }, { "name": "United States", "dial_code": "+1", "code": "US" }, { "name": "Uruguay", "dial_code": "+598", "code": "UY" }, { "name": "Uzbekistan", "dial_code": "+998", "code": "UZ" }, { "name": "Vanuatu", "dial_code": "+678", "code": "VU" }, { "name": "Venezuela, Bolivarian Republic of Venezuela", "dial_code": "+58", "code": "VE" }, { "name": "Vietnam", "dial_code": "+84", "code": "VN" }, { "name": "Virgin Islands, British", "dial_code": "+1284", "code": "VG" }, { "name": "Virgin Islands, U.S.", "dial_code": "+1340", "code": "VI" }, { "name": "Wallis and Futuna", "dial_code": "+681", "code": "WF" }, { "name": "Yemen", "dial_code": "+967", "code": "YE" }, { "name": "Zambia", "dial_code": "+260", "code": "ZM" }, { "name": "Zimbabwe", "dial_code": "+263", "code": "ZW"}]');
        return $countryAarray;
      }
  function lastLogin($user_id){ 
    $UsersModel = TableRegistry::get("users");  
    $user = $UsersModel->find('all')->where(['id' => $user_id])->first();
    $user->second_last_login = $user->last_login;
    $user->last_login =  date("Y-m-d h:i:s");
    $session = $this->request->session();
    $session->write('last_login', $user->last_login);    
     $UsersModel->save($user);
  }

  /*
  * Add Application
  * @params (appName, userId)
  * @response - true/false
  */
  public function addCommonApplication($appName, $userId){
    $appModal = TableRegistry::get('Applications');
    $usersModel = TableRegistry::get('Users');
    $groupsModel = TableRegistry::get('Groups');

    $group = $groupsModel->newEntity();
    $group->group_name = $appName;
    $group->owner_id = $userId;
    $groupName = $appName;
        
    //API call to create wowza app
    //$url = WOWZA_DOMAIN.'api/v1/createApp';
    $url = WOWZA_DOMAIN.'api/v1/createAutoBroadcastApplicaton';
    $appName =  $appName.rand(10,1000)."000001";
    $data = array('app_name' => $appName);
    $result = $this->postCurl( $url, json_encode($data) );
    $api_responcs = json_decode( $result );
    if( $api_responcs->status == 'failure' ){
      return false;
    }else{
      $groupsModelData = $groupsModel->find('all')->where(['owner_id'=>$userId,'group_name' =>$groupName ])->first();
       $application = $appModal->newEntity();
       if(!empty($groupsModelData)){
        $application->group_id = $groupsModelData->id;
      }else if($groupsModel->save($group)){
        $application->group_id = $group->id;
      }else{
        return false;
      } 
      $application->application_name = $appName;
      $application->manager_id = $userId;
      $application->id_web_registration_enabled = true;
      if($appModal->save($application)){
        return true;
      }else {
        return false;
      }
    }
  }

    function changeAutoLiveStatus($target_id = "", $value = "",$internal){
    $this->autoRender = false; // avoid to render view
    if($target_id  == ""){
        $target_id = $this->request->data['target_id'];
    }
    if($value  == ""){
        $value = $this->request->data['value'];
    }

    $CommingUserModel = TableRegistry::get("comming_users");
    $CommingUser = $CommingUserModel->find('all')->where(['id' => $target_id])->first();

    $FormListingModel = TableRegistry::get("form_listings");  
    $FormSingleData = $FormListingModel->find('all')->contain(['Groups'])->where(['form_id'=>$CommingUser['form_id']])->first();
  

    $totalTargetLimit = 0;
    $peg_limit = 0;
    $date = date("Y-m-d");
    $UserPlansModel = TableRegistry::get("user_plans");
    $PlanData = $UserPlansModel->find('all')->where(['user_id' => $FormSingleData['owner_id'], 'DATE(start_date) <=' => $date, 'DATE(expiry_date) >=' => $date, 'is_expired'=> 0,'auto_limit >' =>'0'])->toArray();
    
    if(count($PlanData) > 0){
      foreach($PlanData as $plan){
        $totalTargetLimit += $plan['auto_limit'];
        //$advancedPlanNameArr = array('Trial', 'Unlimited', 'Multi 50', 'Multi PLUS 10', 'Multi PLUS 25', 'Multi PLUS 50', 'Multi PLUS 1 Extra'); 
        $advancedPlanNameArr = array('Trial', 'Unlimited', 'Multi 10', 'Multi 20', 'Multi 30', 'Customize'); 
        if(in_array($plan['product_name'], $advancedPlanNameArr)){
          if($plan['product_name'] == 'Multi 50'){
            $peg_limit += 1;
          }else{
            $peg_limit += $plan['target_limit'];
          }
        }
      }     
    }

    $totalActiveCount = $CommingUserModel->find('all')->where(['form_id' =>  $CommingUser['form_id'], 'auto_live'=> 1])->count();

    $CommingUserData            = $CommingUserModel->newEntity();
    $CommingUserData->auto_live       = $value;
    $CommingUserData->id          = $target_id;
    
    if(!empty($CommingUser)){
      $status = false;
      if($value == 1){
        $isAdd = true;
        if($totalActiveCount >= $totalTargetLimit || $totalTargetLimit == 0){
          $isAdd = false;
        }

        if($isAdd){
          $status = true;
        }else{
          if($internal){
            return 2;
          }
          echo 2;
          exit;
        }
      }else{
        $status = true;
      }
      if($status){
        if ($CommingUserModel->save($CommingUserData)) {
          if($internal){
            return 1;
          }
          echo 1;
        }       
      }else{
        if($internal){
          return 0;
        }
        echo 0;
      }
    }else{
      if($internal){
        return 0;
      }
      echo 0;
    } 
    exit;
  }

  /*
  Method : updateInteractions()
  Title : Used to update interactions in broadcast table using FB batch request
  author : Sandhya Ghatoda
  Created Date : 19-09-2019
  */
  function updateInteractions($streamID = "",$formId = "" ){

     $broadCastStatesModel = TableRegistry::get("broadcast_details");
    if($streamID != ""){
      $broadCastStates = $broadCastStatesModel->find('all')->where(['stream_id' => $streamID, 'error'=> 0])->toArray();
    } else if($formId != ""){
      $broadCastStates = $broadCastStatesModel->find('all')->where(['form_id' => $formId, 'error'=> 0])->toArray();
    }else{
      $broadCastStates = $broadCastStatesModel->find('all')->where(['error'=> 0])->toArray(); 
    } 
    //echo "<pre>";print_r($broadCastStates);die;
    $targetChunks = array_chunk($broadCastStates, 50);

    foreach ($targetChunks as $targets) 
      {
        $batchArray = array();
        $batchArrayNew = array();
      foreach ($targets as $broadCastStats) {
        $targetModel = TableRegistry::get("comming_users");
        $target = $targetModel->find('all')->where(['id' => $broadCastStats->target_id])->first(); 
        if($broadCastStats->type == "yt"){
          if($broadCastStats->video_id != ""){
            $url = "https://www.googleapis.com/youtube/v3/videos?id=".$broadCastStats->video_id."&key=AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI&part=statistics";
            $restResult = $this->getCurl( $url);
            if(!empty($restResult)){
              $broadCastStats->reaction_response = $restResult;
              $response = json_decode( $restResult );
              $broadCastStats->status = "VOD";
              $broadCastStats->error = 0;
              if(isset($response->items[0]) && isset($response->items[0]->statistics)){
                $broadCastStats->view =    isset($response->items[0]->statistics->viewCount) ? $response->items[0]->statistics->viewCount : 0;
                $broadCastStats->vlike =    isset($response->items[0]->statistics->likeCount) ? $response->items[0]->statistics->likeCount : 0;
                $broadCastStats->dislike =    isset($response->items[0]->statistics->dislikeCount) ? $response->items[0]->statistics->dislikeCount : 0;
                $broadCastStats->comment =    isset($response->items[0]->statistics->commentCount) ? $response->items[0]->statistics->commentCount : 0;
              }
            }else{
            $broadCastStats->error = 1;
              if($broadCastStats->status == "" || $broadCastStats->status != "VOD"){
              $broadCastStats->status = "UNPUBLISHED";
            }
          }
          }
        }else if($broadCastStats->type == "fb"){
          if($broadCastStats->video_id != ""){
            if (strpos($broadCastStats->video_id, '?') !== false) {
               $videos =  explode("?", $broadCastStats->video_id); 
               if(isset($videos[0])){
                $videoID = $videos[0];
               }
            }else{
              $videoID =  $broadCastStats->video_id;
            }

            $batchData = $this->updateInteractionsBatchRequest($videoID, $target->access_token);

            array_push($batchArray, $batchData);

            //echo "<pre>"; print_r($batchArray);

          }
        }
       
      }
      $data = array(
              'include_headers' => false,
              'access_token' => FB_GLOBAL_TOKEN,
              'batch' => json_encode($batchArray)
          );

        // CURL POST Request for batch request
      

      $result = $this->postCurl(FB_GRAPH_API_HOST, $data, true);

      $result = json_decode($result);
      //echo "<pre>";print_r($result);
      if(!empty($result->error))
      {
        echo '<pre>'; print_r($result); die;
      }
      else
      {
        $i = 0;
        foreach ($targets as $broadCastStats) 
        {
          if($broadCastStats->type == "fb"){
          $targetModel = TableRegistry::get("comming_users");
          $target = $targetModel->find('all')->where(['id' => $broadCastStats->target_id])->first(); 

          $response = json_decode($result[$i]->body);
         //echo "<pre>";print_r($response);
          $broadCastStats->reaction_response .= $response->error;

          if(!empty($response->error)){
              $broadCastStats->message = $response->error->message;
              $broadCastStats->error = 1;
              if($broadCastStats->status == "" || $broadCastStats->status != "VOD"){
                $broadCastStats->status = "UNPUBLISHED";
              }
            }else{
              $broadCastStats->error = 0;
              $broadCastStats->status = $response->status;
            }

            if($response->permalink_url != ""){
              $insight = explode('videos',$response->permalink_url);
              if(isset($insight[1])){
                 $insight =  str_replace('/','',$insight[1]);
                 $viewUrl = FB_GRAPH_API_HOST .$insight."?fields=video_insights{period,values,live_views,description,name,title}&access_token=".$target->access_token;
                  //NONE, LIKE, LOVE, WOW, HAHA, SAD, ANGRY, THANKFUL, PRIDE,comment,view
                  $viewRestResult = $this->getCurl( $viewUrl);
                  $broadCastStats->reaction_response .= $viewRestResult;
                  $viewResponse = json_decode( $viewRestResult );
                  if(isset($viewResponse->video_insights->data[0])){
                    $totalView = $viewResponse->video_insights->data[0]->values;
                    if(isset($totalView[0])){
                      $broadCastStats->view = $totalView[0]->value;
                    }
                  }
              }
            }

            if(isset($response->comments)){
              $broadCastStats->view = $response->live_views;
              $broadCastStats->vlike = $response->like->summary->total_count;
              $broadCastStats->comment =  $response->comments->summary->total_count;
              $broadCastStats->love = $response->love->summary->total_count;
              $broadCastStats->none = $response->none->summary->total_count;
              $broadCastStats->wow = $response->wow->summary->total_count;
              $broadCastStats->pride = $response->pride->summary->total_count;
              $broadCastStats->thankful = $response->thankful->summary->total_count;
              $broadCastStats->angry = $response->angry->summary->total_count;
              $broadCastStats->sad = $response->sad->summary->total_count;
              $broadCastStats->haha = $response->haha->summary->total_count;
            }

            if($response->permalink_url != ""){

              $broadCastStats->permalink_video_id = $response->video->id;

              $batchDataNew = $this->updateInteractionsShareBatchRequest($broadCastStats->user_identifier, $response->video->id, $target->access_token);

              array_push($batchArrayNew, $batchDataNew);
            }
           


          $i++;
        }
        $broadCastStats->updated_date = date("Y-m-d H:i:s");
        $ss = $broadCastStatesModel->save($broadCastStats);

        }

        $dataNew = array(
            'include_headers' => false,
            'access_token' => FB_GLOBAL_TOKEN,
            'batch' => json_encode($batchArrayNew)
        );

          // CURL POST Request for batch request
        

        $resultNew = $this->postCurl(FB_GRAPH_API_HOST, $dataNew, true);

        $resultNew = json_decode($resultNew);

        //echo "<pre>";print_r($resultNew);

        if(!empty($resultNew->error))
        {
          echo '<pre>'; print_r($resultNew); die;
        }
        else
        {
          $i = 0;
          foreach ($targets as $broadCastStats) 
          {
            if($broadCastStats->type == "fb"){

              $responseNew = json_decode($resultNew[$i]->body);
              //echo "<pre>";print_r($response);
              $broadCastStats->reaction_response .= $responseNew->error;

              if(!empty($responseNew->error)){
                $broadCastStats->share_error_message = $responseNew->error->message;
                
              }

              if(isset($responseNew->shares)){
                $broadCastStats->share = $responseNew->shares->count;
              } 

              $broadCastStats->updated_date = date("Y-m-d H:i:s");
              $broadCastStatesModel->save($broadCastStats);


            }
            $i++;
          }

        }


      }
   
    }
    
  }

  /*
    Method : updateInteractionsBatchRequest()
    Title : Used to make batch for update interactions
    author : Sandhya Ghatoda
    Created Date : 19-09-2019
  */
  function updateInteractionsBatchRequest($videoID, $access_token)
  {
      $batch = new \stdClass();
      $batch->method = "POST";

      $batch->relative_url = $videoID."?fields=status,live_views,permalink_url,video,comments.summary(total_count).limit(0).as(comments),reactions.type(LIKE).summary(total_count).limit(0).as(like),reactions.type(LOVE).summary(total_count).limit(0).as(love),reactions.type(WOW).summary(total_count).limit(0).as(wow),reactions.type(HAHA).summary(total_count).limit(0).as(haha),reactions.type(SAD).summary(total_count).limit(0).as(sad),reactions.type(ANGRY).summary(total_count).limit(0).as(angry),reactions.type(NONE).summary(total_count).limit(0).as(none),reactions.type(THANKFUL).summary(total_count).limit(0).as(thankful),reactions.type(PRIDE).summary(total_count).limit(0).as(pride)&access_token=".$access_token;

      return $batch;
  }

  /*
    Method : updateInteractionsShareBatchRequest()
    Title : Used to make batch for update video Shares
    author : Sandhya Ghatoda
    Created Date : 21-10-2019
  */
  function updateInteractionsShareBatchRequest($userIdentifier, $videoID, $access_token)
  {
      $batch = new \stdClass();
      $batch->method = "GET";

      $batch->relative_url = $userIdentifier."_".$videoID."?fields=shares&access_token=".$access_token;

      return $batch;
  }

  /*
    Method : updateVideoStatNew()
    Title : Used for update video status in broadcast_detail table using facebook batch process
    author : Sandhya Ghatoda
    Created Date : 27-09-2019
    Modified Date : 12-10-2019
  */
  function updateVideoStatNew($broadCastStates ){
  $broadCastStatesModel = TableRegistry::get("broadcast_details");
   

    $targetChunks = array_chunk($broadCastStates, 50);

    foreach ($targetChunks as $targets) 
      {
        $batchArray = array();
        $batchArrayNew = array();
      foreach ($targets as $broadCastStats) {
        $targetModel = TableRegistry::get("comming_users");
        $target = $targetModel->find('all')->where(['id' => $broadCastStats->target_id])->first(); 
        if($broadCastStats->type == "yt"){
          if($broadCastStats->video_id != ""){
            $url = "https://www.googleapis.com/youtube/v3/videos?id=".$broadCastStats->video_id."&key=AIzaSyB7qIKLbjvc0DdP3HNpltuMNdBzBo_afUI&part=statistics";
            $restResult = $this->getCurl( $url);
            if(!empty($restResult)){
              $broadCastStats->reaction_response = $restResult;
              $response = json_decode( $restResult );
              $broadCastStats->status = "VOD";
              $broadCastStats->error = 0;
              if(isset($response->items[0]) && isset($response->items[0]->statistics)){
                $broadCastStats->view =    isset($response->items[0]->statistics->viewCount) ? $response->items[0]->statistics->viewCount : 0;
                $broadCastStats->vlike =    isset($response->items[0]->statistics->likeCount) ? $response->items[0]->statistics->likeCount : 0;
                $broadCastStats->dislike =    isset($response->items[0]->statistics->dislikeCount) ? $response->items[0]->statistics->dislikeCount : 0;
                $broadCastStats->comment =    isset($response->items[0]->statistics->commentCount) ? $response->items[0]->statistics->commentCount : 0;
              }
            }else{
            $broadCastStats->error = 1;
              if($broadCastStats->status == "" || $broadCastStats->status != "VOD"){
              $broadCastStats->status = "UNPUBLISHED";
            }
          }
          }
        }else if($broadCastStats->type == "fb"){
          if($broadCastStats->video_id != ""){
            if (strpos($broadCastStats->video_id, '?') !== false) {
               $videos =  explode("?", $broadCastStats->video_id); 
               if(isset($videos[0])){
                $videoID = $videos[0];
               }
            }else{
              $videoID =  $broadCastStats->video_id;
            }

            $batchData = $this->updateVideoStateBatchRequest($videoID, $target->access_token);

            array_push($batchArray, $batchData);

            //echo "<pre>"; print_r($batchArray);

          }
        }
       
      }
      $data = array(
              'include_headers' => false,
              'access_token' => FB_GLOBAL_TOKEN,
              'batch' => json_encode($batchArray)
          );

        // CURL POST Request for batch request
      

      $result = $this->postCurl(FB_GRAPH_API_HOST, $data, true);

      $result = json_decode($result);

      if(!empty($result->error))
      {
        echo '<pre>'; print_r($result); die;
      }
      else
      {
        $i = 0;
        foreach ($targets as $broadCastStats) 
        {
          if($broadCastStats->type == "fb"){
          $targetModel = TableRegistry::get("comming_users");
          $target = $targetModel->find('all')->where(['id' => $broadCastStats->target_id])->first(); 

          $response = json_decode($result[$i]->body);
         //echo $response->permalink_url;
          $broadCastStats->reaction_response .= $response->error;

          if(!empty($response->error)){
              $broadCastStats->message = $response->error->message;
              $broadCastStats->error = 1;
              if($broadCastStats->status == "" || $broadCastStats->status != "VOD"){
                $broadCastStats->status = "UNPUBLISHED";
              }
            }else{
              $broadCastStats->error = 0;
              $broadCastStats->status = $response->status;
            }

            if($response->permalink_url != ""){
              $insight = explode('videos',$response->permalink_url);
              if(isset($insight[1])){
                 $insight =  str_replace('/','',$insight[1]);
                 $viewUrl = FB_GRAPH_API_HOST .$insight."?fields=video_insights{period,values,live_views,description,name,title}&access_token=".$target->access_token;
                  //NONE, LIKE, LOVE, WOW, HAHA, SAD, ANGRY, THANKFUL, PRIDE,comment,view
                  $viewRestResult = $this->getCurl( $viewUrl);
                  $broadCastStats->reaction_response .= $viewRestResult;
                  $viewResponse = json_decode( $viewRestResult );
                  if(isset($viewResponse->video_insights->data[0])){
                    $totalView = $viewResponse->video_insights->data[0]->values;
                    if(isset($totalView[0])){
                      $broadCastStats->view = $totalView[0]->value;
                    }
                  }
              }
            }

            if(isset($response->comments)){
              $broadCastStats->view = $response->live_views;
              $broadCastStats->vlike = $response->like->summary->total_count;
              $broadCastStats->comment =  $response->comments->summary->total_count;
              $broadCastStats->love = $response->love->summary->total_count;
              $broadCastStats->none = $response->none->summary->total_count;
              $broadCastStats->wow = $response->wow->summary->total_count;
              $broadCastStats->pride = $response->pride->summary->total_count;
              $broadCastStats->thankful = $response->thankful->summary->total_count;
              $broadCastStats->angry = $response->angry->summary->total_count;
              $broadCastStats->sad = $response->sad->summary->total_count;
              $broadCastStats->haha = $response->haha->summary->total_count;
            }

             if($response->permalink_url != ""){

              $broadCastStats->permalink_video_id = $response->video->id;

              $batchDataNew = $this->updateInteractionsShareBatchRequest($broadCastStats->user_identifier, $response->video->id, $target->access_token);

              array_push($batchArrayNew, $batchDataNew);
            }


            $i++;
          }
          $broadCastStats->updated_date = date("Y-m-d H:i:s");
          $broadCastStatesModel->save($broadCastStats);
        }

        $dataNew = array(
            'include_headers' => false,
            'access_token' => FB_GLOBAL_TOKEN,
            'batch' => json_encode($batchArrayNew)
        );

          // CURL POST Request for batch request
        

        $resultNew = $this->postCurl(FB_GRAPH_API_HOST, $dataNew, true);

        $resultNew = json_decode($resultNew);

        //echo "<pre>";print_r($resultNew);
        if(!empty($resultNew->error))
        {
          echo '<pre>'; print_r($resultNew); die;
        }
        else
        {
          $i = 0;
          foreach ($targets as $broadCastStats) 
          {
            if($broadCastStats->type == "fb"){

              $responseNew = json_decode($resultNew[$i]->body);
              //echo "<pre>";print_r($response);
              $broadCastStats->reaction_response .= $responseNew->error;

              if(!empty($responseNew->error)){
                $broadCastStats->share_error_message = $responseNew->error->message;
                
              }

              if(isset($responseNew->shares)){
                $broadCastStats->share = $responseNew->shares->count;
              } 

              $broadCastStats->updated_date = date("Y-m-d H:i:s");
              $broadCastStatesModel->save($broadCastStats);


            }
            $i++;
          }

        }

      }
    }
  }

   /*
    Method : updateVideoStateBatchRequest()
    Title : Create batch array
    author : Sandhya Ghatoda
    Created Date : 27-09-2019
    Modified Date : 12-10-2019
  */
  function updateVideoStateBatchRequest($videoID, $access_token)
  {
      $batch = new \stdClass();
      $batch->method = "POST";

      $batch->relative_url = $videoID."?fields=status,live_views,permalink_url,video,comments.summary(total_count).limit(0).as(comments),reactions.type(LIKE).summary(total_count).limit(0).as(like),reactions.type(LOVE).summary(total_count).limit(0).as(love),reactions.type(WOW).summary(total_count).limit(0).as(wow),reactions.type(HAHA).summary(total_count).limit(0).as(haha),reactions.type(SAD).summary(total_count).limit(0).as(sad),reactions.type(ANGRY).summary(total_count).limit(0).as(angry),reactions.type(NONE).summary(total_count).limit(0).as(none),reactions.type(THANKFUL).summary(total_count).limit(0).as(thankful),reactions.type(PRIDE).summary(total_count).limit(0).as(pride)&access_token=".$access_token;

      return $batch;
  }
}