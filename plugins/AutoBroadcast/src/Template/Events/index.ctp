<!-- END PAGE HEADER-->

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"><?php echo __(Manage_Schedule)?> </span>
                </div>
            </div> 
            <div class="portlet-body">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <div class="btn-group">
                                <a href="<?php echo HTTP_ROOT; ?>events/addnew" class="nav-link ">
                                    <button class="btn sbold green"><?php echo __(ScheduleNew)?>
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </a>
                            </div>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body table-responsive1">
                        <table class="table table-striped table-bordered table-hover dt-responsive" width="100%" id="events_table">
                            <thead>
                              <tr>
								<!-- <th><?= __(title);?></th> -->
								<th><?= __(start);?></th>
					            <th><?= __(Repetative);?></th>
                                 <th><?= __(Added_By);?></th>
                                 <th>
                                   <?php echo __(live_status);?>
                                </th>

								<th class="actions"><?= __(actions);?></th>
								</tr>
                            </thead>
                            <tbody>
                            <?php
								$i = 0;
								foreach ($events as $event):
                              
                                date_default_timezone_set("America/Sao_Paulo");
                                $dateEnd =  $event->server_time->format('Y-m-d H:i:s');
                                $seconds  = strtotime($dateEnd) - strtotime(date('Y-m-d H:i:s'));

									$class = null;
									if ($i++ % 2 == 0) {
										$class = ' class="altrow"';
									}
							?>
								<tr<?= $class;?>>
								
									<!-- <td><?= $event->title ?></td> -->
									<td><?= $event->start->format('Y-m-d H:i:s'); ?></td>							       
							        <td><?php if($event->is_repetative != 0) { echo "Yes"; } else { echo "No"; } ?></td>
                                    <td><?php echo $event->user->name ?></td>
                                    <td>
                                       <?php 
                                       if($event->is_preminum){
                                            if($event->current_status == 1){
                                            echo __('Awaiting');

                                            }else if($event->current_status == 2){
                                             echo __('PROCESSSING');
                                            }else if($event->current_status == 3){
                                             echo __('COMPETETED');
                                            }else if($event->current_status == 4){
                                             echo __('STOPED');
                                            }else if($event->current_status == 5){
                                             echo __('PAUSED');
                                            }
                                       }else{
                                            if(!$event->is_live){
                                                echo __('Awaiting');
                                            }else{
                                                echo __('COMPETETED');
                                            }
                                         
                                       }?>
                                   </td>
									<td class="actions">
										<?= $this->Html->link('<i class="fa fa-eye text-green" aria-hidden="true"></i>', ['action' => 'view', $event->id], array('escape'=> false)); ?>

										<?php echo ($seconds > 0 ) ?   $this->Html->link('<i class="fa fa-edit font-dark" aria-hidden="true"></i>', ['action' => 'editnew', $event->id], array('escape' => false)) : ''; ?>
										<?= $this->Form->postLink('<i class="fa fa-trash-o text-danger"></i>', ['action' => 'delete', $event->id], array('escape'=> false,'confirm' => __(Are_you_sure))); ?>
									</td>
                                    
								</tr>
							<?php endforeach; ?>
                            
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
        </div>
<!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<style>
	[class*=" fa-"]:not(.fa-stack), [class*=" glyphicon-"], [class*=" icon-"], [class^=fa-]:not(.fa-stack), [class^=glyphicon-], [class^=icon-] {
		margin: 0 2px;
	}

</style>