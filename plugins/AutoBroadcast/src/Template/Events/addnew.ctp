<div class="">
   <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
         <div class="x_panel">
            <div class="x_title">
               <h4><?= __(ScheduleNew); ?><small></small></h4>
               
            </div>
			<?php if(($isAutoPrem)){ ?>
            <div class="share_box">
               <div class="row  flex_row">
                  <div class="col-md-2">
                     <img class="pal_img" src="../img/palimage.png" >
                  </div>
                  <div class="col-md-10">
                     <p><?= __(pal_msg); ?></p>
                  </div>
               </div>
            </div>
			<?php } else { ?>
			
			<div class="share_box">
               <div class="row  flex_row">
                  <div class="col-md-2">
                     <img class="pal_img" src="../img/palimage.png" >
                  </div>
                  <div class="col-md-10">
                     <p><?= __(non_pal_msg); ?></p>
                  </div>
               </div>
            </div>			
			
			<?php } ?>
			
            
			
            <div class="x_content">
               <div class="row">
               <div class="col-sm-12">
                  <section>
                     <div class="wizard mWizard">
                        <div class="wizard-inner">
                           <div class="connecting-line"></div>
                           <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation" class="active" id = "1">
                                 <a href="#step1" data-toggle="tab" aria-controls="step1"  role="tab" >
                                 <span class="round-tab">
                                 1
                                 </span>
                                 <span class="desc">   <?php echo __('titileText'); ?></span>
                                 <span class="small_text"><?php echo __('titileTextD'); ?></span>

                                 </a>
                              </li>
                              <li role="presentation" class="disabled" id = "2">
                                 <a href="#step2" data-toggle="tab" aria-controls="step2"  role="tab" >
                                 <span class="round-tab">
                                 2
                                 </span>
                                  <span class="desc"><?php echo __('uploadVideo'); ?> </span>
                                 <span class="small_text"><?php echo __('uploadVideod'); ?></span>
                                 </a>
                              </li>
                              <li role="presentation" class="disabled" id = "3">
                                 <a href="#step3" data-toggle="tab" aria-controls="step3"  role="tab" >
                                 <span class="round-tab">
                                 3
                                 </span>
                                  <span class="desc">  <?php echo __('dateTime'); ?></span>
                                 <span class="small_text"><?php echo __('dateTimed'); ?></span>
                                 </a>
                              </li>
                              <li role="presentation" class="disabled" id = "4">
                                 <a href="#complete" data-toggle="tab" aria-controls="complete"  role="tab" >
                                 <span class="round-tab">
                                 4
                                 </span>
                                  <span class="desc">  <?php echo __('confirm'); ?></span>
                                 <span class="small_text"><?php echo __('confirmd'); ?></span>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <?= $this->Form->create($event,[
                           'class'     =>'form-horizontal form-label-left',
                           //'id'        =>'broadcastAdd',
                           'enctype'   =>'multipart/form-data',
                           'novalidate'=>'novalidate',
                           'autocomplete'=>'off'
                            // 'autocomplete'=>'off',
                           ]);?>
                        <?php 
                           echo $this->Form->input('td_uses',array('type'=>'hidden',
                             'label' => false,
                             'value' =>0,
                              'class'=>'td_uses'));
                        ?>
                        <?php 
                            echo $this->Form->input('is_preminum',array('type'=>'hidden',
                              'label' => false,
                              'name' =>'is_preminum',
                              'value' =>($isAutoPrem) ? 1 : 0,
                               'class'=>'is_premium'));
                        ?>
                        <div class="tab-content">
                           <div class="tab-pane active" role="tabpanel" id="step1">
                              <h3><?php echo __(step1title); ?></h3>
                              <!--<p><?php echo __(step1des); ?></p>-->
                               <div class="item form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12"><?= __(title); ?>
                                   <span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                      <?php
                                         echo $this->Form->input('title', ['label' => false,
                                            // 'required' => true,
                                           'value' => $titles,
                                            'type' => 'textarea',
                                            'class'=>'form-control col-md-7 col-xs-12 hint2emoji']);
                                         ?>
                                     <span id="title-error" class="help-block help-block-error"></span>
                                   </div>
                                </div>
                               
                                <div class="item form-group">
                                   <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                   <?= __(description); ?><span class="required">*</span>
                                   </label>
                                   <div class="col-md-6 col-sm-6 col-xs-12">
                                      <?php 
                                         echo $this->Form->input('details', ['label' => false,
                                            // 'required' => true,
                                           'value' => $details,
                                            'type' => 'textarea',
                                            'class'=>'hint2emoji form-control col-md-7 col-xs-12']);
                                         ?>
                                         <span id="des-error" class="help-block help-block-error"></span>
                                   </div>
                                   
                                </div>
                              <ul class="list-inline" style="text-align:center; width:100%;">
                                 <li><button type="button" class="next btn open_panel hvr-icon-wobble-horizontal next-step"><?php echo __(step1btn); ?></button></li>
                              </ul>
                           </div>
                           <div class="tab-pane" role="tabpanel" id="step2">
                              <h3><?php echo __(step2title); ?></h3>
							  <p><?php echo __(step2des); ?></p>
                              <!--<p><div class="clearfix"><?php echo __(upfrontmsg); ?></div></p>-->
                              <div class="display-none help-block-error" id="error"></div>
                              <div class="col-md-12 col-xs-12">
                                   <div class="display-none" id="do_not_close" style="text-align:center;"><b><?php echo __('do_not_close'); ?></b></div>
                                    <div id="progress-div" class="display-none">
                                      <div id="progress-bar"></div>
                                    </div>
                                    <div id="targetLayer"></div>
                                     <div id="do_not_close_spn" class="display-none" style="text-align: center; margin-bottom: 20px;">

                                    <div class="loading">
                                      <div class="loading-bar"></div>
                                      <div class="loading-bar"></div>
                                      <div class="loading-bar"></div>
                                      <div class="loading-bar"></div>
                                    </div>
                                     </div>
                              </div>
                              <input type="hidden" name="site_lang" id="site_lang" value="<?php echo $site_lang; ?>">
                              <input type="hidden" name="video_name" id="video-name">
                              <div class="video-item-section" style="text-align:center; margin-bottom: 15px;"></div>
                              <div class="form-group tab2-sec">

                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                 <?= __(Event_Type); ?> <span class="required">*</span>
                                 </label>
                                 <div class="col-md-7 col-xs-12 optsent">
                                    <?php 
                                       $options = array(
                                           'video' => __(video),
                                           // 'photos' => __(Photos),
                                           'external' =>  __(External_Links)
                                       );
                                       
                                       $attributes = array(
                                           'legend' => false,
                                           'value' => 'video',
                                       );
                                       
                                       echo $this->Form->radio('event_type', $options, $attributes);
                                       ?>
                                 </div>
                              </div>
                              <div class="item form-group tab2-sec">

                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                 <?= __(Upload); ?> <span class="required">*</span>
                                 </label>
                                 <div class="col-md-6 col-sm-6 col-xs-12 video_section">
                                    <?php
                                       echo $this->Form->input('upload', ['type' => 'file' ,'label' =>  false,
                                           'required' => true,
                                           'name' =>'video',
                                           'class'=>'video_input']);
                                        ?>
                                 </div>
                                 <div class="col-md-6 col-sm-6 col-xs-12 photos_section display-none">
                                    <div class="col-md-12" > <?= __(broadcastduration); ?>
                                       <?php
                                          echo $this->Form->input('image_duration', ['label' => false,
                                             'required' => true,
                                             'type' => 'text',
                                             'class'=>'form-control col-md-7 col-xs-12']);
                                             ?>
                                    </div>
                                    <div class="col-md-12 field_wrapper_image" >
                                       <div class="col-md-10 col-sm-8 col-xs-6">
                                          <?php     
                                             echo $this->Form->input('image', ['type' => 'file' ,'label' =>  false,
                                                'required' => true,
                                                'name' =>'images[]',
                                                'class'=>'image_input']);
                                             ?>
                                       </div>
                                       <div class="col-md-2 col-sm-4 col-xs-6 pluseBox">
                                          <a href="javascript:void(0);" class="add_button_image" title="Add field"><button  class="btn sbold green"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="col-md-6 col-sm-6 col-xs-12 link_section display-none">
                                    <!--<div class="col-md-12">
                                       <div class="col-md-1">
                                          <input type="radio" name="link_radio" value="youtube_link" class="link_radio"  />
                                       </div>
                                       <div class="col-md-11">
                                          <?php 
                                             echo $this->Form->input('youtube_link', ['label' => false,
                                             'required' => true,
                                             'type' => 'text',
                                             'class'=>'form-control col-md-7 col-xs-12',
                                             'placeholder'=> __(ytlink),
                                             'disabled'=> true]);
                                             ?>
                                       </div>
                                    </div>
                                    <br/><br/><br/> -->
                                    <div class="col-md-12">
                                       <div class="col-md-1">
                                          <input type="radio" name="link_radio" value="dropbox_link" class="link_radio" checked />
                                       </div>
                                       <div class="col-md-11">
                                          <?php 
                                             echo $this->Form->input('dropbox_link', ['label' => false,
                                             'required' => true,
                                             'type' => 'text',
                                             'class'=>'form-control col-md-7 col-xs-12',
                                             'placeholder'=>__(dblink)]);
                                             ?>
                                       </div>
                                    </div><br>
                                    <br>
                                    
                                    <div class="col-md-12">
                                      <button class="btn" id="upload_other"><?php echo __(Upload)?></button>
                                    </div>
                                 </div>
                              </div>
                              <ul class="list-inline" style="text-align:center; width:100%;">
                                 <li><button type="button" class="next btn open_panel hvr-icon-wobble-horizontal prev-step"><?php echo __(step2pbtn); ?></button></li>
                                 <li><button type="button" class="next btn open_panel hvr-icon-wobble-horizontal next-step"><?php echo __(step2btn); ?></button></li>
                              </ul>
                           </div>
                           <div class="tab-pane" role="tabpanel" id="step3">
                              <div class="row" style="margin-left: 0;">
								<div class="col-sm-5 col-sm-offset-3" style="padding-left: 5px;"><h3 align="center"><?php echo __(step3title); ?></h3></div>
								</div>
                              <!--<p><?php echo __(step3des); ?></p>-->
                              <div class="item form-group">
                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                 <?= __(start); ?> <span class="required">*</span>
                                 </label>
                                 <div class="col-md-6 col-sm-6 col-xs-12 field_wrapper">
                                    <div class="col-md-10 col-sm-8 col-xs-9 parentStart ">
                                       <?php
                                          echo $this->Form->input('start', ['label' => false,
                                             'required' => true,
                                             'type' => 'text',
                                             'class'=>'form-control col-md-7 col-xs-12 startRepetative']);
                                          ?>
                                    </div>
                                    <div class="col-md-2 col-sm-4 col-xs-3 pluseBox">
                                       <a href="javascript:void(0);" class="add_button" title="<?= __(add_new); ?>"><button  class="btn sbold green"> <i class="fa fa-plus" aria-hidden="true"></i></button></a>
                                    </div>
                                 </div>
                              </div>
                              <ul class="list-inline" style="text-align:center; width:100%;">
                                 <li><button type="button" class="next btn open_panel hvr-icon-wobble-horizontal prev-step"><?php echo __(step3pbtn); ?></button></li>
                                 <li><button type="button" class="next btn open_panel hvr-icon-wobble-horizontal next-step"><?php echo __(step3btn); ?></button></li>
                              </ul>
                           </div>
                           <div class="tab-pane" role="tabpanel" id="complete">
                              <h3><?php echo __(step4title); ?></h3>
                              <p><?php echo __(step4des); ?></p>
                              <div class="item form-group">
                                 <label class="control-label col-md-3 col-sm-3 col-xs-12">
                                 <?= __(Email_Receiver); ?> 
                                 </label>
                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                    <?php
                                       echo $this->Form->input('email_receivers', ['label' => false,
                                          // 'required' => true,
                                         'value' => $emails,
                                          'type' => 'text',
                                          'class'=>'form-control col-md-7 col-xs-12']);
                                       ?>
                                    <span class="help-block">  <?= __(Email_Receiver_eg); ?>  </span>
                                 </div>
                              </div>
                              <ul class="list-inline" style="text-align:center; width:100%;">
                                 <button id="send" type="submit" class="next btn open_panel hvr-icon-wobble-horizontal"> <?= __(step4btn); ?> </button>
                              </ul>
                           </div>
                           <div class="clearfix"></div>
                        </div>
                        <?php echo $this->form->end(); ?>
                     </div>
                  </section>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
</div>

<style>

@media only screen and (max-device-width: 540px) and (min-device-width: 320px){
	.page-content-wrapper .x_title {
		order: 1;
	}
}

</style>